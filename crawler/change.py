import concurrent.futures
import time
import traceback
from collections.abc import Callable

import requests
from redis import Redis

import database.unparsed as unparsed
from api.iris import get_all_changes, get_recent_changes
from config import redis_url
from database.base import create_all
from database.engine import sessionfactory
from database.unique_change import UniqueChangeV4
from database.upsert import upsert_with_retry
from helpers.batcher import batcher
from helpers.logger import logging
from helpers.StationPhillip import StationPhillip
from public_config import STATIONS_TO_MONITOR_PER_THREAD


def change_crawler_worker(
    evas: list[int], download_function: Callable
) -> dict[int, UniqueChangeV4]:
    changes = {}
    with requests.Session() as session:
        for eva in evas:
            try:
                result = download_function(eva, session=session)
                for change in result:
                    change = UniqueChangeV4(change, stop_id=eva)
                    changes[change.change_hash] = change
            except requests.exceptions.HTTPError:
                pass
        return changes


def get_and_process_changes(
    evas: list[int], download_function: Callable, engine, redis_client
):
    eva_batches = [
        list(batch) for batch in batcher(evas, STATIONS_TO_MONITOR_PER_THREAD)
    ]
    with concurrent.futures.ThreadPoolExecutor(
        max_workers=len(eva_batches)
    ) as executor:
        futures = list(
            executor.submit(change_crawler_worker, eva_batch, download_function)
            for eva_batch in eva_batches
        )

        changes: dict[int, UniqueChangeV4] = {}
        for future in concurrent.futures.as_completed(futures):
            result = future.result()
            changes.update(result)

        upsert_with_retry(
            engine=engine,
            table=UniqueChangeV4.__table__,
            rows=list([change.as_dict() for change in changes.values()]),
            on_conflict_strategy='nothing',
        )
        unparsed.add_change(redis_client, changes.values())


def main():
    engine, Session = sessionfactory()
    create_all(engine)

    stations = StationPhillip()
    redis_client = Redis.from_url(redis_url)

    get_and_process_changes(
        stations.get_iris_active_evas(),
        get_all_changes,
        engine,
        redis_client,
    )

    while True:
        try:
            start_time = time.time()
            get_and_process_changes(
                evas=stations.get_iris_active_evas(),
                download_function=get_recent_changes,
                engine=engine,
                redis_client=redis_client,
            )
            logging.info(f'Finished in {time.time() - start_time} seconds')
            # print(f'Finished in {time.time() - start_time} seconds')
            time.sleep(max(0, 120 - (time.time() - start_time)))
        except Exception:
            traceback.print_exc()


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
