import math
from dataclasses import dataclass, fields

import numpy.typing as npt
import polars as pl

from public_config import OFFSET


@dataclass
class TransferData:
    number: list[int]
    lat: list[float]
    lon: list[float]
    stop_sequence: list[int]
    distance_traveled: list[int]
    dwell_time_schedule: list[int | None]
    dwell_time_prognosed: list[int | None]
    bearing: list[int]
    delay_prognosed: list[int]
    minute_of_day: list[int]
    minutes_to_prognosed_time: list[int]
    weekday: list[int]
    is_regional: list[bool]
    is_arrival: list[bool]
    operator: list[str]
    category: list[str]
    line: list[str]
    prognosed_transfer_time: list[int | None]
    minimal_transfer_time: list[int | None]

    def check_length(self):
        length = len(self.number)
        assert all(
            len(getattr(self, field.name)) == length for field in fields(self)
        ), 'All attributes must have the same length'

    def to_polars(self) -> pl.DataFrame:
        self.check_length()

        df = pl.DataFrame(
            {field.name: getattr(self, field.name) for field in fields(self)}
        )

        return df


@dataclass
class PredictionResults:
    predictions: list[list[float]]
    transfer_scores: list[float]
    offset: int = OFFSET

    def __init__(
        self, predictions: npt.NDArray, transfer_scores: npt.NDArray, precision: int = 5
    ):
        self.predictions = [
            list(map(lambda v: round(v, precision), row))
            for row in predictions.tolist()
        ]
        self.transfer_scores = [
            None if math.isnan(score) else round(score, precision)
            for score in transfer_scores.tolist()
        ]
