import uvicorn
from fastapi import FastAPI

from ml_models.single_predictor import SinglePredictor
from ml_models.training_stats import TrainingStats
from predictor_webserver.types import PredictionResults, TransferData

app = FastAPI()

predictor = SinglePredictor()


@app.post('/rate-journeys/')
async def rate_journeys(transfer_data: TransferData) -> PredictionResults:
    """Rate the given journeys i.e. predict the probability of each transfer in the
    journeys being successful.

    ### Parameters:
    **transfer_data:** TransferData
    The data of the transfers to rate. The format is a little weird for performance
    reasons. Think of `trasfer_data` as a DataFrame where the rows represent
    arrivals and departures of the legs of the journeys. For web-reasons the
    DataFrame is transmitted as a dictionary of columns.

    For example for a journey from A to C with a transfer at B the data would look
    like the following abstract example:

    | data columns         | minimal needed transfer time |
    |----------------------|------------------------------|
    | departure data at A  | None                         |
    | arrival data at B    | 5 minutes                    |
    | departure data at B  | 5 minutes                    |
    | arrival data at C    | None                         |
    | ... further journeys | ...                          |

    The following columns are needed:

    - **number:** The train number
    - **lat:** The latitude of the station
    - **lon:** The longitude of the station
    - **stop_sequence:** The sequence number of the stop in the train trip (0 for the first stop, 1 for the second, ...)
    - **distance_traveled:** The distance traveled in meters from the start of the trip
    - **dwell_time_schedule:** The scheduled dwell time at the stop in minutes, or None if not available
    - **dwell_time_prognosed:** The prognosed dwell time at the stop in minutes, or None if not available
    - **bearing:** The bearing of the train in degrees between the start of the trip and the end of the trip. See https://www.movable-type.co.uk/scripts/latlong.html
    - **delay_prognosed:** The prognosed delay of the train in minutes
    - **minute_of_day:** The minute within the day of the stop (0 for midnight, 60 for 1am, ...)
    - **minutes_to_prognosed_time:** The minutes to the prognosed time of the stop i.e. 5 minutes if the prognosed time is 5 minutes in the future
    - **weekday:** ISO weekday number where monday = 1 and sunday = 7
    - **is_regional:** True if the train is a regional train, False if not
    - **is_arrival:** True if the stop is an arrival, False if it is a departure
    - **operator:** The operator code of the train, sometimes called admin code
    - **category:** The category of the train e.g. *ICE*, *RE*, *S*, *FLX*
    - **line:** The line of the train. For *RE 9* it would be *9*, for *ag RB15* it would be *RB15*
    - **prognosed_transfer_time:** The currently prognosed transfer time in minutes or None if not available or if the stop is not included in a transfer
    - **minimal_transfer_time:** The minimal transfer time in minutes for the transfer to work or None if not available or if the stop is not included in a transfer

    ### Returns:
    PredictionResults containing `transfer_scores`, raw delay `predictions` and the predictions `offset`. Considering the example from above the results fit in like the following:

    | data columns         | minimal needed transfer time | transfer score | predictions                          |
    |----------------------|------------------------------|----------------|--------------------------------------|
    | departure data at A  | None                         | None           | delay predictions for departure at A |
    | arrival data at B    | 5 minutes                    | 70%            | delay predictions for arrival at B   |
    | departure data at B  | 5 minutes                    | 70%            | delay predictions for departure at B |
    | arrival data at C    | None                         | None           | delay predictions for arrival at C   |
    | ... further journeys | ...                          | ...            | ...                                  |

    Both `predictions` and `transfer_scores` are lists of the same length as the number of rows in `transfer_data`

    - **transfer_scores:** The estimated probability that at least *minimal_transfer_time minutes remain*
    - **predictions:** Estimated probability distribution of deviation of the *prognosed_delay*. Each row is a list of probabilities. The 0th element represents the probability of the actual delay being *prognosed_delay - offset*, the *offset*-th element represents the probability of *prognosed_delay* being correct, the *offset*+1-th element represents the probability of the actual delay being *prognosed_delay* + 1, and so on.
    - **offset:** The offset used for the predictions
    """  # noqa: W505
    probas, transfer_scores = predictor.predict(transfer_data.to_polars())

    return PredictionResults(predictions=probas, transfer_scores=transfer_scores)


@app.get('/training-stats/')
async def get_training_stats() -> TrainingStats:
    """Get the training statistics of the used xgboost model."""
    return TrainingStats.load_from_file()


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8000)
