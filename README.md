# Bahn-Vorhersage [https://bahnvorhersage.de/](https://bahnvorhersage.de/)

```bash
████████████████████████████████████▇▆▅▃▁
       Bahn-Vorhersage      ███████▙  ▜██▆▁
███████████████████████████████████████████▃
▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀█████▄▖
█████████████████████████████████████████████
 ▜█▀▀▜█▘                       ▜█▀▀▜█▘   ▀▀▀
```

Bahn-Vorhersage (formerly known as TrAIn_Connection_Prediction) is a train delay prediction system for German railways. We are trying to make rail travel more user-friendly. Our machine learning system predicts the probability that all connecting trains in a connection will be coughed.

## Further Information

For more details on this project please refer to Theos talk on the Chaos Communication Congress 38c3 which is available in nativ German as well as in English an Spanish translations: https://media.ccc.de/v/38c3-wann-klappt-der-anschluss-wann-nicht-und-wie-sagt-man-chaos-vorher

For competitions we have written papers and made some posters and presentations, that can be found in our [docs repository](https://gitlab.com/bahnvorhersage/docs). Most of that is in German and most of it is outdated.

## Run your own predictions

We publish a tiny api webserver containing the current prediction model that can be used to do self-hosted delay predictions. The image is available on docker and is updated every week. The image only contains the prediction logic itself, it cannot search for journeys or do similar stuff.


### Quickstart:
```zsh
# Download image
docker pull trainconnectionprediction/bahnvorhersage-predictor:latest

# Run webserver
docker run -p 8000:8000 trainconnectionprediction/bahnvorhersage-predictor:latest
```

Futher documentation is now available at: http://0.0.0.0:8000/redoc

## Credits

- Marius De Kuthy Meurers aka [NotSomeBot](https://github.com/mariusdkm)
- Theo Döllman aka [McToel](https://gitlab.com/mctoel)
  
