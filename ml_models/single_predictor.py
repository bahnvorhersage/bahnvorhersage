import pickle

import numpy as np
import numpy.typing as npt
import polars as pl
import xgboost as xgb

from ml_models.training_stats import TrainingStats
from public_config import DATA_SCHEMA_PATH, MODEL_PATH, TRAIN_COLUMNS


class SinglePredictor:
    def __init__(self):
        self.model = xgb.XGBClassifier()
        self.model.load_model(MODEL_PATH)

        self.data_schema = pickle.load(open(DATA_SCHEMA_PATH, 'rb'))
        self.training_stats = TrainingStats.load_from_file()

    def predict(self, data: pl.DataFrame) -> tuple[npt.NDArray, npt.NDArray]:
        is_arrival = data['is_arrival'].to_list()
        assert all(is_arrival[1::2]) and not any(
            is_arrival[0::2]
        ), 'Arrival and departure must alternate, starting with departure'

        probas = self.predict_probabilities(data)

        ar_transfer_mask = (
            data['is_arrival'] & data['minimal_transfer_time'].is_not_null()
        )
        dp_transfer_mask = (
            ~data['is_arrival'] & data['minimal_transfer_time'].is_not_null()
        )

        ar_predictions = probas[ar_transfer_mask]
        dp_predictions = probas[dp_transfer_mask]

        transfer_scores = self.calculate_transfer_scores(
            ar_predictions,
            dp_predictions,
            data.filter(ar_transfer_mask)['prognosed_transfer_time'].to_list(),
            data.filter(ar_transfer_mask)['minimal_transfer_time'].to_list(),
        )

        all_transfer_scores = np.full(len(data), np.nan)
        all_transfer_scores[ar_transfer_mask] = transfer_scores
        all_transfer_scores[dp_transfer_mask] = transfer_scores

        return probas, all_transfer_scores

    def predict_ar_dp(
        self,
        arrival: pl.DataFrame,
        departure: pl.DataFrame,
        transfer_times: pl.DataFrame,
    ):
        probas_ar = self.predict_probabilities(arrival)
        probas_dp = self.predict_probabilities(departure)

        transfer_scores = self.calculate_transfer_scores(
            probas_ar,
            probas_dp,
            transfer_times['prognosed_transfer_time'].to_list(),
            transfer_times['minimal_transfer_time'].to_list(),
        )

        return transfer_scores

    def predict_probabilities(self, data: pl.DataFrame) -> np.ndarray:
        # Select only the columns that are in the model
        data = data.select(TRAIN_COLUMNS)

        # Cast to schema
        data = data.cast(self.data_schema, strict=False)

        # Predict
        return self.model.predict_proba(data.to_pandas())

    def calculate_transfer_scores(
        self,
        ar_predictions: np.ndarray,
        dp_predictions: np.ndarray,
        transfer_times: list[int],
        minimal_transfer_times: list[int],
    ) -> np.ndarray:
        # n_prediction_classes = ar_predictions.shape[1]
        transfer_scores = np.zeros(len(transfer_times))

        dp_cdf_reverse = np.flip(
            np.cumsum(np.flip(dp_predictions, axis=1), axis=1), axis=1
        )

        for i, transfer_time, minimal_transfer_time in zip(
            range(len(transfer_times)), transfer_times, minimal_transfer_times
        ):
            if transfer_time is None:
                transfer_scores[i] = None
                continue

            transfer_time_buffer = transfer_time - minimal_transfer_time

            if transfer_time_buffer > 0:
                # If the ar delay is at most transfer_time_buffer minutes, the
                # transfer does work no matter the dp delay. Otherwise, the
                # dp delay must be at least (transfer_time_buffer - ar delay) minutes
                transfer_scores[i] = np.sum(
                    ar_predictions[i, :transfer_time_buffer]
                ) + np.sum(
                    ar_predictions[i, transfer_time_buffer:]
                    * dp_cdf_reverse[i, :-transfer_time_buffer]
                )
            elif transfer_time_buffer == 0:
                # dp delay must match ar delay
                transfer_scores[i] = np.sum(ar_predictions[i] * dp_cdf_reverse[i])
            else:  # transfer_time_buffer < 0
                # dp delay must be at least -transfer_time_buffer minutes greater than
                # ar delay. Thus, the dp delay must be at least -transfer_time_buffer.
                # Big ar delays cannot work as the dp delay is capped and thus the
                # probability of the dp delay being high enough is 0.
                transfer_scores[i] = np.sum(
                    ar_predictions[i, :transfer_time_buffer]
                    * dp_cdf_reverse[i, -transfer_time_buffer:]
                )

        return transfer_scores
