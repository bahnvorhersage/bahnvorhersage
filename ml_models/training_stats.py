import json
from dataclasses import dataclass
from datetime import datetime

from helpers.json_serializer import COMPACT_ENCODING_KWARGS
from public_config import TRAINING_STATS_PATH


@dataclass
class TrainingStats:
    n_batches: int
    n_data_points: int
    training_start: datetime
    training_end: datetime

    @staticmethod
    def start_training(n_batches: int, n_data_points: int):
        return TrainingStats(
            n_batches=n_batches,
            n_data_points=n_data_points,
            training_start=datetime.now(),
            training_end=None,
        )

    def end_training(self):
        self.training_end = datetime.now()
        json.dump(self, open(TRAINING_STATS_PATH, 'w'), **COMPACT_ENCODING_KWARGS)

    @staticmethod
    def load_from_file():
        stats = json.load(open(TRAINING_STATS_PATH))
        stats['training_start'] = datetime.fromisoformat(stats['training_start'])
        stats['training_end'] = datetime.fromisoformat(stats['training_end'])

        return TrainingStats(**stats)
