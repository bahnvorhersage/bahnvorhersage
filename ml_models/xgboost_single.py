import argparse
import pickle
from datetime import date, timedelta
from itertools import pairwise

import polars as pl
from xgboost import XGBClassifier

from helpers import TrainStopStolli
from helpers.StationPhillip import StationPhillip
from ml_models.data_preperation import (
    apply_enum_encoding,
    filter_useful,
    generate_enum_encoding,
    prepare_progression_data,
)
from ml_models.training_stats import TrainingStats
from parser.gtfs_to_train_stops import download_last_two_weeks
from public_config import (
    CATEGORICAL_COLUMNS,
    DATA_SCHEMA_PATH,
    MODEL_PATH,
    TEST_DAYS,
    TRAIN_BATCHES,
    TRAIN_COLUMNS,
    TRAIN_DAYS,
)


def load_ml_data(
    service_date: date | None = None,
    min_date: date | None = None,
    max_date: date | None = None,
    include_join_ids: bool = False,
):
    stops = StationPhillip(prefer_cache=True).station_df.lazy()

    train_stops = TrainStopStolli.load_data(
        service_date=service_date, min_date=min_date, max_date=max_date
    )
    train_stops = filter_useful(train_stops)
    train_stops = prepare_progression_data(
        train_stops, stops=stops, include_join_ids=include_join_ids
    ).collect(streaming=True)
    return train_stops


def check_train_data(use_train_data_until: date):
    min_date = use_train_data_until - timedelta(days=TRAIN_DAYS * TRAIN_BATCHES)

    n_data_days = TrainStopStolli.days_of_data_available(
        min_date=min_date, max_date=use_train_data_until
    )

    assert (
        n_data_days >= (TRAIN_DAYS * TRAIN_BATCHES) - 3
    ), f'Not enough data available: wanted {TRAIN_DAYS * TRAIN_BATCHES} days, got {n_data_days}'


def load_train_data(use_train_data_until: date):
    train_days = timedelta(days=TRAIN_DAYS)

    enum_encoders = generate_enum_encoding(
        TrainStopStolli.load_data(
            min_date=use_train_data_until - train_days, max_date=use_train_data_until
        ),
        cat_cols=CATEGORICAL_COLUMNS,
    )

    train_stops: list[pl.DataFrame] = []
    for i in range(TRAIN_BATCHES):
        batch = load_ml_data(
            min_date=use_train_data_until - (train_days * (TRAIN_BATCHES - i + 1)),
            max_date=use_train_data_until - (train_days * (TRAIN_BATCHES - i)),
        )
        batch = apply_enum_encoding(batch, enum_encoders)
        train_stops.append(batch)

    for first_batch, second_batch in pairwise(train_stops):
        assert first_batch.schema == second_batch.schema, 'Schema mismatch'

    return train_stops


def load_train_test_data(split_date: date) -> tuple[list[pl.DataFrame], pl.DataFrame]:
    test_days = timedelta(days=TEST_DAYS)

    train_stops = load_train_data(split_date)

    test_stops = load_ml_data(min_date=split_date, max_date=split_date + test_days)
    test_stops = test_stops.cast(train_stops[0].schema, strict=False)

    return train_stops, test_stops


def train_model(use_train_data_until: date):
    train = load_train_data(use_train_data_until)

    training_stats = TrainingStats.start_training(
        n_batches=len(train), n_data_points=sum(len(batch) for batch in train)
    )

    hypertuned_params = {
        'n_estimators': 139,
        'lambda': 0.00040235587631109906,
        'alpha': 0.7157812006875595,
        'subsample': 0.22248557141961095,
        'colsample_bytree': 0.6843943490843347,
        'max_depth': 11,
        'min_child_weight': 9,
        'eta': 0.07934570401794394,
        'gamma': 0.11070993071085203,
    }
    est = XGBClassifier(
        n_jobs=-1,
        objective='multi:softmax',
        num_class=34,
        eval_metric='logloss',
        tree_method='hist',
        random_state=0,
        device='cuda',
        enable_categorical=True,
        **hypertuned_params,
    )

    for train_batch in train:
        est.fit(
            train_batch.drop(['delay_diff']).to_pandas(),
            train_batch.select('delay_diff').get_column('delay_diff').to_pandas(),
        )

    training_stats.end_training()
    pickle.dump(train[0].select(TRAIN_COLUMNS).schema, open(DATA_SCHEMA_PATH, 'wb'))
    est.save_model(MODEL_PATH)


def main(args):
    if args.train_today:
        download_last_two_weeks()
        check_train_data(date.today())
        train_model(date.today())


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        '--train-today',
        action='store_true',
        help='Train ml model on all data until today',
    )
    args = argument_parser.parse_args()

    main(args)
