from datetime import date, timedelta

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import polars as pl
import tueplots.bundles

from data_analysis.over_time import plot_over_time
from data_analysis.per_delay_diff import plot_per_delay
from ml_models.single_predictor import SinglePredictor
from ml_models.xgboost_single import load_ml_data
from public_config import OFFSET


def over_day_one_week(data: pl.DataFrame, title: str, save_as: str = None):
    predictor = SinglePredictor()

    probas = predictor.predict_probabilities(data)
    estimated_delay = probas @ np.arange(probas.shape[1])
    estimated_delay = estimated_delay - OFFSET

    data = data.with_columns(
        pl.Series('estimated_delay', estimated_delay),
        pl.col('delay_diff') - OFFSET,
        (
            pl.datetime(2000, 1, 1)
            + pl.duration(
                minutes=(pl.col('minute_of_day') // 20) * 20,
            )
        ).alias('x'),
    )
    data = (
        data.group_by('x')
        .agg(
            pl.col('estimated_delay').mean().alias('estimated_delay'),
            pl.len().alias('count'),
            pl.col('delay_diff').mean().alias('delay'),
        )
        .with_columns(
            (pl.col('count') / pl.col('count').sum()).alias('relative_count'),
        )
        .sort('x')
    )

    plot_over_time(
        data=data,
        fig_title=title,
        ax_title='20 Minuten Klassen',
        cols=['delay', 'estimated_delay'],
        col_labels=['Tatsächliche Verspätung', 'Vorhergesagte Verspätung'],
        y_axis_label='Ø Verspätung in Minuten',
        x_axis_label='Tageszeit',
        x_axis_locator=mdates.HourLocator(interval=4),
        x_axis_formatter=mdates.DateFormatter('%H:%M'),
        legend_ncols=3,
        save_as=save_as,
    )

    plot_over_time(
        data=data,
        fig_title=title,
        ax_title='20 Minuten Klassen',
        cols=['delay', 'estimated_delay'],
        col_labels=['Tatsächliche Verspätung', 'Vorhergesagte Verspätung'],
        col_hidden=[True, False],
        y_axis_label='Ø Verspätung in Minuten',
        x_axis_label='Tageszeit',
        x_axis_locator=mdates.HourLocator(interval=4),
        x_axis_formatter=mdates.DateFormatter('%H:%M'),
        legend_ncols=3,
        save_as=save_as + '_real_hidden.svg',
    )


def mse_mae(data: pl.DataFrame):
    predictor = SinglePredictor()

    probas = predictor.predict_probabilities(data)
    estimated_delay_diff = probas @ np.arange(probas.shape[1])
    estimated_delay_diff = estimated_delay_diff - OFFSET

    data = data.with_columns(
        pl.Series('estimated_delay_diff', estimated_delay_diff),
        pl.col('delay_diff') - OFFSET,
    )

    mse = data.select(
        ((pl.col('estimated_delay_diff') - pl.col('delay_diff')) ** 2).mean()
    ).item()
    mae = data.select(
        (pl.col('estimated_delay_diff') - pl.col('delay_diff')).abs().mean()
    ).item()

    mse_baseline = data.select(
        ((pl.col('delay_diff') - pl.col('delay_diff').mean()) ** 2).mean()
    ).item()
    mae_baseline = data.select(
        (pl.col('delay_diff') - pl.col('delay_diff').mean()).abs().mean()
    ).item()

    print(f'MSE: {mse}')
    print(f'MSE Baseline: {mse_baseline}')

    print(f'MAE: {mae}')
    print(f'MAE Baseline: {mae_baseline}')


def plot_average_histogram(data: pl.DataFrame, title: str, save_as: str):
    predictor = SinglePredictor()

    probas = predictor.predict_probabilities(data)

    data = (
        data.group_by('delay_diff')
        .agg(pl.len().alias('count'))
        .sort('delay_diff')
        .with_columns(
            (pl.col('count') / pl.col('count').sum()).alias('relative_count'),
            pl.Series('predicted_relative_count', probas.mean(axis=0)),
        )
        .with_columns(
            pl.col('delay_diff') - OFFSET,
        )
    )

    plot_per_delay(
        data=[
            data,
            data.drop('relative_count').rename(
                {'predicted_relative_count': 'relative_count'}
            ),
        ],
        labels=['Tatsächliche Abweichung', 'Vorhergesagte Abweichung'],
        title=title,
        loggy=False,
        save_as=save_as,
    )

    plot_per_delay(
        data=[
            data,
            data.drop('relative_count').rename(
                {'predicted_relative_count': 'relative_count'}
            ),
        ],
        labels=['Tatsächliche Abweichung', 'Vorhergesagte Abweichung'],
        data_hidden=[True, False],
        title=title,
        loggy=False,
        save_as=save_as + '_real_hidden.svg',
    )


def main():
    plt.rcParams.update(tueplots.bundles.beamer_moml())

    test_start = date(2024, 10, 1)
    test_end = test_start + timedelta(days=7)

    test_week_all = load_ml_data(min_date=test_start, max_date=test_end).filter(
        pl.col('minutes_to_prognosed_time') == 24 * 60
    )
    plot_average_histogram(
        test_week_all,
        title='Abweichung von DB-Verspätungsprognose',
        save_as='cache/plots/average_prediction_histogram_all.svg',
    )
    # over_day_one_week(
    #     test_week_all,
    #     title='Verspätungsvorhersage über den Tag',
    #     save_as='cache/plots/over_day_one_week_all.svg',
    # )
    # mse_mae(test_week_all)

    test_week_ice = load_ml_data(min_date=test_start, max_date=test_end).filter(
        (pl.col('minutes_to_prognosed_time') == 24 * 60) & (pl.col('category') == 'ICE')
    )
    plot_average_histogram(
        test_week_ice,
        title='Abweichung von DB-Verspätungsprognose (ICE)',
        save_as='cache/plots/average_prediction_histogram_ice.svg',
    )
    # over_day_one_week(
    #     test_week_ice,
    #     title='Verspätungsvorhersage über den Tag (ICE)',
    #     save_as='cache/plots/over_day_one_week_ice.svg',
    # )
    # mse_mae(test_week_ice)


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
