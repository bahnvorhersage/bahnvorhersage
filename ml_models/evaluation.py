import matplotlib.pyplot as plt
import numpy as np
import polars as pl


def plot_cdf(pred_cdf: np.ndarray, target_cdf: np.ndarray):
    plt.plot(pred_cdf, label='pred')
    plt.plot(target_cdf, label='target')

    plt.legend()
    plt.show()


def cdf_diff(pred_proba: np.array, target: np.array):
    target_proba = np.zeros_like(pred_proba)
    target_proba[np.arange(len(target)), target] = 1

    target_cdf = np.cumsum(target_proba, axis=1)
    pred_cdf = np.cumsum(pred_proba, axis=1)

    return np.mean(np.sum(np.abs(target_cdf - pred_cdf), axis=1) / target_cdf.shape[1])


def evaluate_prediction(pred_proba: np.array, target: np.array):
    target_proba = np.zeros_like(pred_proba)
    target_proba[np.arange(len(target)), target] = 1

    target_cdf = np.cumsum(target_proba, axis=1)
    pred_cdf = np.cumsum(pred_proba, axis=1)

    # plot_cdf(pred_cdf[0], target_cdf[0])
    # plot_cdf(pred_cdf[10], target_cdf[10])
    plot_cdf(np.mean(pred_cdf, axis=0), np.mean(target_cdf, axis=0))

    cdf_diff = np.mean(
        np.sum(np.abs(target_cdf - pred_cdf), axis=1) / target_cdf.shape[1]
    )

    pred_weighted_mean = pred_proba @ np.arange(pred_proba.shape[1])
    pred_cdf_median = np.abs(pred_cdf - 0.5).argmin(axis=1)
    pred_argmax = np.argmax(pred_proba, axis=1)

    pred_correct_baseline = np.mean(np.argmax(np.bincount(target)) == target)
    target_mean = np.mean(target)
    mae_baseline = np.mean(np.abs(target - target_mean))
    mse_baseline = np.mean((target - target_mean) ** 2)

    print(f'CDF diff: \t{cdf_diff:.3f}')
    print(
        f'Prediction weighted mean: {np.mean(pred_weighted_mean):.3f}, target mean: {target_mean:.3f}'
    )

    print('MAE:')
    print(f'Baseline: \t{mae_baseline:.3f}')
    print(f'Weighted mean: \t{np.mean(np.abs(pred_weighted_mean - target)):.3f}')
    print(f'CDF media label: \t{np.mean(np.abs(pred_cdf_median - target)):.3f}')
    print(f'Argmax: \t{np.mean(np.abs(pred_argmax - target)):.3f}')

    print('MSE:')
    print(f'Baseline: \t{mse_baseline:.3f}')
    print(f'Weighted mean: \t{np.mean((pred_weighted_mean - target) ** 2):.3f}')
    print(f'CDF media label: \t{np.mean((pred_cdf_median - target) ** 2):.3f}')
    print(f'Argmax: \t{np.mean((pred_argmax - target) ** 2):.3f}')

    print('Accuracy:')
    print(f'Majority baseline: \t{pred_correct_baseline:.3f}')
    print(
        f'Rounded weighted mean: \t{np.mean(np.round(pred_weighted_mean) == target):.3f}'
    )
    print(f'CDF media label: \t{np.mean(pred_cdf_median == target):.3f}')
    print(f'Argmax: \t{np.mean(pred_argmax == target):.3f}')


def evaluate_model(est, test: pl.DataFrame):
    pred_proba = est.predict_proba(test.drop(['delay_diff']).to_pandas())
    target = test.select('delay_diff').get_column('delay_diff').to_pandas()

    print(f'\nAll data ({len(target)} samples):')
    evaluate_prediction(pred_proba, target)

    mask = (
        test.select('minutes_to_prognosed_time').get_column('minutes_to_prognosed_time')
        <= 5
    )
    print(f'\n5 minutes to prognosed time ({mask.sum()} samples):')
    evaluate_prediction(pred_proba[mask], target[mask])

    mask = (
        test.select('minutes_to_prognosed_time')
        .get_column('minutes_to_prognosed_time')
        .is_between(6, 15)
    )
    print(f'\n6 to 15 minutes to prognosed time ({mask.sum()} samples):')
    evaluate_prediction(pred_proba[mask], target[mask])

    mask = (
        test.select('minutes_to_prognosed_time')
        .get_column('minutes_to_prognosed_time')
        .is_between(16, 30)
    )
    print(f'\n16 to 30 minutes to prognosed time ({mask.sum()} samples):')
    evaluate_prediction(pred_proba[mask], target[mask])

    mask = (
        test.select('minutes_to_prognosed_time')
        .get_column('minutes_to_prognosed_time')
        .is_between(31, 60)
    )
    print(f'\n31 to 60 minutes to prognosed time ({mask.sum()} samples):')
    evaluate_prediction(pred_proba[mask], target[mask])

    mask = (
        test.select('minutes_to_prognosed_time')
        .get_column('minutes_to_prognosed_time')
        .is_between(61, 120)
    )
    print(f'\n61 to 120 minutes to prognosed time ({mask.sum()} samples):')
    evaluate_prediction(pred_proba[mask], target[mask])

    mask = (
        test.select('minutes_to_prognosed_time')
        .get_column('minutes_to_prognosed_time')
        .is_between(121, 240)
    )
    print(f'\n121 to 240 minutes to prognosed time ({mask.sum()} samples):')
    evaluate_prediction(pred_proba[mask], target[mask])

    mask = (
        test.select('minutes_to_prognosed_time')
        .get_column('minutes_to_prognosed_time')
        .is_between(241, 60 * 24 - 1)
    )
    print(f'\n241 to 60*24-1 minutes to prognosed time ({mask.sum()} samples):')
    evaluate_prediction(pred_proba[mask], target[mask])

    mask = (
        test.select('minutes_to_prognosed_time').get_column('minutes_to_prognosed_time')
        >= 60 * 24
    )
    print(f'\n60*24 minutes to prognosed time ({mask.sum()} samples):')
    evaluate_prediction(pred_proba[mask], target[mask])

    mask = ~test.select('is_regional').get_column('is_regional')
    print(f'\nLong distance trains ({mask.sum()} samples):')
    evaluate_prediction(pred_proba[mask], target[mask])
