from datetime import date

import numpy as np
import optuna
from xgboost import XGBClassifier

from ml_models.xgboost_single import load_train_test_data


def tune_hyperparameters():
    train, test = load_train_test_data(date(2024, 10, 1))

    test_x = test.drop(['delay_diff']).to_pandas()
    test_y = test.select('delay_diff').get_column('delay_diff').to_pandas()

    def objective(trial: optuna.Trial):
        param = {
            'n_estimators': trial.suggest_int('n_estimators', 50, 200),
            # L2 regularization weight.
            'lambda': trial.suggest_float('lambda', 1e-8, 1.0, log=True),
            # L1 regularization weight.
            'alpha': trial.suggest_float('alpha', 1e-8, 1.0, log=True),
            # sampling ratio for training data.
            'subsample': trial.suggest_float('subsample', 0.2, 1.0),
            # sampling according to each tree.
            'colsample_bytree': trial.suggest_float('colsample_bytree', 0.2, 1.0),
            'max_depth': trial.suggest_int('max_depth', 3, 12),
            # minimum child weight, larger the term more conservative the tree.
            'min_child_weight': trial.suggest_int('min_child_weight', 2, 10),
            'eta': trial.suggest_float('eta', 1e-8, 1.0, log=True),
            # defines how selective algorithm is.
            'gamma': trial.suggest_float('gamma', 1e-8, 1.0, log=True),
        }

        model = XGBClassifier(
            n_jobs=-1,
            objective='multi:softmax',
            num_class=34,
            eval_metric='logloss',
            tree_method='hist',
            random_state=0,
            device='cuda',
            enable_categorical=True,
            **param,
        )

        for batch in train:
            model.fit(
                batch.drop(['delay_diff']).to_pandas(),
                batch.select('delay_diff').get_column('delay_diff').to_pandas(),
            )

        pred_proba = model.predict_proba(test_x)
        pred_weighted_mean = pred_proba @ np.arange(pred_proba.shape[1])
        mse = np.mean((pred_weighted_mean - test_y) ** 2)

        return mse

    study_name = 'xgboost_single_hyper_tuning'
    storage_name = f'sqlite:///{study_name}.db'
    study = optuna.create_study(
        study_name=study_name,
        storage=storage_name,
        direction='minimize',
        load_if_exists=True,
    )
    study.optimize(objective, n_trials=100, gc_after_trial=True)

    print('Best hyperparameters:', study.best_params)
    print('Best MSE:', study.best_value)
