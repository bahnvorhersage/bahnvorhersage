import polars as pl

from public_config import LABEL_COLUMN, OFFSET, TRAIN_COLUMNS


def bearing(df: pl.LazyFrame):
    """Calculate bearing angle in degree between two points.

    Parameters
    ----------
    df : pl.LazyFrame
        df with columns 'lat_start', 'lon_start', 'lat_end', 'lon_end'
    """
    # Formula
    # bearing_rad =
    #   atan2(
    #       cos(lat_end) * sin(lon_end - lon_start),
    #       cos(lat_start) * sin(lat_end)
    #       - sin(lat_start) * cos(lat_end) * cos(lon_end - lon_start)
    #   )
    return df.with_columns(
        pl.arctan2(
            (pl.col('lon_end') - pl.col('lon_start')).radians().sin()
            * pl.col('lat_end').radians().cos(),
            pl.col('lat_start').radians().cos() * pl.col('lat_end').radians().sin()
            - pl.col('lat_start').radians().sin()
            * pl.col('lat_end').radians().cos()
            * (pl.col('lon_end') - pl.col('lon_start')).radians().cos(),
        )
        .degrees()
        .round()
        .cast(pl.Int16)
        .alias('bearing')
    )


def filter_useful(train_stops: pl.LazyFrame) -> pl.LazyFrame:
    # Filtering out some S-Bahn trains (60%) reduces the data size
    # by about 30%
    sbahn_out = (
        pl.col('trip_id')
        .filter(pl.col('category') == 'S')
        .unique()
        .sample(fraction=0.8, seed=0)
    )

    rb_out = (
        pl.col('trip_id')
        .filter(pl.col('category') == 'RB')
        .unique()
        .sample(fraction=0.5, seed=0)
    )

    re_out = (
        pl.col('trip_id')
        .filter(pl.col('category') == 'RE')
        .unique()
        .sample(fraction=0.4, seed=0)
    )

    other_out = (
        pl.col('trip_id')
        .filter(~pl.col('category').is_in(['S', 'RB', 'RE']) & pl.col('is_regional'))
        .unique()
        .sample(fraction=0.2, seed=0)
    )

    out = pl.concat([sbahn_out, rb_out, re_out, other_out])

    # Filter out data points where trains do not stop
    return train_stops.filter(
        ~pl.col('is_betriebshalt')
        & (pl.col('pickup_drop_off_type') != 'NOT_AVAILABLE')
        & ~pl.col('is_cancelled')
        & ~pl.col('trip_id').is_in(out)
    )


def prepare_progression_data(
    train_stops: pl.LazyFrame, stops: pl.LazyFrame, include_join_ids: bool = False
) -> pl.LazyFrame:
    # Filter out data points that arrived way to early
    # TODO: Fix EN 40414 beeing multiple days to early (trip_id: 6325477523804418803)
    train_stops = train_stops.filter(pl.col('delay') >= -100_000)

    # Add final delay and time
    final_train_stops = (
        train_stops.filter(pl.col('is_final'))
        .select(['trip_id', 'stop_sequence', 'is_arrival', 'delay', 'time_real'])
        .rename({'delay': 'delay_final', 'time_real': 'time_final'})
    )
    train_stops = train_stops.join(
        final_train_stops, on=['trip_id', 'stop_sequence', 'is_arrival'], how='left'
    )

    # Filter out data points were the update timestamp is after the final time
    # (not predictable)
    train_stops = train_stops.filter(
        (pl.col('update_timestamp') <= pl.col('time_final')) | pl.col('is_final')
    )

    # Add start stop id
    start_stop_ids = train_stops.group_by('trip_id').agg(
        pl.col('stop_id')
        .sort_by(pl.col('stop_sequence'))
        .first()
        .alias('start_stop_id')
    )
    train_stops = train_stops.join(start_stop_ids, on='trip_id', how='left')

    # Add bearing
    train_stops = train_stops.join(
        stops.select(['eva', 'lat', 'lon']),
        left_on='trip_headsign',
        right_on='eva',
        suffix='_end',
        how='left',
    )
    train_stops = train_stops.join(
        stops.select(['eva', 'lat', 'lon']),
        left_on='start_stop_id',
        right_on='eva',
        suffix='_start',
        how='left',
    )
    train_stops = bearing(train_stops)

    # Add additional columns and cast to save memory
    train_stops = train_stops.with_columns(
        (pl.col('delay') // 60).cast(pl.Int16).alias('delay_prognosed'),
        (pl.col('delay_final') // 60).cast(pl.Int16).alias('delay_final'),
        ((pl.col('delay_final') - pl.col('delay')) // 60)
        .cast(pl.Int16)
        .alias('delay_diff'),
        pl.col('lat').cast(pl.Float32),
        pl.col('lon').cast(pl.Float32),
        (pl.col('dwell_time_schedule') // 60).cast(pl.Int16),
        (pl.col('dwell_time_real') // 60).cast(pl.Int16).alias('dwell_time_prognosed'),
        # Calculate minute of day. Type cast is necessary because .hour() and .minute()
        # return Int8, which cannot handle 24 * 60 = 1440
        (
            pl.col('time_schedule').dt.hour().cast(pl.Int16) * 60
            + pl.col('time_schedule').dt.minute().cast(pl.Int16)
        ).alias('minute_of_day'),
        pl.col('time_schedule').dt.weekday().alias('weekday'),
        ((pl.col('time_real') - pl.col('update_timestamp')).dt.total_seconds() // 60)
        .cast(pl.Int32)
        .alias('minutes_to_prognosed_time'),
        pl.col('number').cast(pl.Int32),
    )

    # Remove real time data from final data points
    final_train_stops = train_stops.filter(pl.col('is_final')).with_columns(
        pl.lit(0).cast(pl.Int16).alias('delay_prognosed'),
        pl.col('delay_final').alias('delay_diff'),
        pl.col('dwell_time_schedule').cast(pl.Int16).alias('dwell_time_prognosed'),
        pl.lit(60 * 24).cast(pl.Int32).alias('minutes_to_prognosed_time'),
    )
    # Filter out data points were the update timestamp is after the final time
    # (not predictable)
    train_stops = train_stops.filter(pl.col('update_timestamp') <= pl.col('time_final'))
    # Concatenate final data points with the rest
    train_stops = pl.concat([train_stops, final_train_stops])

    train_stops = train_stops.select(
        TRAIN_COLUMNS + [LABEL_COLUMN] + (['trip_id'] if include_join_ids else []),
    )

    # 98% bounds are between -5 and +40 minutes delay_diff,
    # 97% bounds are between -3 and +30 minutes delay_diff,
    # 95% bounds are between -2 and +20 minutes delay_diff,
    # 90% bounds are between -1 and +15 minutes delay_diff,
    # xgboost needs all positive classes, so clip and shift
    train_stops = train_stops.filter(
        pl.col('delay_diff').is_between(-3, 30)
    ).with_columns(pl.col('delay_diff') + OFFSET)

    return train_stops


def generate_enum_encoding(df: pl.LazyFrame, cat_cols: list[str]):
    enum_map = {
        col: pl.Enum(
            df.select(col).unique().collect(streaming=True).get_column(col).to_list()
        )
        for col in cat_cols
    }

    return enum_map


def create_enum_encoding(df: pl.DataFrame, cat_cols: list[str]):
    enum_map = {col: pl.Enum(df[col].unique().to_list()) for col in cat_cols}
    df = df.with_columns(
        [pl.col(col_name).cast(enum) for col_name, enum in enum_map.items()]
    )

    return df, enum_map


def apply_enum_encoding(df: pl.DataFrame, enum_map: dict[str, pl.Enum]):
    return df.with_columns(
        [
            pl.col(col_name).cast(enum, strict=False)
            for col_name, enum in enum_map.items()
        ]
    )
