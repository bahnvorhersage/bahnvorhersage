from datetime import date
from itertools import pairwise

import numpy as np
import sqlalchemy
import sqlalchemy.orm


def get_date_limits(session: sqlalchemy.orm.Session, column) -> tuple[date, date]:
    """
    For a given date column, calculate lower and upper date limits.

    Parameters
    ----------
    session : Session
        sqlalchemy session to query the database
    column :
        Column to calculate date limits for, must be a date column

    Returns
    -------
    list[tuple[int, int]]
        Date limits for the given column
    """
    minimum, maximum = session.query(
        sqlalchemy.func.min(column),
        sqlalchemy.func.max(column),
    ).first()
    return (minimum, maximum)


def get_chunk_limits(
    session: sqlalchemy.orm.Session, column, n_rows_per_division=20_000
) -> list[tuple[int, int]]:
    """
    For a given index column, calculate lower and upper index chunk limits,
    so that every chunk has about n_rows_per_division rows.

    Parameters
    ----------
    session : Session
        sqlalchemy session to query the database
    column :
        Column to calculate chunk limits for, must be a numeric column and should be
        evenly distributed
    n_rows_per_division : _type_, optional
        Approximate number of Rows to put in each chunk, not exact, by default 20_000

    Returns
    -------
    list[tuple[int, int]]
        Chunk limits for the given column
    """
    count, minimum, maximum = session.query(
        sqlalchemy.func.count(column),
        sqlalchemy.func.min(column),
        sqlalchemy.func.max(column),
    ).first()

    n_divisions = count // n_rows_per_division

    if n_divisions == 1:
        return [(minimum, maximum)]

    divisions = np.linspace(minimum, maximum, n_divisions, dtype=int).tolist()
    chunk_limits = [(lower, upper) for lower, upper in pairwise(divisions)]
    return chunk_limits


if __name__ == '__main__':
    from database.engine import sessionfactory
    from database.plan_by_id import PlanByIdV4

    _, Session = sessionfactory()

    with Session() as session:
        date_limits = get_date_limits(session, PlanByIdV4.service_date)
        print(date_limits)
        # chunk_limits = get_chunk_limits(session, Transfers.from_stop_id)
        # print(chunk_limits)
