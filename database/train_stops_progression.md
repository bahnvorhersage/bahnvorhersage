# Train stops progression

This file describes the data format used by Bahnvorhersage on disk for data analysis and machine learning. The data is split into gtfs-like `service_day`s. Each `service_day` contains all train stops of trains that started on `service_day`, but the stops itself might not be on `service_day`. The files are in parquet format. I like to use [polars](https://pola.rs/) to work with the data as it's performant and can do out of core computation through the streaming API.

For included columns and data types, please see `DATAFRAME_TYPES_PL` in [gtfs_to_train_stops.py](../parser/gtfs_to_train_stops.py). All durations are in seconds. Stop ids match the dataset found on [bahnvorhersage.de/stationviewer](https://bahnvorhersage.de/stationviewer).

`trip_headsign` also uses a stop id and is not a string (like it's supposed to be according to the gtfs-spec).

The codes in `operator` are resolved in [this repository](https://gitlab.com/bahnvorhersage/iris-owner-matchings).

Due to a bug, in there might be missing `stop_id` and route information like `line` and `number` in recent train stops.

When `is_betriebshalt` is True, the stop is not intended for passengers to enter or exit. I personally filter all of these stops away.

The data contains all the known delay prognoses. Time fields are a therefor a little confusing due to historical reasons. `time_real` does not necessarily contain the real time. It should be called `time_prognosed`. It only contains the real time if `is_final` is true.