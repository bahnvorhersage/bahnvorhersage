from collections.abc import Iterable
from datetime import UTC, date, datetime
from functools import cached_property

from sqlalchemy.orm import Mapped, Session, mapped_column
from sqlalchemy.types import BigInteger, DateTime

from api import iris
from database.base import Base
from helpers.group import group_list_of_dict
from helpers.hash64 import xxhash64
from helpers.json_storage import compress_json, decompress_json, hash_json

# Partman partitioning
# SELECT partman.create_parent(
#     p_parent_table := 'public.unique_change_v4'
#     , p_control := 'service_date'
#     , p_interval := '1 day'
#     , p_start_partition := '2021-09-01'
#     , p_premake := '60'
# );


class UniqueChangeV4(Base):
    __tablename__ = 'unique_change_v4'
    __table_args__ = {
        'postgresql_partition_by': 'RANGE (service_date)',
    }
    service_date: Mapped[date] = mapped_column(primary_key=True)
    hash_id: Mapped[int] = mapped_column(
        BigInteger, primary_key=True, autoincrement=False
    )
    change_hash: Mapped[int] = mapped_column(
        BigInteger, primary_key=True, autoincrement=False
    )
    stop_id: Mapped[int]
    time_crawled: Mapped[datetime] = mapped_column(DateTime(timezone=True))
    change_compressed: Mapped[bytes]

    def __init__(
        self, change: dict, stop_id: int, time_crawled: datetime = None
    ) -> None:
        try:
            self.service_date = iris.parse_id(change['id'])[1].date()
        except ValueError:
            self.service_date = iris.db_to_utc(change['ts']).date()
        self.hash_id = xxhash64(change['id'])
        self.stop_id = stop_id
        self.change_hash = hash_json(change)
        self.time_crawled = datetime.now(UTC) if time_crawled is None else time_crawled
        self.change_compressed = compress_json(change)

    @cached_property
    def change(self) -> dict:
        return decompress_json(self.change_compressed)

    def timetable_stop(self) -> iris.TimetableStop:
        return iris.TimetableStop(self.change)

    def compressed_timetable_stop(self) -> bytes:
        return compress_json(self.timetable_stop())

    def as_dict(self):
        return {
            'service_date': self.service_date,
            'hash_id': self.hash_id,
            'change_hash': self.change_hash,
            'stop_id': self.stop_id,
            'time_crawled': self.time_crawled,
            'change_compressed': self.change_compressed,
        }

    @staticmethod
    def count_at_date(session: Session, service_date: date) -> int:
        return (
            session.query(UniqueChangeV4)
            .filter(UniqueChangeV4.service_date == service_date)
            .count()
        )

    @staticmethod
    def get_stops_of_service_date(
        session: Session, service_date: date
    ) -> list['UniqueChangeV4']:
        return (
            session.query(UniqueChangeV4)
            .filter(UniqueChangeV4.service_date == service_date)
            .yield_per(500_000)
        )

    @staticmethod
    def get_stops_from_unparsed_ids(
        session: Session, unparsed_ids: list[dict]
    ) -> Iterable['UniqueChangeV4']:
        daygrouped_unparsed_ids = group_list_of_dict(unparsed_ids, key='service_date')
        for unparsed_ids_chunk in daygrouped_unparsed_ids.values():
            yield from UniqueChangeV4._unparsed_ids_chunk(session, unparsed_ids_chunk)

    @staticmethod
    def _unparsed_ids_chunk(
        session: Session, unparsed_ids: list[dict]
    ) -> list['UniqueChangeV4']:
        service_dates = set(u['service_date'] for u in unparsed_ids)
        change_hashes = set(u['change_hash'] for u in unparsed_ids)

        return (
            session.query(UniqueChangeV4)
            .filter(
                UniqueChangeV4.service_date.in_(service_dates),
                UniqueChangeV4.change_hash.in_(change_hashes),
            )
            .yield_per(100_000)
        )


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)
