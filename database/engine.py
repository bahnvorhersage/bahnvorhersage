import sqlalchemy
from sqlalchemy.orm import sessionmaker

from config import db_database, db_password, db_server, db_username

DB_URI = (
    'postgresql+psycopg2://'
    + db_username
    + ':'
    + db_password
    + '@'
    + db_server
    + '/'
    + db_database
    + '?sslmode=require'
)

PL_DB_URI = (
    'postgresql://'
    + db_username
    + ':'
    + db_password
    + '@'
    + db_server
    + '/'
    + db_database
    + '?sslmode=require'
)


def get_engine(**kwargs):
    return sqlalchemy.create_engine(DB_URI, pool_pre_ping=True, **kwargs)


def sessionfactory(**kwargs):
    engine = get_engine(**kwargs)
    Session = sessionmaker(bind=engine)
    return engine, Session
