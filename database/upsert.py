import csv
import io
import os
import random
import time
from typing import Literal

import sqlalchemy
from sqlalchemy.dialects.postgresql import Insert, insert
from sqlalchemy.orm import Session

from database.engine import get_engine
from helpers.batcher import batcher


def create_upsert_statement(
    table: sqlalchemy.sql.schema.Table,
    rows: list[dict],
    on_conflict_strategy: Literal['update', 'nothing'] = 'update',
) -> Insert:
    stmt = insert(table).values(rows)

    match on_conflict_strategy:
        case 'update':
            update_cols = [
                c.name for c in table.c if c not in list(table.primary_key.columns)
            ]

            on_conflict_stmt = stmt.on_conflict_do_update(
                index_elements=table.primary_key.columns,
                set_={k: getattr(stmt.excluded, k) for k in update_cols},
            )
        case 'nothing':
            on_conflict_stmt = stmt.on_conflict_do_nothing(
                index_elements=table.primary_key.columns
            )
        case _:
            raise ValueError(f'Invalid on_conflict_strategy: {on_conflict_strategy}')

    return on_conflict_stmt


def upsert_base(
    session: sqlalchemy.orm.Session,
    table: sqlalchemy.sql.schema.Table,
    rows: list[dict],
    on_conflict_strategy: Literal['update', 'nothing'] = 'update',
) -> None:
    """Upsert rows to table using session"""

    on_conflict_stmt = create_upsert_statement(table, rows, on_conflict_strategy)
    session.execute(on_conflict_stmt)


def upsert_with_retry(
    engine: sqlalchemy.engine.Engine,
    table: sqlalchemy.sql.schema.Table,
    rows: list[dict],
    on_conflict_strategy: Literal['update', 'nothing'] = 'update',
):
    for row_batch in batcher(rows, 30_000):
        while True:
            try:
                with Session(engine) as session:
                    upsert_base(session, table, row_batch, on_conflict_strategy)
                    session.commit()
                    break
            except sqlalchemy.exc.OperationalError as ex:
                if 'deadlock detected' in ex.args[0]:
                    timeout = random.randint(20, 80)
                    print(
                        f'{table.fullname} deadlock detected. Waiting {timeout} seconds.'
                    )
                    time.sleep(timeout)
                elif 'QueryCanceled' in ex.args[0]:
                    print(f'{table.fullname} QueryCanceled due to timeout. Retrying.')
                    time.sleep(120)
                else:
                    raise ex


class psql_csv_dialect(csv.Dialect):
    """Describe the usual properties of Unix-generated CSV files."""

    delimiter = ','
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\n'
    quoting = csv.QUOTE_MINIMAL


def tuples_to_csv(tuples: list[tuple]) -> str:
    """Convert a list of tuples to a csv string

    Parameters
    ----------
    tuples : List[tuple]
        list of tuples to convert to csv

    Returns
    -------
    str
        csv string
    """
    fbuf = io.StringIO()
    writer = csv.writer(fbuf, dialect=psql_csv_dialect)
    writer.writerows(tuples)
    fbuf.seek(0)
    return fbuf.read()


def upsert_copy_from(
    table: sqlalchemy.schema.Table,
    csv: str,
    on_conflict_strategy: Literal['update', 'nothing'] = 'update',
    engine=None,
):
    if engine is None:
        engine = get_engine()

    temp_table_name = f'temp_{table.name}_{os.urandom(4).hex()}'

    match on_conflict_strategy:
        case 'update':
            # INSTERT INTO table SELECT * FROM temp_table ON CONFLICT DO UPDATE
            # Insert the data from the temporary table into the real table using raw sql
            update_cols = [
                c.name for c in table.c if c not in list(table.primary_key.columns)
            ]

            sql_insert = f"""
                INSERT INTO {table.fullname}
                SELECT * FROM {temp_table_name}
                ON CONFLICT ({', '.join(table.primary_key.columns.keys())}) DO UPDATE
                SET {', '.join([f'{col} = excluded.{col}' for col in update_cols])}
            """

        case 'nothing':
            sql_insert = f"""
                INSERT INTO {table.fullname}
                SELECT * FROM {temp_table_name}
                ON CONFLICT ({', '.join(table.primary_key.columns.keys())}) DO NOTHING 
            """

        case _:
            raise ValueError(f'Invalid on_conflict_strategy: {on_conflict_strategy}')

    with Session(engine) as session:
        session.execute(
            sqlalchemy.text(
                f'CREATE TEMP TABLE {temp_table_name} AS TABLE {table.fullname} WITH NO DATA'
            )
        )
        # Use copy from to insert the date into a temporary table
        cursor = session.connection().connection.cursor()

        fbuf = io.StringIO(csv)
        cursor.copy_from(fbuf, temp_table_name, sep=',', null='')

        session.execute(sqlalchemy.text(sql_insert))
        session.commit()
