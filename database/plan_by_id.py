from collections.abc import Iterable
from datetime import date
from functools import cached_property
from itertools import batched

import sqlalchemy
import sqlalchemy.orm
from sqlalchemy import BIGINT
from sqlalchemy.orm import Mapped, mapped_column

from api import iris
from database.base import Base
from helpers.hash64 import xxhash64
from helpers.json_storage import compress_json, decompress_json

# Partman partitioning
# SELECT partman.create_parent(
#     p_parent_table := 'public.plan_by_id_v4'
#     , p_control := 'service_date'
#     , p_interval := '1 day'
#     , p_start_partition := '2021-09-01'
#     , p_premake := '60'
# );


class PlanByIdV4(Base):
    __tablename__ = 'plan_by_id_v4'
    __table_args__ = {
        'postgresql_partition_by': 'RANGE (service_date)',
    }
    service_date: Mapped[date] = mapped_column(primary_key=True)
    hash_id: Mapped[int] = mapped_column(BIGINT, primary_key=True, autoincrement=False)
    stop_id: Mapped[int] = mapped_column(BIGINT)
    plan_compressed: Mapped[bytes]

    def __init__(self, plan: dict, stop_id: int) -> None:
        self.service_date = iris.parse_id(plan['id'])[1].date()
        self.hash_id = xxhash64(plan['id'])
        self.stop_id = stop_id
        self.plan_compressed = compress_json(plan)

    @cached_property
    def plan(self) -> dict:
        return decompress_json(self.plan_compressed)

    def as_dict(self):
        return {
            'service_date': self.service_date,
            'hash_id': self.hash_id,
            'stop_id': self.stop_id,
            'plan_compressed': self.plan_compressed,
        }

    def timetable_stop(self) -> iris.TimetableStop:
        return iris.TimetableStop(self.plan)

    def compressed_timetable_stop(self) -> bytes:
        return compress_json(self.timetable_stop())

    @staticmethod
    def count_at_date(session: sqlalchemy.orm.Session, service_date: date) -> int:
        return (
            session.query(PlanByIdV4)
            .filter(PlanByIdV4.service_date == service_date)
            .count()
        )

    @staticmethod
    def get_stops_of_service_date(
        session: sqlalchemy.orm.Session, service_date: date
    ) -> list['PlanByIdV4']:
        return (
            session.query(PlanByIdV4)
            .filter(PlanByIdV4.service_date == service_date)
            .yield_per(100_000)
        )

    @staticmethod
    def get_stops_from_unparsed_ids(
        session: sqlalchemy.orm.Session, unparsed_ids: list[dict]
    ) -> Iterable['PlanByIdV4']:
        for ids_chunk in batched(unparsed_ids, 10_000):
            yield from PlanByIdV4._unparsed_ids_chunk(session, ids_chunk)

    @staticmethod
    def _unparsed_ids_chunk(
        session: sqlalchemy.orm.Session, unparsed_ids: list[dict]
    ) -> list['PlanByIdV4']:
        service_dates = set(u['service_date'] for u in unparsed_ids)
        hash_ids = set(u['hash_id'] for u in unparsed_ids)

        return (
            session.query(PlanByIdV4)
            .filter(
                PlanByIdV4.service_date.in_(service_dates),
                PlanByIdV4.hash_id.in_(hash_ids),
            )
            .all()
        )
