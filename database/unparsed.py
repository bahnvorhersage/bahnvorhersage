from datetime import date

from redis import Redis

from database.plan_by_id import PlanByIdV4
from database.unique_change import UniqueChangeV4


def add_change(redis_client: Redis, changes: list[UniqueChangeV4]) -> None:
    if changes:
        pipe = redis_client.pipeline()
        for change in changes:
            pipe.xadd(
                'unparsed_change',
                {
                    'change_hash': change.change_hash.to_bytes(8, 'big', signed=True),
                    'service_date': change.service_date.isoformat().encode('utf-8'),
                },
                maxlen=1_000_000,
                approximate=True,
            )
            if 'from' in change.change:
                pass
            else:
                pipe.xadd(
                    'message_queue_change',
                    {
                        'service_date': change.service_date.isoformat().encode('utf-8'),
                        'hash_id': change.hash_id.to_bytes(8, 'big', signed=True),
                        'change_hash': change.change_hash.to_bytes(
                            8, 'big', signed=True
                        ),
                        'stop_id': change.stop_id.to_bytes(4, 'big', signed=True),
                        'time_crawled': change.time_crawled.isoformat().encode('utf-8'),
                        'change_compressed': change.compressed_timetable_stop(),
                    },
                    maxlen=2_000_000,
                    approximate=True,
                )
        pipe.execute()


def add_plan(redis_client: Redis, plans: list[PlanByIdV4]) -> None:
    if plans:
        pipe = redis_client.pipeline()
        for plan in plans:
            pipe.xadd(
                'unparsed_plan',
                {
                    'hash_id': plan.hash_id.to_bytes(8, 'big', signed=True),
                    'service_date': plan.service_date.isoformat().encode('utf-8'),
                },
                maxlen=1_000_000,
                approximate=True,
            )

            pipe.xadd(
                'message_queue_plan',
                {
                    'hash_id': plan.hash_id.to_bytes(8, 'big', signed=True),
                    'service_date': plan.service_date.isoformat().encode('utf-8'),
                    'stop_id': plan.stop_id.to_bytes(4, 'big', signed=True),
                    'plan_compressed': (plan.compressed_timetable_stop()),
                },
                maxlen=500_000,
                approximate=True,
            )
        pipe.execute()


def get_change(redis_client: Redis, from_id: bytes) -> tuple[bytes, list[dict]]:
    resp = redis_client.xread({'unparsed_change': from_id})
    if resp:
        changes = []
        for last_id, data in resp[0][1]:
            changes.append(
                {
                    'change_hash': int.from_bytes(data[b'change_hash'], signed=True),
                    'service_date': date.fromisoformat(
                        data[b'service_date'].decode('utf-8')
                    ),
                }
            )
        return last_id, changes
    else:
        return from_id, []


def get_plan(redis_client: Redis, from_id: bytes) -> tuple[bytes, list[dict]]:
    resp = redis_client.xread({'unparsed_plan': from_id})
    if resp:
        plans = []
        for last_id, data in resp[0][1]:
            plans.append(
                {
                    'hash_id': int.from_bytes(data[b'hash_id'], signed=True),
                    'service_date': date.fromisoformat(
                        data[b'service_date'].decode('utf-8')
                    ),
                }
            )
        return last_id, plans
    else:
        return from_id, []
