# Realtime streams of unparsed data

Two realtime streams of the unparsed data exist. `message_queue_plan` holds the last 500,000 gathered plans. `message_queue_change` holds the last 2,000,000 changes to the plan data. Exact details on how the data is encoded using Python can be found in [this file](unparsed.py).

## `message_queue_plan`

`message_queue_plan` has the following fields:

- **hash_id**: Hash of the full internal IRIS id. Usefull to match changes to plans. Encoded as singed 8 bytes
- **service_date**: Date of initial departure of train according to IRIS. ISO datetime without timezone
- **stop_id**: EVA id of stop. EVA ids might not always perfectly match DB official sources. Please refer to https://bahnvorhersage.de/stationviewer for exact data used. Encoded as singed 4 bytes
- **plan_compressed**: zstd compressed json parsed from IRIS xml. [Python Code for compression and decompression found here](../helpers/json_storage.py)


## `message_queue_change`

`message_queue_change` has the following fields:

- **hash_id**: See plan
- **service_date**: See plan
- **change_hash**: Hash of the change json. Useful for checking whether two changes are identical. Encoded as 8 signed bytes
- **stop_id**: See plan
- **time_crawled**: ISO UTC datetime indicating the time the change was crawled, should be similar to the time it was published by IRIS
- **change_compressed**: See `plan_compressed`