# Kaniko

Damit Kaniko funktioniert, muss das passende secret bestehen:


```zsh
kubectl create secret docker-registry regcred --docker-server=https://index.docker.io/v1/ --docker-username=<username> --docker-password=<password-or-token> --docker-email=<email>
``` 