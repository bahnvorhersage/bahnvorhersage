# Redis

Redis wird ganz einfach über `redis.yaml` erstellt. Dafür muss die config `redis-config.yaml` applied sein.

Für db-rest braucht es die extra Datei `redis_secret.yaml` die wie folgt ausschaut:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: redis
type: Opaque
data:
  username: base64 encoded username
  password: base64 encoded password
  url: base64 encoded url
```