import json

import matplotlib.pyplot as plt
import polars as pl
import tueplots.bundles


def main():
    plt.rcParams.update(tueplots.bundles.beamer_moml())

    logs = json.load(open('db_rest_log.json'))

    cleaned_logs = {
        'level': [],
        'time': [],
    }
    for log in logs:
        cleaned_logs['level'].append(log['level'])
        cleaned_logs['time'].append(log['time'])

    logs_df = pl.DataFrame(cleaned_logs)
    logs_df = logs_df.with_columns(pl.from_epoch(pl.col('time'), time_unit='ms'))

    stats = (
        logs_df.group_by(pl.col('time').dt.truncate('1h'))
        .agg(
            (pl.col('level') == 30).sum().alias('request_count'),
            (pl.col('level') == 50).sum().alias('error_count'),
        )
        .sort('time')
    )

    fig, ax = plt.subplots()

    ax.step(stats['time'], stats['request_count'], label='Request count', where='mid')
    ax.step(stats['time'], stats['error_count'], label='Error count', where='mid')
    ax.grid(True)
    ax.set_xlim(stats['time'].min(), stats['time'].max())
    # ax.set_xlim(datetime(2025, 1, 11, 17, 30), datetime(2025, 1, 11, 18, 00))
    ax.set_ylim(0, None)
    fig.legend()

    plt.savefig('db_rest_logs.svg')

    print(stats)


if __name__ == '__main__':
    main()
