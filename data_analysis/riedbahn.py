import io
from datetime import date, datetime

import matplotlib
import matplotlib.pyplot as plt
import polars as pl
import pytz
import tueplots.bundles
from matplotlib.figure import Figure
from PIL import Image

from data_analysis import map
from helpers import TrainStopStolli

BBOX_RIEDBAHN = (
    8.1494,
    8.9858,
    49.3352,
    50.1506,
)

matplotlib.use('Agg')
# plt.style.use('dark_background')


def fig_to_webp_in_memory(fig: Figure) -> io.BytesIO:
    """Convert fig to webp in memory"""
    png = io.BytesIO()
    fig.savefig(
        png,
        bbox_inches='tight',
        dpi=700,
        format='png',
    )
    image = Image.open(png)
    image = image.convert('RGBA')
    webp = io.BytesIO()
    image.save(webp, 'webp')
    return webp


def pre_aggregate_data():
    train_stops = (
        TrainStopStolli.load_data(min_date=date(2024, 1, 1))
        .filter(
            (pl.col('lon') > BBOX_RIEDBAHN[0] - 1)
            & (pl.col('lon') < BBOX_RIEDBAHN[1] + 1)
            & (pl.col('lat') > BBOX_RIEDBAHN[2] - 1)
            & (pl.col('lat') < BBOX_RIEDBAHN[3] + 1)
            & pl.col('is_final')
            & ~pl.col('is_cancelled')
        )
        .group_by([pl.col('stop_id'), pl.col('time_schedule').dt.round('1d')])
        .agg(
            pl.col('delay').mean().alias('delay'),
            # pl.col('is_cancelled').mean().alias('is_cancelled'),
            pl.col('delay').count().alias('count'),
            pl.col('lat').first().alias('lat'),
            pl.col('lon').first().alias('lon'),
        )
    )

    train_stops = train_stops.collect(streaming=True)
    # train_stops.write_database(
    #     'per_station_riedbahn', connection=DB_URI, if_table_exists='replace'
    # )
    return train_stops


def aggregate_data(pre_agg: pl.DataFrame, start_time: datetime, end_time: datetime):
    data = pre_agg.filter(
        (pl.col('time_schedule') >= start_time) & (pl.col('time_schedule') < end_time)
    )

    return data.group_by('stop_id').agg(
        (pl.col('delay') * pl.col('count')).sum() / pl.col('count').sum(),
        # (pl.col('is_cancelled') * pl.col('count')).sum() / pl.col('count').sum(),
        pl.col('count').sum(),
        pl.col('lat').first().alias('lat'),
        pl.col('lon').first().alias('lon'),
    )


class RiedbahnAnalysis:
    def __init__(self):
        self.fig, self.ax, self.cmap, self.sc, self.colorbar = map.create_base_plot(
            bbox=BBOX_RIEDBAHN,
            ylabel='Ø Verspätung in Minuten',
            color_scheme='light',
            alpha=0.9,
        )

        map.add_shapes(
            self.ax,
            read_rail_shapefiles(
                [
                    '/home/mctoel/geodata/Streckennetz DB/strecken_polyline.shp',
                ],
                bbox=BBOX_RIEDBAHN,
            )
            .geometry.force_2d()
            .to_list(),
            facecolor='none',
            edgecolor='grey',
        )

        # self.data = pl.read_database_uri(
        #     'SELECT * FROM per_station_riedbahn', uri=PL_DB_URI
        # )
        self.data = pre_aggregate_data()
        self.data.write_parquet('riedbahn.parquet')
        self.data = pl.read_parquet('riedbahn.parquet')
        self.max_time = self.data['time_schedule'].max()
        self.min_time = self.data['time_schedule'].min()

    def default(self):
        return self.plot(self.min_time, self.max_time)

    def plot(self, start_time: datetime, end_time: datetime):
        # Make times utc if they have no timezone
        local_tz = pytz.timezone('Europe/Berlin')
        if start_time.tzinfo is None:
            start_time = local_tz.localize(start_time).astimezone(pytz.utc)
        if end_time.tzinfo is None:
            end_time = local_tz.localize(end_time).astimezone(pytz.utc)

        data = aggregate_data(self.data, start_time, end_time)

        n_days = (end_time - start_time).days

        # Normalize the circle size to be the same no matter how many days of data were
        # requested. 2000 is the average max number of trains on the busiest station.
        # This is used to scale the size of the markers
        data = data.with_columns(
            [
                ((pl.col('count') * 70) / (n_days * 2000)).alias('size'),
                pl.col('delay') / 60,  # convert to minutes
            ]
        )

        # change the positions
        self.sc.set_offsets(
            list(zip(*map.CRS_TRANSFORMER.transform(data['lon'], data['lat'])))
        )
        # change the sizes
        self.sc.set_sizes(data['size'].to_numpy())
        # change the color
        self.sc.set_array(data['delay'].to_numpy())

        self.ax.set_title(
            start_time.astimezone(local_tz).strftime('%d.%m.%Y')
            + '-'
            + end_time.astimezone(local_tz).strftime('%d.%m.%Y'),
            fontsize=12,
        )
        plot = fig_to_webp_in_memory(self.fig)
        return plot


def read_rail_shapefiles(paths: list[str], bbox: tuple[float, float, float, float]):
    import geopandas as gpd
    import pandas as pd

    geo_dfs = [gpd.read_file(path) for path in paths]
    railways = pd.concat(geo_dfs, ignore_index=True).clip(bbox)
    # railways = railways[(railways['fclass'] == 'rail')]
    return railways


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    plt.rcParams.update(tueplots.bundles.beamer_moml())

    per_station = RiedbahnAnalysis()

    plot_image = per_station.plot(
        datetime(2024, 1, 1, tzinfo=pytz.utc),
        datetime(2024, 1, 22, tzinfo=pytz.utc),
    )
    with open('cache/plots/riedbahn/generalprobe.webp', 'wb') as f:
        f.write(plot_image.getvalue())

    plot_image = per_station.plot(
        datetime(2024, 1, 23, tzinfo=pytz.utc),
        datetime(2024, 7, 14, tzinfo=pytz.utc),
    )
    with open('cache/plots/riedbahn/before_generalsanierung.webp', 'wb') as f:
        f.write(plot_image.getvalue())

    plot_image = per_station.plot(
        datetime(2024, 7, 15, tzinfo=pytz.utc),
        datetime(2024, 12, 14, tzinfo=pytz.utc),
    )
    with open('cache/plots/riedbahn/during_generalsanierung.webp', 'wb') as f:
        f.write(plot_image.getvalue())

    plot_image = per_station.plot(
        datetime(2024, 12, 15, tzinfo=pytz.utc),
        datetime(2024, 12, 25, tzinfo=pytz.utc),
    )
    with open('cache/plots/riedbahn/after_generalsanierung.webp', 'wb') as f:
        f.write(plot_image.getvalue())
