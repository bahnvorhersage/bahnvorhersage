import os
from datetime import date
from functools import lru_cache

import matplotlib.pyplot as plt
import polars as pl
import tueplots.bundles

from helpers import TrainStopStolli

PLOT_ARGS = {
    'delay': {
        'y_axis_label': 'Ø Verspätung in Minuten',
        'y_col': 'delay',
    },
    'delay_var': {
        'y_axis_label': 'Delay variance in minutes$^2$',
        'y_col': 'delay_var',
    },
    'cancellation': {
        'y_axis_label': 'Ausfallrate',
        'y_col': 'is_cancelled',
    },
}


def categories_to_str(categories: tuple) -> str:
    return '_'.join(list(sorted(categories)))


def categories_to_text(categories: tuple) -> str:
    return ' & '.join(list(sorted(categories)))


@lru_cache(maxsize=1)
def aggregate_data(train_categories: tuple[str]) -> pl.DataFrame:
    BIN_SIZE = 300_000
    MAX_BINS = 101

    data_length = (
        TrainStopStolli.load_data(min_date=date(2021, 9, 1))
        .filter(
            TrainStopStolli.final_filter & pl.col('category').is_in(train_categories)
        )
        .limit(200_000_000)
        .select(pl.len())
        .collect(streaming=True)
        .item()
    )

    n_bins = min(data_length // BIN_SIZE, MAX_BINS)

    aggregated = (
        TrainStopStolli.load_data(min_date=date(2021, 9, 1))
        .filter(
            TrainStopStolli.final_filter & pl.col('category').is_in(train_categories)
        )
        .limit(200_000_000)
        .group_by(pl.col('distance_traveled').qcut(n_bins, allow_duplicates=True))
        .agg(
            (pl.col('delay').mean() / 60).alias('delay'),
            (pl.col('delay') / 60).var().alias('delay_var'),
            pl.col('delay').count().alias('count'),
            pl.col('is_cancelled').mean().alias('is_cancelled'),
            pl.col('distance_traveled').max().alias('upper_bound') / 1000,  # in km
        )
        .collect(streaming=True)
    )

    return aggregated.sort('upper_bound')


def plot(
    train_categories: tuple[str], y_col: str, y_axis_label: str, save_as: str = None
):
    plt.rcParams.update(tueplots.bundles.beamer_moml())

    data = aggregate_data(train_categories)
    data = data.filter(
        pl.col('upper_bound') != pl.col('upper_bound').max()
    )  # remove last bin

    fig, ax = plt.subplots()

    bins = [0] + data['upper_bound'].to_list()

    ax.stairs(data[y_col], bins, baseline=None)

    ax.grid(True)

    ax.set_xlim(bins[0], bins[-1])
    ax.set_ylim(0, data[y_col].max() * 1.1)

    ax.set_xlabel('Gefahrene Strecke seit Startbahnhof (km)')
    ax.set_ylabel(y_axis_label)
    fig.suptitle('Durchschnittliche Verspätung in Minuten über die Strecke')
    ax.set_title(
        f'{data['count'].sum():,} Datenpunkte ({categories_to_text(train_categories)}) in {len(bins) - 1} Klassen'
    )
    if save_as is not None:
        fig.savefig(save_as, dpi=700)
    else:
        plt.show()


def main():
    if not os.path.exists('cache/plots/over_distance'):
        os.makedirs('cache/plots/over_distance')

    train_category_groups = [
        tuple(['ICE']),
        tuple(['IC', 'EC']),
        tuple(['S']),
        tuple(['RE']),
    ]

    for train_categories in train_category_groups:
        print('Processing', categories_to_str(train_categories))

        plot(
            train_categories=train_categories,
            save_as=f'cache/plots/over_distance/delay_{categories_to_str(train_categories)}.svg',
            **PLOT_ARGS['delay'],
        )

        plot(
            train_categories=train_categories,
            save_as=f'cache/plots/over_distance/delay_var_{categories_to_str(train_categories)}.svg',
            **PLOT_ARGS['delay_var'],
        )

        plot(
            train_categories=train_categories,
            save_as=f'cache/plots/over_distance/cancellations_{categories_to_str(train_categories)}.svg',
            **PLOT_ARGS['cancellation'],
        )


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
