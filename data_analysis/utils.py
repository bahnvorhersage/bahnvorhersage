from datetime import date, timedelta

import polars as pl

from helpers import TrainStopStolli


def data_aggregator(
    filter_expr: pl.Expr,
    group_by: pl.Expr,
    min_date: date = None,
    max_date: date = None,
    service_date: date = None,
):
    data = (
        TrainStopStolli.load_data(
            min_date=min_date, max_date=max_date, service_date=service_date
        )
        .filter(filter_expr)
        .with_columns(
            (group_by).alias('x'),
        )
        .group_by('x')
        .agg(
            (pl.col('delay') / 60).mean().alias('delay'),
            # pl.col('delay').var().alias('delay_var'), # Not supported in streaming
            (pl.col('delay') <= (5 * 60)).mean().alias('on_time'),
            pl.len().alias('count'),
            pl.sum('is_final').alias('stop_count'),
            pl.col('is_cancelled').sum().alias('cancelled_count'),
            (
                pl.col('is_cancelled')
                & (
                    pl.col('time_schedule')
                    < (pl.col('update_timestamp') + timedelta(minutes=60 * 2))
                )
            )
            .sum()
            .alias('spontaneous_cancellation_count'),
        )
        .with_columns(
            (pl.col('count') / pl.col('count').sum()).alias('relative_count'),
            (pl.col('cancelled_count') / pl.col('stop_count')).alias(
                'cancellation_rate'
            ),
            (pl.col('spontaneous_cancellation_count') / pl.col('stop_count')).alias(
                'spontaneous_cancellation_rate'
            ),
        )
        .sort('x')
        .collect(streaming=True)
    )

    return data


def group_uncommon(data: pl.DataFrame, threshold: float):
    uncommon = data.filter(pl.col('relative_count') < threshold)
    common = data.filter(pl.col('relative_count') >= threshold)

    uncommon = uncommon.select(
        pl.lit('other').alias('x'),
        (pl.col('delay') * pl.col('count')).sum() / pl.col('count').sum(),
        (pl.col('on_time') * pl.col('count')).sum() / pl.col('count').sum(),
        pl.col('count').sum(),
        pl.col('stop_count').sum(),
        pl.col('cancelled_count').sum(),
        pl.col('spontaneous_cancellation_count').sum(),
        pl.col('relative_count').sum(),
        (pl.col('cancellation_rate') * pl.col('count')).sum() / pl.col('count').sum(),
        (pl.col('spontaneous_cancellation_rate') * pl.col('count')).sum()
        / pl.col('count').sum(),
    )

    return common.extend(uncommon).sort('x')
