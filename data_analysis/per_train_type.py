import matplotlib.pyplot as plt
import numpy as np
import polars as pl
import tueplots.bundles

from data_analysis.packed_bubbles import BubbleChart
from data_analysis.utils import data_aggregator, group_uncommon


def plot_per_category(
    data: pl.DataFrame,
    title: str,
    color_by: str,
    colorbar_label: str,
    save_as: str | None = None,
    clim: tuple[float, float] | None = None,
):
    coloring = data[color_by].to_numpy()

    # use_trains = np.logical_not(np.isnan(delays))
    # delays = delays[use_trains]

    fig, ax = plt.subplots(subplot_kw=dict(aspect='equal'))
    ax.set_title(title)
    ax.axis('off')

    count = data['count'].to_numpy()

    cmap = plt.cm.bwr
    # matplotlib.colors.LinearSegmentedColormap.from_list(
    #     '', ['green', 'yellow', 'red']
    # )
    if clim is not None:
        # cmap.set_over('red')
        # cmap.set_under('green')
        # cmap.set_bad('gray')
        coloring = np.clip(coloring, clim[0], clim[1])
        # cmap.set_clim(clim)

    bubble_plot = BubbleChart(area=count, bubble_spacing=600)
    bubble_plot.collapse(n_iterations=100)

    if clim is None:
        colors = [
            cmap(delay)
            for delay in (coloring - coloring.min()) / max(coloring - coloring.min())
        ]
    else:
        colors = [cmap(delay) for delay in (coloring - clim[0]) / (clim[1] - clim[0])]

    labels = data['x'].to_list()
    wanted_labels = set(['ICE', 'IC', 'EC', 'RE', 'RB', 'S', 'Bus', 'SBB'])
    labels = [label if label in wanted_labels else '' for label in labels]

    bubble_plot.plot(
        ax,
        labels=labels,
        colors=colors,
    )

    # scatter in order to set colorbar
    if clim is None:
        scatter = ax.scatter(
            np.zeros(len(coloring)), np.zeros(len(coloring)), s=0, c=coloring, cmap=cmap
        )
    else:
        scatter = ax.scatter(
            np.zeros(len(coloring)),
            np.zeros(len(coloring)),
            s=0,
            c=coloring,
            cmap=cmap,
            vmin=clim[0],
            vmax=clim[1],
        )

    colorbar = fig.colorbar(scatter)
    colorbar.solids.set_edgecolor('face')
    # colorbar.outline.set_linewidth(0)
    colorbar.ax.get_yaxis().labelpad = 15
    colorbar.ax.set_ylabel(colorbar_label, rotation=270)

    ax.relim()
    ax.autoscale_view()

    if save_as is not None:
        fig.savefig(save_as, dpi=700)
    else:
        plt.show()


def main():
    plt.rcParams.update(tueplots.bundles.beamer_moml())

    data = group_uncommon(
        data_aggregator(
            filter_expr=pl.col('is_final'),
            group_by=pl.col('category'),
            # min_date=date(2023, 9, 1),
        ),
        threshold=0.002,
    )
    data.write_parquet('cache/per_category.parquet')
    data = pl.read_parquet('cache/per_category.parquet')

    plot_per_category(
        data,
        'Ø Verspätung pro Zugtyp',
        color_by='delay',
        colorbar_label='Ø Verspätung in Minuten',
        save_as='cache/plots/per_train_type_delay.svg',
    )

    plot_per_category(
        data,
        'Ausfallrate pro Zugtyp',
        color_by='cancellation_rate',
        colorbar_label='Ausfallrate',
        save_as='cache/plots/per_train_type_cancellation.svg',
        clim=(0, 0.08),
    )

    plot_per_category(
        data,
        'Spontane Ausfallrate pro Zugtyp',
        color_by='spontaneous_cancellation_rate',
        colorbar_label='Spontane Ausfallrate',
        save_as='cache/plots/per_train_type_spontaneous_cancellation.svg',
        clim=(0, 0.08),
    )


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
