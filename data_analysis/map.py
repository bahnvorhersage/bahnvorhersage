from itertools import repeat

import cartopy
import cartopy.crs as ccrs
import cartopy.feature
import cartopy.mpl.geoaxes
import matplotlib.pyplot as plt
import numpy as np
import pyproj
from shapely.ops import clip_by_rect, transform

# Bounding Box of Germany
MIN_LON = 5.67
MAX_LON = 15.64
MIN_LAT = 47.06
MAX_LAT = 55.06

BBOX_GERMANY = (MIN_LON, MAX_LON, MIN_LAT, MAX_LAT)

CRS_TRANSFORMER = pyproj.Transformer.from_crs('EPSG:4326', 'EPSG:32632', always_xy=True)
MAP_CRS = ccrs.UTM(32)


def clip_to_plot(geom, bbox):
    clip_bbox = (
        bbox[0] - 1,
        bbox[1] + 1,
        bbox[2] - 1,
        bbox[3] + 1,
    )
    return clip_by_rect(geom, *clip_bbox)


def not_empty(geom):
    return not geom.is_empty


def add_feature(
    ax: cartopy.mpl.geoaxes.GeoAxes, feature_args, facecolor, edgecolor, bbox
):
    feature = cartopy.feature.NaturalEarthFeature(*feature_args)
    feature = list(
        filter(not_empty, map(clip_to_plot, feature.geometries(), repeat(bbox)))
    )
    add_shapes(ax, feature, facecolor, edgecolor)


def add_shapes(ax: cartopy.mpl.geoaxes.GeoAxes, shapes, facecolor, edgecolor):
    shapes = [transform(CRS_TRANSFORMER.transform, geom) for geom in shapes]
    ax.add_geometries(
        shapes,
        crs=MAP_CRS,
        facecolor=facecolor,
        edgecolor=edgecolor,
    )


def create_base_plot(
    bbox: tuple[float, float, float, float],
    ylabel: str,
    alpha: float = 0.5,
    color_scheme: str = 'dark',
):
    """Create pretty geo base plot."""

    if color_scheme == 'dark':
        map_colors = {
            'background': '#191a1a',
            'land': '#343332',
            'land_edge': '#5c5b5b',
            'states': '#444444',
            'state_borders': '#5c5b5b',
        }
    elif color_scheme == 'light':
        map_colors = {
            'background': '#ffffff',
            'land': (240 / 256, 240 / 256, 220 / 256),
            'land_edge': '#adb5bd',
            'states': 'lightgray',
            'state_borders': '#adb5bd',
        }
    else:
        raise ValueError(f'Unknown color scheme {color_scheme}')

    fig, ax = plt.subplots(subplot_kw={'projection': MAP_CRS})
    ax: cartopy.mpl.geoaxes.GeoAxes

    ax.set_extent(bbox, crs=ccrs.Geodetic())

    # Add natural earth features to the base plot. We don't use ax.add_feature here,
    # because it doesn't support shape clipping and therefore is way slower.
    # https://stackoverflow.com/questions/61157293/speeding-up-high-res-coastline-plotting-with-cartopy

    # Background
    ax.set_facecolor(map_colors['background'])
    ax.set_frame_on(False)

    # Land
    add_feature(
        ax,
        ('physical', 'land', '10m'),
        facecolor=map_colors['land'],
        edgecolor=map_colors['land_edge'],
        bbox=bbox,
    )

    # States
    add_feature(
        ax,
        ('cultural', 'admin_1_states_provinces_lakes', '10m'),
        facecolor=map_colors['land'],
        edgecolor=map_colors['states'],
        bbox=bbox,
    )

    # Borders
    add_feature(
        ax,
        ('cultural', 'admin_0_boundary_lines_land', '10m'),
        facecolor=map_colors['land'],
        edgecolor=map_colors['land_edge'],
        bbox=bbox,
    )

    cmap = plt.cm.bwr

    sc = ax.scatter(
        np.zeros(1),
        np.zeros(1),
        marker='o',
        edgecolors='none',
        c=np.zeros(1),
        s=np.zeros(1),
        cmap=cmap,
        vmin=0,
        vmax=7,
        alpha=alpha,
        zorder=10,
        transform=MAP_CRS,
    )

    colorbar = fig.colorbar(sc)
    # colorbar.solids.set_edgecolor('face')
    # colorbar.outline.set_linewidth(0)

    colorbar.ax.get_yaxis().labelpad = 15
    colorbar.ax.set_ylabel(ylabel=ylabel, rotation=270)

    return fig, ax, cmap, sc, colorbar
