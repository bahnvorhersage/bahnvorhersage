from datetime import datetime

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import matplotlib.ticker
import polars as pl
import tueplots.bundles

from data_analysis.db_stats import load_db_stats
from data_analysis.utils import data_aggregator
from helpers.plt_legend_top import LEG_TOP_KWARGS


def nice_upper_bound(max_value: float, n_ticks=5) -> float:
    scale = 1

    while max_value < 10:
        max_value *= 10
        scale /= 10

    while max_value > 100:
        max_value /= 10
        scale *= 10

    max_value = max_value // 1

    max_value += n_ticks - (max_value % n_ticks)

    return max_value * scale


def plot_over_time(
    data: pl.DataFrame,
    fig_title: str,
    ax_title: str,
    cols: list[str],
    col_labels: list[str],
    y_axis_label: str,
    x_axis_label: str,
    x_axis_locator: matplotlib.ticker.Locator,
    x_axis_formatter: matplotlib.ticker.Formatter,
    col_hidden: list[bool] | None = None,
    legend_ncols: int = 2,
    save_as: str | None = None,
):
    if col_hidden is None:
        col_hidden = [False] * len(cols)

    fig, ax1 = plt.subplots()
    ax1: plt.Axes
    ax2: plt.Axes = ax1.twinx()

    ax1.set_zorder(1)  # default zorder is 0 for ax1 and ax2
    # ax1.patch.set_visible(False)  # prevents ax1 from hiding ax2

    ax2.fill_between(
        data['x'],
        data['relative_count'],
        step='mid',
        label='Aktivität Zugnetz',
        color='lightgray',
    )
    for col, label, hidden in zip(cols, col_labels, col_hidden):
        ax1.step(data['x'], data[col], where='mid', label=label if not hidden else ' ')[
            0
        ].set_visible(not hidden)

    ax1.set_xlim(data['x'].min(), data['x'].max())
    ax2.set_xlim(data['x'].min(), data['x'].max())

    ax1.set_ylim(0, nice_upper_bound(max([data[col].max() for col in cols])))
    ax2.set_ylim(0, nice_upper_bound(data['relative_count'].max()))

    ax1.grid(True)
    # ax2.grid(True)

    # # Align grids of ax1 and ax2
    # ax1.yaxis.set_major_locator(matplotlib.ticker.LinearLocator(5))
    # ax2.yaxis.set_major_locator(matplotlib.ticker.LinearLocator(5))

    ax1.set_ylabel(y_axis_label)

    # ax2.set_ylabel('Relative Anzahl an Zughalten')
    ax2.set_axis_off()

    ax1.set_xlabel(x_axis_label)
    fig.suptitle(fig_title)
    ax1.set_title(ax_title)
    ax1.xaxis.set_major_formatter(x_axis_formatter)
    ax1.xaxis.set_major_locator(x_axis_locator)
    # ax2.xaxis.set_major_formatter(x_axis_formatter)
    # ax2.xaxis.set_major_locator(x_axis_locator)

    fig.legend(ncols=legend_ncols, **LEG_TOP_KWARGS)

    if save_as is not None:
        fig.savefig(save_as, dpi=700)
    else:
        plt.show()


def plot_over_hour(save_as: str | None = None):
    data = data_aggregator(
        filter_expr=pl.col('is_final'),
        group_by=pl.datetime(2000, 1, 1)
        + pl.duration(
            minutes=(pl.col('time_schedule').dt.minute() // 2) * 2,
        ),
    )

    plot_over_time(
        data=data,
        fig_title='Durchnittliche Verspätung über die Stunde',
        ax_title='2 Minuten Klassen',
        cols=['delay'],
        col_labels=['Verspätung'],
        y_axis_label='Ø Verspätung in Minuten',
        x_axis_label='Minute',
        x_axis_locator=mdates.MinuteLocator(byminute=[0, 10, 20, 30, 40, 50]),
        x_axis_formatter=mdates.DateFormatter('%M'),
        save_as=save_as,
    )


def plot_over_day(save_as: str | None = None):
    data = data_aggregator(
        filter_expr=pl.col('is_final'),
        group_by=pl.datetime(2000, 1, 1)
        + pl.duration(
            hours=pl.col('time_schedule').dt.hour(),
            minutes=(pl.col('time_schedule').dt.minute() // 20) * 20,
        ),
    )

    plot_over_time(
        data=data,
        fig_title='Durchnittliche Verspätung über den Tag',
        ax_title='20 Minuten Klassen',
        cols=['delay'],
        col_labels=['Verspätung'],
        y_axis_label='Ø Verspätung in Minuten',
        x_axis_label='Tageszeit',
        x_axis_locator=mdates.HourLocator(interval=4),
        x_axis_formatter=mdates.DateFormatter('%H:%M'),
        save_as=save_as,
    )


def plot_over_week(save_as: str | None = None):
    data = data_aggregator(
        filter_expr=pl.col('is_final'),
        group_by=pl.datetime(2000, 1, 2)
        + pl.duration(
            days=pl.col('time_schedule').dt.weekday(),
            hours=(pl.col('time_schedule').dt.hour() // 2) * 2,
        ),
    )

    plot_over_time(
        data=data,
        fig_title='Durchschnittliche Verspätung über die Woche',
        ax_title='2 Stunden Klassen',
        cols=['delay'],
        col_labels=['Verspätung'],
        y_axis_label='Ø Verspätung in Minuten',
        x_axis_label='Wochentag',
        x_axis_locator=mdates.HourLocator(byhour=0),
        x_axis_formatter=mdates.DateFormatter('%a'),
        save_as=save_as,
    )


def plot_over_year_weekly_count(
    hide_progression: bool = False, save_as: str | None = None
):
    data = data_aggregator(
        filter_expr=pl.lit(True), group_by=pl.col('time_schedule').dt.truncate('1w')
    )

    # Convert to millions
    data = data.with_columns(
        pl.col('count') / 1_000_000, pl.col('stop_count') / 1_000_000
    )

    fig, ax = plt.subplots()
    ax: plt.Axes

    ax.fill_between(data['x'], data['count'], step='mid', alpha=0.4).set_visible(
        not hide_progression
    )
    ax.fill_between(data['x'], data['stop_count'], step='mid', alpha=0.4)

    ax.step(
        data['x'],
        data['count'],
        where='mid',
        label='DB-Verspätungsprognosen' if not hide_progression else ' ',
    )[0].set_visible(not hide_progression)
    ax.step(data['x'], data['stop_count'], where='mid', label='Zughalte')

    ax.set_xlim(data['x'].min(), data['x'].max())
    ax.set_ylim(0, data['count'].max() * 1.1)
    ax.grid(True)

    ax.set_xlabel('Woche')
    ax.set_ylabel('Anzahl Datenpunkte pro Woche (Mio)')
    fig.suptitle('Datenpunkte pro Woche')
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %Y'))
    ax.xaxis.set_major_locator(mdates.MonthLocator(bymonth=[1, 7]))

    fig.legend(ncols=2, **LEG_TOP_KWARGS)

    if save_as is not None:
        fig.savefig(save_as, dpi=700)
    else:
        plt.show()


def plot_over_year_weekly_delay(save_as: str | None = None):
    data = data_aggregator(
        filter_expr=pl.col('is_final'),
        group_by=pl.col('time_schedule').dt.truncate('1w'),
    )

    plot_over_time(
        data=data,
        fig_title='Durchschnittliche Verspätung pro Woche',
        ax_title='',
        x_axis_label='Woche',
        cols=['delay'],
        col_labels=['Verspätung'],
        y_axis_label='Ø Verspätung in Minuten',
        x_axis_locator=mdates.MonthLocator(bymonth=[1, 7]),
        x_axis_formatter=mdates.DateFormatter('%b %Y'),
        save_as=save_as,
    )


def plot_over_year_weekly_cancellations(save_as: str | None = None):
    data = data_aggregator(
        filter_expr=pl.col('is_final') & (pl.col('category') == 'ICE'),
        group_by=pl.col('time_schedule').dt.truncate('1w'),
    )

    plot_over_time(
        data=data,
        fig_title='Cancellation rate',
        ax_title='',
        x_axis_label='Week',
        cols=['cancellation_rate', 'spontaneous_cancellation_rate'],
        col_labels=['Cancellations', 'Spontaneous cancellations'],
        y_axis_label='Cancellation rate',
        x_axis_locator=mdates.MonthLocator(bymonth=[1, 7]),
        x_axis_formatter=mdates.DateFormatter('%b %Y'),
        legend_ncols=3,
        save_as=save_as,
    )


def plot_over_year_monthly(db_visible: bool = True, save_as: str | None = None):
    arrival_data_total = data_aggregator(
        filter_expr=pl.col('is_final')
        & ~pl.col('is_betriebshalt')
        & pl.col('is_arrival')
        & (~pl.col('is_cancelled')),
        group_by=pl.col('time_schedule').dt.truncate('1mo'),
    )

    arrival_data_long_distance = data_aggregator(
        filter_expr=(
            pl.col('is_final')
            & ~pl.col('is_betriebshalt')
            & (pl.col('category').is_in(['ICE', 'IC', 'EC']))
            & (pl.col('operator') == '80')
            & pl.col('is_arrival')
            & (~pl.col('is_cancelled'))
        ),
        group_by=pl.col('time_schedule').dt.truncate('1mo'),
    )

    db_stats = load_db_stats().filter(pl.col('date') >= datetime(2021, 9, 1))

    fig, ax = plt.subplots()
    ax: plt.Axes

    ax.step(
        arrival_data_total['x'],
        arrival_data_total['on_time'],
        label='Eigene Daten (alle)',
        where='mid',
    )
    ax.step(
        arrival_data_long_distance['x'],
        arrival_data_long_distance['on_time'],
        label='Eigene Daten (Fernverkehr)',
        where='mid',
    )

    ax.step(
        db_stats['date'],
        db_stats['on_time'],
        label='DB (alle)' if db_visible else '         ',
        where='mid',
    )[0].set_visible(db_visible)
    ax.step(
        db_stats['date'],
        db_stats['on_time_long_distance'],
        label='DB (Fernverkehr)' if db_visible else '                ',
        where='mid',
    )[0].set_visible(db_visible)

    ax.set_xlim(arrival_data_total['x'].min(), arrival_data_total['x'].max())
    ax.set_ylim(0.4, 1)
    ax.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter(xmax=1))
    ax.grid(True)

    ax.set_xlabel('Monat')
    ax.set_ylabel('Ankunft pünktlich')
    fig.suptitle('Pünktlichkeit pro Monat')
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %Y'))
    ax.xaxis.set_major_locator(mdates.MonthLocator(bymonth=[1, 7]))

    fig.legend(ncols=4, **LEG_TOP_KWARGS)

    if save_as is not None:
        fig.savefig(save_as, dpi=700)
    else:
        plt.show()


def main():
    plt.rcParams.update(tueplots.bundles.beamer_moml())

    # plot_over_hour(save_as='cache/plots/over_hour.svg')

    # plot_over_day(save_as='cache/plots/over_day.svg')

    # plot_over_week(save_as='cache/plots/over_week.svg')

    plot_over_year_weekly_count(save_as='cache/plots/over_year_weekly_count.svg')
    plot_over_year_weekly_count(
        hide_progression=True,
        save_as='cache/plots/over_year_weekly_count_no_progression.svg',
    )

    # plot_over_year_weekly_delay(save_as='cache/plots/over_year_weekly_delay.svg')

    # plot_over_year_weekly_cancellations(
    #     save_as='cache/plots/over_year_weekly_cancellations.svg'
    # )

    # plot_over_year_monthly(save_as='cache/plots/over_year_monthly.svg')
    # plot_over_year_monthly(
    #     db_visible=False, save_as='cache/plots/over_year_monthly_only_own.svg'
    # )


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
