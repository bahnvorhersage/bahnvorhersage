from datetime import date

import matplotlib.pyplot as plt
import matplotlib.ticker
import polars as pl
import tueplots.bundles
from tqdm import tqdm

from helpers import TrainStopStolli
from helpers.date_range import date_range, date_range_len
from helpers.plt_legend_top import LEG_TOP_KWARGS
from helpers.StationPhillip import StationPhillip
from ml_models.data_preperation import prepare_progression_data
from public_config import OFFSET

GROUPER = [
    pl.col('delay_diff'),
    pl.col('category'),
    pl.col('minutes_to_prognosed_time')
    .cut([5, 15, 30, 60, 120, 240, 24 * 60 - 1])
    .alias('minutes_to_prognosed_time_bins'),
    pl.col('delay_prognosed').cut([0, 1, 3, 6, 15, 30]).alias('delay_prognosed_bins'),
]

GROUP_NAMES = [
    'delay_diff',
    'category',
    'minutes_to_prognosed_time_bins',
    'delay_prognosed_bins',
]


def data_prep():
    stops = StationPhillip(prefer_cache=True).station_df.lazy()

    day_chunks = []

    for service_date in tqdm(
        date_range(date(2023, 9, 1), date.today()),
        total=date_range_len(date(2023, 9, 1), date.today()),
    ):
        try:
            data = TrainStopStolli.load_data(service_date=service_date)
        except ValueError:
            continue
        data = (
            prepare_progression_data(data, stops=stops)
            .select(
                [
                    'delay_diff',
                    'category',
                    'delay_prognosed',
                    'minutes_to_prognosed_time',
                ]
            )
            .collect(streaming=False)
        )
        day_chunks.append(daily_data_aggregation(data))

    data: pl.DataFrame = pl.concat(day_chunks)
    data.write_parquet('cache/plots/per_delay_diff_chunks.parquet')
    data = pl.read_parquet('cache/plots/per_delay_diff_chunks.parquet')

    data = data.group_by(GROUP_NAMES).agg(pl.col('count').sum())

    return data


def daily_data_aggregation(
    data: pl.DataFrame,
):
    return (
        data.group_by(GROUPER)
        .agg(
            pl.len().alias('count'),
        )
        .with_columns(
            pl.col('delay_diff') - OFFSET,
        )
    )


def plot_per_delay(
    data: list[pl.DataFrame],
    labels: list[str],
    title: str,
    data_hidden: list[bool] | None = None,
    y_limits: tuple[float, float] | None = (0, 0.5),
    x_limits: tuple[float, float] | None = None,
    loggy: bool = False,
    save_as: str | None = None,
):
    if data_hidden is None:
        data_hidden = [False] * len(data)
    fig, ax = plt.subplots()
    ax: plt.Axes

    for df, label, hidden in zip(data, labels, data_hidden):
        ax.step(
            df['delay_diff'],
            df['relative_count'],
            where='mid',
            label=label if not hidden else ' ',
        )[0].set_visible(not hidden)
        ax.fill_between(
            df['delay_diff'], df['relative_count'], step='mid', alpha=0.3
        ).set_visible(not hidden)

    if x_limits is None:
        x_limits = (data[0]['delay_diff'].min(), data[0]['delay_diff'].max())

    ax.set_xlim(x_limits)
    ax.grid(True)

    ax.set_xlabel('Abweichung von DB-Verspätungsprognose in Minuten')
    ax.set_ylabel('Relative Häufigkeit')
    ax.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter(xmax=1))
    ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(1))

    if loggy:
        ax.set_yscale('log')
    else:
        if y_limits is not None:
            ax.set_ylim(y_limits)
        else:
            ax.set_ylim(bottom=0)

    fig.suptitle(title)

    fig.legend(ncols=3, **LEG_TOP_KWARGS)

    if save_as is not None:
        fig.savefig(save_as, dpi=700)
    else:
        plt.show()


def main():
    plt.rcParams.update(tueplots.bundles.beamer_moml())

    # data = data_prep()

    # data.write_parquet('cache/plots/per_delay_diff.parquet')
    data = pl.read_parquet('cache/plots/per_delay_diff.parquet')

    ### Total
    total = (
        data.group_by('delay_diff')
        .agg(pl.col('count').sum())
        .with_columns((pl.col('count') / pl.col('count').sum()).alias('relative_count'))
        .sort('delay_diff')
    )
    plot_per_delay(
        data=[total],
        labels=['Alle'],
        title='Abweichung von DB-Verspätungsprognose',
        loggy=False,
        x_limits=(-3, 15),
        save_as='cache/plots/per_delay_diff_total.svg',
    )

    ### Per train category
    per_category_counts = data.group_by('category').agg(
        pl.col('count').sum().alias('cat_count')
    )
    per_category = (
        data.group_by(['delay_diff', 'category'])
        .agg(pl.col('count').sum())
        .join(per_category_counts, on='category')
        .with_columns((pl.col('count') / pl.col('cat_count')).alias('relative_count'))
    )
    train_types = ['ICE', 'S']
    plot_per_delay(
        [
            per_category.filter(pl.col('category') == train_type).sort('delay_diff')
            for train_type in train_types
        ],
        train_types,
        'Abweichung von DB-Verspätungsprognose nach Zugtyp',
        x_limits=(-3, 15),
        save_as='cache/plots/per_delay_diff_category.svg',
    )

    ### Per minutes_to_prognosed_time
    per_minutes_to_prognosed_time_counts = data.group_by(
        'minutes_to_prognosed_time_bins'
    ).agg(pl.col('count').sum().alias('minutes_to_prognosed_time_bins_count'))
    per_minutes_to_prognosed_time = (
        data.group_by(['delay_diff', 'minutes_to_prognosed_time_bins'])
        .agg(pl.col('count').sum())
        .join(per_minutes_to_prognosed_time_counts, on='minutes_to_prognosed_time_bins')
        .with_columns(
            (pl.col('count') / pl.col('minutes_to_prognosed_time_bins_count')).alias(
                'relative_count'
            )
        )
    )
    bins = ['(-inf, 5]', '(30, 60]']
    labels = ['Maximal 5 Minuten im Voraus', '30 bis 60 Minuten im Voraus']
    plot_per_delay(
        [
            per_minutes_to_prognosed_time.filter(
                pl.col('minutes_to_prognosed_time_bins') == bin
            ).sort('delay_diff')
            for bin in bins
        ],
        labels,
        'Abweichung von DB-Verspätungsprognose nach Zeit bis zur prognostizierten Zeit',
        x_limits=(-3, 15),
        save_as='cache/plots/per_delay_diff_minutes_to_prognosed_time.svg',
    )
    plot_per_delay(
        [
            per_minutes_to_prognosed_time.filter(
                pl.col('minutes_to_prognosed_time_bins') == bin
            ).sort('delay_diff')
            for bin in bins
        ],
        labels,
        'Abweichung von DB-Verspätungsprognose nach Zeit bis zur prognostizierten Zeit',
        x_limits=(-3, 15),
        data_hidden=[False, True],
        save_as='cache/plots/per_delay_diff_minutes_to_prognosed_time_short_time.svg',
    )

    ### Per delay_prognosed
    per_delay_prognosed_counts = data.group_by('delay_prognosed_bins').agg(
        pl.col('count').sum().alias('delay_prognosed_bins_count')
    )
    per_delay_prognosed = (
        data.group_by(['delay_diff', 'delay_prognosed_bins'])
        .agg(pl.col('count').sum())
        .join(per_delay_prognosed_counts, on='delay_prognosed_bins')
        .with_columns(
            (pl.col('count') / pl.col('delay_prognosed_bins_count')).alias(
                'relative_count'
            )
        )
    )
    bins = ['(0, 1]', '(15, 30]']
    labels = ['Pünktlich', '15-30 Minuten Verspätung']
    plot_per_delay(
        [
            per_delay_prognosed.filter(pl.col('delay_prognosed_bins') == bin).sort(
                'delay_diff'
            )
            for bin in bins
        ],
        labels,
        'Abweichung von DB-Verspätungsprognose nach aktueller Verspätung',
        x_limits=(-3, 15),
        save_as='cache/plots/per_delay_diff_delay_prognosed.svg',
    )
    plot_per_delay(
        [
            per_delay_prognosed.filter(pl.col('delay_prognosed_bins') == bin).sort(
                'delay_diff'
            )
            for bin in bins
        ],
        labels,
        'Abweichung von DB-Verspätungsprognose nach aktueller Verspätung',
        x_limits=(-3, 15),
        data_hidden=[False, True],
        save_as='cache/plots/per_delay_diff_delay_prognosed_on_time.svg',
    )

    ### Extremes
    counts = data.group_by(
        ['category', 'minutes_to_prognosed_time_bins', 'delay_prognosed_bins']
    ).agg(pl.col('count').sum().alias('count_group'))
    per_all = (
        data.group_by(GROUP_NAMES)
        .agg(pl.col('count').sum())
        .join(
            counts,
            on=['category', 'minutes_to_prognosed_time_bins', 'delay_prognosed_bins'],
        )
        .with_columns((pl.col('count') / pl.col('count_group')).alias('relative_count'))
    )
    one_extreme = per_all.filter(
        (pl.col('category') == 'ICE')
        & (pl.col('delay_prognosed_bins') == '(15, 30]')
        & (pl.col('minutes_to_prognosed_time_bins') == '(30, 60]')
    ).sort('delay_diff')
    other_extreme = per_all.filter(
        (pl.col('category') == 'S')
        & (pl.col('delay_prognosed_bins') == '(0, 1]')
        & (pl.col('minutes_to_prognosed_time_bins') == '(-inf, 5]')
    ).sort('delay_diff')
    plot_per_delay(
        [other_extreme, one_extreme],
        [
            'S-Bahn, 0 - 5 Min. im Voraus, pünktlich',
            'ICE, 30 - 60 Min. im Voraus, 15 - 30 Min. Verspätung',
        ],
        'Abweichung von DB-Verspätungsprognose Extrembeispiele',
        x_limits=(-3, 15),
        save_as='cache/plots/per_delay_diff_extreme.svg',
    )
    plot_per_delay(
        [other_extreme, one_extreme],
        [
            'S-Bahn, 0 - 5 Min. im Voraus, pünktlich',
            'ICE, 30 - 60 Min. im Voraus, 15 - 30 Min. Verspätung',
        ],
        'Abweichung von DB-Verspätungsprognose Extrembeispiele',
        x_limits=(-3, 15),
        data_hidden=[False, True],
        save_as='cache/plots/per_delay_diff_extreme_s.svg',
    )


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
