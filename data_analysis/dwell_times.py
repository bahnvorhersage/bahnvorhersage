import polars as pl

from helpers import TrainStopStolli


def get_stats():
    train_stop = TrainStopStolli.load_data().filter(
        pl.col('is_final')
        & ~pl.col('is_cancelled')
        & ~pl.col('dwell_time_real').is_null()
        & pl.col('dwell_time_real')
        >= 0
    )

    return (
        train_stop.group_by('stop_id')
        .agg(
            pl.col('dwell_time_schedule').mean(),
            pl.col('dwell_time_real').mean(),
            pl.col('dwell_time_real').count().alias('count'),
        )
        .collect(streaming=True)
    )


def main():
    stats = (
        get_stats()
        .with_columns(
            (pl.col('dwell_time_real') - pl.col('dwell_time_schedule')).alias('delta')
        )
        .sort('dwell_time_schedule')
    )
    print(stats)


if __name__ == '__main__':
    main()
