import argparse
import datetime
import io
import os

import cartopy
import cartopy.feature
import cartopy.mpl.geoaxes
import matplotlib
import matplotlib.pyplot as plt
import polars as pl
import pytz
from matplotlib.figure import Figure
from PIL import Image

from data_analysis import map
from database.cached_table_fetch import cached_sql_fetch_pl
from database.engine import DB_URI
from helpers import TrainStopStolli

matplotlib.use('Agg')
plt.style.use('dark_background')
# Configure cache path in kubernetes
if os.path.isdir('/usr/src/app/cache'):
    cartopy.config['data_dir'] = '/usr/src/app/cache'


def fig_to_webp_in_memory(fig: Figure) -> io.BytesIO:
    """Convert fig to webp in memory"""
    png = io.BytesIO()
    fig.savefig(
        png,
        bbox_inches='tight',
        dpi=700,
        format='png',
    )
    image = Image.open(png)
    image = image.convert('RGBA')
    webp = io.BytesIO()
    image.save(webp, 'webp')
    return webp


def pre_aggregate_data():
    train_stops = (
        TrainStopStolli.load_data()
        .filter(TrainStopStolli.final_filter)
        .group_by([pl.col('stop_id'), pl.col('time_schedule').dt.truncate('1w')])
        .agg(
            pl.col('delay').mean().alias('delay'),
            pl.col('is_cancelled').mean().alias('is_cancelled'),
            pl.col('delay').count().alias('count'),
            pl.col('lat').first().alias('lat'),
            pl.col('lon').first().alias('lon'),
        )
    ).filter(pl.col('count') > 100)

    train_stops = train_stops.collect(streaming=True)
    train_stops.write_database(
        'per_station_over_time_polars', connection=DB_URI, if_table_exists='replace'
    )
    return train_stops


def aggregate_data(
    pre_agg: pl.DataFrame, start_time: datetime.datetime, end_time: datetime.datetime
):
    data = pre_agg.filter(
        (pl.col('time_schedule') >= start_time) & (pl.col('time_schedule') < end_time)
    )

    return data.group_by('stop_id').agg(
        (pl.col('delay') * pl.col('count')).sum() / pl.col('count').sum(),
        (pl.col('is_cancelled') * pl.col('count')).sum() / pl.col('count').sum(),
        pl.col('count').sum(),
        pl.col('lat').first().alias('lat'),
        pl.col('lon').first().alias('lon'),
    )


class PerStationOverTimePolars:
    def __init__(self):
        self.fig, self.ax, self.cmap, self.sc, self.colorbar = map.create_base_plot(
            bbox=map.BBOX_GERMANY, ylabel='Ø Verspätung in Minuten'
        )

        self.data = cached_sql_fetch_pl(
            'SELECT * FROM per_station_over_time_polars', prefer_cache=True
        )
        self.max_time = self.data['time_schedule'].max()
        self.min_time = self.data['time_schedule'].min()

    def default(self):
        return self.plot(self.min_time, self.max_time)

    def plot(self, start_time: datetime.datetime, end_time: datetime.datetime):
        # Make times utc if they have no timezone
        local_tz = pytz.timezone('Europe/Berlin')
        if start_time.tzinfo is None:
            start_time = local_tz.localize(start_time).astimezone(pytz.utc)
        if end_time.tzinfo is None:
            end_time = local_tz.localize(end_time).astimezone(pytz.utc)

        data = aggregate_data(self.data, start_time, end_time)

        n_days = (end_time - start_time).days

        # Normalize the circle size to be the same no matter how many days of data were
        # requested. 2000 is the average max number of trains on the busiest station.
        # This is used to scale the size of the markers
        data = data.with_columns(
            [
                ((pl.col('count') * 70) / (n_days * 2000)).alias('size'),
                pl.col('delay') / 60,  # convert to minutes
            ]
        )

        # change the positions
        self.sc.set_offsets(
            list(zip(*map.CRS_TRANSFORMER.transform(data['lon'], data['lat'])))
        )
        # change the sizes
        self.sc.set_sizes(data['size'].to_numpy())
        # change the color
        self.sc.set_array(data['delay'].to_numpy())

        self.ax.set_title(
            start_time.astimezone(local_tz).strftime('%d.%m.%Y')
            + '-'
            + end_time.astimezone(local_tz).strftime('%d.%m.%Y'),
            fontsize=12,
        )

        plot = fig_to_webp_in_memory(self.fig)
        return plot


def main(args):
    if args.pre_aggregate:
        pre_aggregate_data()
    else:
        per_station = PerStationOverTimePolars()
        plot_image = per_station.plot(
            datetime.datetime(2022, 1, 1, tzinfo=pytz.utc),
            datetime.datetime(2022, 12, 31, tzinfo=pytz.utc),
        )
        with open('test.webp', 'wb') as f:
            f.write(plot_image.getvalue())


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        '--pre-aggregate',
        action='store_true',
        help='Pre aggregate data and store it in the database',
    )
    args = argument_parser.parse_args()

    main(args)
