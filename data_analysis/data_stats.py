import argparse
from datetime import UTC, date, datetime, timedelta

import polars as pl

from database.cached_table_fetch import cached_sql_fetch_pl
from database.engine import DB_URI
from helpers import TrainStopStolli
from helpers.cache import ttl_lru_cache


def generate_stats(data: pl.LazyFrame):
    data = data.select(
        pl.col('delay').max().alias('max_delay') // 60,
        pl.col('delay').mean().alias('avg_delay') / 60,
        pl.col('delay').count().alias('count'),
        pl.col('is_cancelled').mean().alias('cancelled'),
        (pl.col('delay').filter(~pl.col('is_cancelled')) <= 5 * 60)
        .mean()
        .alias('on_time'),
    ).collect()

    return data


@ttl_lru_cache(maxsize=1, seconds_to_live=60 * 60)
def load_stats() -> dict:
    stats = cached_sql_fetch_pl('SELECT * FROM stats_overview_v2')

    return stats.row(0, named=True)


def calculate_stats():
    # Previous month stats
    prev_month_start = (date.today().replace(day=1) - timedelta(days=1)).replace(day=1)
    current_month_start = date.today().replace(day=1)

    data_prev_month = TrainStopStolli.load_data(
        min_date=prev_month_start, max_date=current_month_start
    ).filter(TrainStopStolli.final_filter)
    data_prev_month_arrival = data_prev_month.filter(pl.col('is_arrival'))
    data_prev_month_departure = data_prev_month.filter(~pl.col('is_arrival'))

    stats_prev_month_arrival = generate_stats(data_prev_month_arrival)
    stats_prev_month_departure = generate_stats(data_prev_month_departure)

    # All time stats
    data_all_time = TrainStopStolli.load_data().filter(TrainStopStolli.final_filter)
    data_all_time_arrival = data_all_time.filter(pl.col('is_arrival'))
    data_all_time_departure = data_all_time.filter(~pl.col('is_arrival'))

    stats_all_time_arrival = generate_stats(data_all_time_arrival)
    stats_all_time_departure = generate_stats(data_all_time_departure)

    print('Previous month arrival stats:')
    print(stats_prev_month_arrival, end='\n\n')

    print('Previous month departure stats:')
    print(stats_prev_month_departure, end='\n\n')

    print('All time arrival stats:')
    print(stats_all_time_arrival, end='\n\n')

    print('All time departure stats:')
    print(stats_all_time_departure, end='\n\n')

    combined_stats = pl.DataFrame(
        {
            'month_start': [prev_month_start],
            'month_end': [current_month_start - timedelta(days=1)],
            'stats_creation_ts': [datetime.now(UTC)],
        }
    )

    combined_stats: pl.DataFrame = pl.concat(
        [
            combined_stats,
            stats_prev_month_arrival.rename(lambda name: 'month_ar_' + name),
            stats_prev_month_departure.rename(lambda name: 'month_dp_' + name),
            stats_all_time_arrival.rename(lambda name: 'all_ar_' + name),
            stats_all_time_departure.rename(lambda name: 'all_dp_' + name),
        ],
        how='horizontal',
    )

    combined_stats.write_database(
        'stats_overview_v2', connection=DB_URI, if_table_exists='replace'
    )


def main(args):
    if args.calculate_stats:
        calculate_stats()


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        '--calculate-stats',
        action='store_true',
        help='Calculate the stats and store them in the database',
    )
    args = argument_parser.parse_args()

    main(args)
