import matplotlib.pyplot as plt
import polars as pl
import tueplots.bundles

from data_analysis.utils import data_aggregator
from helpers.plt_legend_top import LEG_TOP_KWARGS


def plot_per_delay(
    data: pl.DataFrame,
    title: str,
    loggy: bool = False,
    save_as: str | None = None,
):
    fig, ax1 = plt.subplots()
    ax1: plt.Axes

    ax1.step(data['x'], data['relative_count'], where='mid', label='Density')
    ax1.step(
        data['x'], data['cancellation_rate'], where='mid', label='Cancellation rate'
    )

    ax1.set_xlim(data['x'].min(), data['x'].max())

    ax1.grid(True)

    ax1.set_xlabel('Delay in minutes')

    if loggy:
        ax1.set_yscale('log')
    else:
        ax1.set_ylim(bottom=0)

    ax1.set_title(title)

    fig.legend(ncols=2, **LEG_TOP_KWARGS)

    if save_as is not None:
        fig.savefig(save_as, dpi=700)
    else:
        plt.show()


def main():
    plt.rcParams.update(tueplots.bundles.beamer_moml())

    data = data_aggregator(
        filter_expr=pl.col('is_final'),
        group_by=(pl.col('delay') // 1).clip(lower_bound=-5, upper_bound=25),
    )
    plot_per_delay(
        data, 'Delay distribution', loggy=False, save_as='cache/plots/per_delay.png'
    )

    # Tübingen Hbf
    data = data_aggregator(
        filter_expr=pl.col('is_final') & (pl.col('stop_id') == 8000141),
        group_by=(pl.col('delay') // 1).clip(lower_bound=-5, upper_bound=25),
    )
    plot_per_delay(
        data,
        'Delay distribution at Tübingen Hbf',
        loggy=False,
        save_as='cache/plots/per_delay_tue.png',
    )


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
