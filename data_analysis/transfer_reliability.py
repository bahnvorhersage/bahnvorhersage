from datetime import date

import polars as pl
import xlsxwriter
from tqdm import tqdm

from gtfs.stops import StopSteffen
from helpers.date_range import date_range, date_range_len
from parser.gtfs_to_train_stops import PATH

STOP_ID_SINGEN_HOHENTWIEL = 8000073


def evaluate_transfer_at_date(transfers: pl.DataFrame, current_date: date):
    transfers = transfers.with_columns(
        pl.lit(current_date)
        .dt.combine(pl.col('ar_time'), time_unit='ns')
        .alias('ar_time'),
        pl.lit(current_date)
        .dt.combine(pl.col('dp_time'), time_unit='ns')
        .alias('dp_time'),
    )
    transfers = transfers.with_columns(
        pl.col('ar_time')
        .dt.replace_time_zone('Europe/Berlin')
        .dt.convert_time_zone('UTC'),
        pl.col('dp_time')
        .dt.replace_time_zone('Europe/Berlin')
        .dt.convert_time_zone('UTC'),
    )

    try:
        train_stops = pl.read_parquet(
            source=PATH.format(service_date=current_date.isoformat()),
            columns=[
                'stop_id',
                'number',
                'time_schedule',
                'time_real',
                'is_cancelled',
                'is_arrival',
                'category',
            ],
        )
    except FileNotFoundError:
        print(f'No data found for {current_date}')
        return None

    # # DEBUG
    # train_stops = train_stops.filter(
    #     (pl.col('stop_id') == STOP_ID_SINGEN_HOHENTWIEL)
    #     & (pl.col('category') == 'IC')
    #     & ~pl.col('is_arrival')
    # ).sort('time_schedule')
    # return None

    transfers = transfers.join(
        train_stops.filter(pl.col('is_arrival'))
        .rename(
            {
                'time_schedule': 'ar_time_schedule',
                'time_real': 'ar_time_real',
                'is_cancelled': 'ar_cancelled',
            }
        )
        .drop(['is_arrival']),
        left_on=['ar_number', 'stop_id', 'ar_time'],
        right_on=['number', 'stop_id', 'ar_time_schedule'],
        how='left',
    ).join(
        train_stops.filter(~pl.col('is_arrival'))
        .rename(
            {
                'time_schedule': 'dp_time_schedule',
                'time_real': 'dp_time_real',
                'is_cancelled': 'dp_cancelled',
            }
        )
        .drop(['is_arrival']),
        left_on=['dp_number', 'stop_id', 'dp_time'],
        right_on=['number', 'stop_id', 'dp_time_schedule'],
        how='left',
    )

    transfers = transfers.with_columns(
        (pl.col('dp_time_real') - pl.col('ar_time_real'))
        .dt.total_minutes()
        .alias('transfer_time_real'),
        (pl.col('dp_time') - pl.col('ar_time'))
        .dt.total_minutes()
        .alias('transfer_time_schedule'),
        pl.lit(current_date).alias('date'),
    )

    transfers = transfers.with_columns(
        (pl.col('transfer_time_real') >= pl.col('minimal_transfer_time')).alias(
            'enough_transfer_time'
        ),
        (pl.col('transfer_time_real') - pl.col('minimal_transfer_time')).alias(
            'transfer_time_buffer'
        ),
        (pl.col('ar_cancelled') | pl.col('dp_cancelled'))
        .alias('transfer_cancelled')
        .fill_null(pl.lit(False)),
        (
            pl.col('ar_time_real').is_not_null() & pl.col('dp_time_real').is_not_null()
        ).alias('ar_and_dp_known'),
    )

    transfers = transfers.with_columns(
        (pl.col('enough_transfer_time') & ~pl.col('transfer_cancelled')).alias(
            'transfer_worked'
        ),
    )

    return transfers


def main():
    start_date = date(2022, 12, 11)
    end_date = date(2023, 12, 9)

    stops = (
        StopSteffen()
        .to_polars()
        .filter(pl.col('location_type') == 'STATION')
        .drop(
            ['stop_lat', 'stop_lon', 'location_type', 'parent_station', 'platform_code']
        )
    )

    transfers = (
        pl.read_csv(
            source='Test_Anschlusserreichung_Datenbasis_ML2korrigiert_varianten.csv',
            has_header=True,
            dtypes={
                'ar_number': pl.String,
                'dp_number': pl.String,
                'minimal_transfer_time': pl.Int64,
            },
        )
        .drop_nulls()
        .with_columns(
            pl.col('ar_time').str.to_time('%H:%M:%S'),
            pl.col('dp_time').str.to_time('%H:%M:%S'),
        )
        .join(stops, left_on='station', right_on='stop_name', how='left')
    )

    all_transfers = list()

    for test_date in tqdm(
        date_range(start_date, end_date), total=date_range_len(start_date, end_date)
    ):
        transfers_today = transfers.clone()
        transfers_today = evaluate_transfer_at_date(transfers_today, test_date)
        if transfers_today is not None:
            all_transfers.append(transfers_today)

    all_transfers = pl.concat(all_transfers)

    # Write results to clipboard
    # TODO: Write automatically to csv or ods
    with xlsxwriter.Workbook('Auswertung NVBW-Bahn-Vorhersage.xlsx') as workbook:
        workbook = (
            all_transfers.group_by(['FahrtNr', 'Status'])
            .agg(
                pl.col('transfer_worked').mean(),
                pl.col('transfer_worked').sum().alias('transfer_worked_count'),
                (~pl.col('transfer_worked')).sum().alias('transfer_not_worked_count'),
                pl.col('transfer_cancelled').mean(),
                pl.col('transfer_time_real').mean(),
                pl.col('transfer_time_schedule').mean(),
                pl.col('ar_time_real').count().alias('ar_count'),
                pl.col('dp_time_real').count().alias('dp_count'),
                pl.col('ar_and_dp_known').sum().alias('ar_and_dp_count'),
            )
            .sort(['FahrtNr', 'Status'])
            .write_excel(workbook=workbook, worksheet='Umstiege', autofit=True)
        )

        all_transfers.group_by(['FahrtNr', 'date']).agg(
            pl.col('transfer_worked').all().alias('all_transfer_worked'),
            pl.col('transfer_cancelled').any().alias('any_transfer_cancelled'),
            pl.col('ar_and_dp_known').all().alias('all_ar_and_dp_known'),
        ).filter(pl.col('all_ar_and_dp_known')).group_by(['FahrtNr']).agg(
            pl.col('all_transfer_worked').mean(),
            pl.col('any_transfer_cancelled').mean(),
            pl.col('all_ar_and_dp_known').sum(),
        ).sort('FahrtNr').write_excel(
            workbook=workbook, worksheet='Reiseketten', autofit=True
        )


if __name__ == '__main__':
    with pl.Config(
        tbl_formatting='UTF8_FULL_CONDENSED',
        tbl_rows=50,
        tbl_cols=10,
    ):
        main()
