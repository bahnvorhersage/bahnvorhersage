from datetime import date

import matplotlib.pyplot as plt
import polars as pl
import tueplots.bundles

from helpers import TrainStopStolli
from helpers.date_range import date_range
from helpers.plt_legend_top import LEG_TOP_KWARGS

FERN_DB_FILTER = (
    pl.col('is_final')
    & ~pl.col('is_betriebshalt')
    & ~pl.col('is_regional')
    & (pl.col('operator') == '80')
)

DB_REGIO_OPERATORS = {
    '80MUS',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '800001',
    'rbpSWM',
    'rbpDRM',
    '800647',
    '800631',
    '8006D6',
    '8006C5',
    '8006C4',
    '8006C6',
    '8006D8',
    '80S631',
    '800654',
    '8006SH',
    '8006RA',
    '8006D3',
    '8006000',
    '800632',
    '8006D2',
    '800694',
    '800693',
    '800659',
    '800622',
    '8006D1',
    '8006C8',
    '8006C9',
    '8006C7',
    '800619',
    '8006B7',
    '8006B8',
    '8006B9',
    '8006C1',
    '8006C2',
    '8006C3',
    '8007D4',
    '8007D5',
    '800742',
    '800768',
    '800734',
    '800755',
    '800733',
    '800772',
    '800765',
    '800767',
    '800759',
    '800720',
    '800746',
    '800785',
    '800721',
    '8007SV',
    '8007H3',
    '8007H1',
    '8007DU',
    '8007H2',
    '800790',
    '800714',
    '8015FR',
    '8005A4',
    '800571',
    '800572',
    '8005KG',
    '801526',
    '800574',
    '8005MW',
    '800535',
    '801512',
    '8015A1',
    '8005ND',
    '8005L2',
    '801566',
    '8015RR',
    '801599',
    '801591',
    '800515',
    '801518',
    '8015RP',
    '801513',
    '801539',
    '8005SV',
    '800536',
    '8015A6',
    '801540',
    '800640',
    '800553',
    '800520',
    '8015H8',
    '8015H9',
    '800277',
    '800204',
    '8002A3',
    '8002B5',
    '800295',
    '800244',
    '800201',
    '800271',
    '800279',
    '800292',
    '800293',
    '800262',
    '800163',
    '800151',
    '800157',
    '800159',
    '800165',
    '800162',
    '800166',
    '800158',
    '800161',
    '800155',
    '800160',
    '800164',
    '800154',
    '800152',
    '800153',
    '800156',
    '800310',
    '8003A5',
    '800349',
    '8003G1',
    '8003A',
    '8003S',
    '800352',
    '800338',
    '800333',
    '800311',
    '800304',
    '800305',
    '800306',
    '800301',
    '800354',
    '800337',
    '8003RL',
    '8003L2',
    '8003L1',
    '8003H5',
    '8003G2',
    '800348',
    '800351',
    '800363',
    '800430',
    '801382',
    '800523',
    '8013D',
    '8013E',
    '8013S',
    '8006A7',
    '800603',
    '801327',
    '80S618',
    '80S603',
    '800618',
    '8',
    '80002',
    '80001',
    '0S',
    '0S0001',
    '0S0002',
    '8007M',
    '800725',
    '800528',
    '800643',
    '800462',
    '04DEKT',
    '800445',
    '800487',
    '800489',
    '800456',
    '800469',
    '800486',
    '8004A9',
    '800417',
    '800416',
    '8004NT',
    '800413',
    '8004L1',
    '800478',
}

DB_REGIO_FILTER = (
    pl.col('is_final')
    & ~pl.col('is_betriebshalt')
    & pl.col('is_regional')
    & (pl.col('operator').is_in(DB_REGIO_OPERATORS))
)


def get_number_of_stops(min_date: date, max_date: date) -> int:
    stop_count = 0
    for service_date in date_range(min_date, max_date):
        try:
            print(service_date)
            stop_count += (
                TrainStopStolli.load_data(service_date=service_date)
                .filter(DB_REGIO_FILTER)
                .select(
                    pl.struct('trip_id', 'stop_sequence')
                    .n_unique()
                    .alias('stop_count'),
                )
                .collect()
                .item()
            )
        except ValueError:
            pass

    return stop_count


def plot_cancellations(cancellations: pl.DataFrame, save_as: str | None = None):
    fig, ax = plt.subplots()
    ax: plt.Axes

    ax.step(cancellations['breakpoint'], cancellations['count'])

    limits = cancellations.filter(pl.col('breakpoint').is_finite()).select(
        pl.col('breakpoint').min().alias('min'), pl.col('breakpoint').max().alias('max')
    )

    ax.set_xlim(
        limits['min'].item(),
        limits['max'].item(),
    )
    ax.grid(True)

    fig.legend(ncols=2, **LEG_TOP_KWARGS)

    if save_as is not None:
        fig.savefig(save_as, dpi=700)
    else:
        plt.show()


def analyze_15_11_to_15_12_24():
    long_dinstance_data = TrainStopStolli.load_data(
        min_date=date(2023, 11, 15), max_date=date(2023, 12, 16)
    ).filter(FERN_DB_FILTER)

    trips = long_dinstance_data.group_by('trip_id').agg(
        pl.len().alias('stop_count'),
        pl.col('is_cancelled').sum().alias('cancelled_count'),
        pl.col('is_cancelled').any().alias('is_cancelled'),
        pl.col('is_cancelled').all().alias('completely_cancelled'),
        pl.col('time_schedule').min().alias('start_time'),
    )

    days = (
        trips.group_by(pl.col('start_time').dt.truncate('1d'))
        .agg(
            pl.col('stop_count').sum().alias('stop_count'),
            pl.col('cancelled_count').sum().alias('cancelled_count'),
            pl.col('is_cancelled').mean().alias('cancellation_rate'),
            pl.col('is_cancelled').sum().alias('cancelled_trips'),
            pl.col('completely_cancelled').sum().alias('completely_cancelled_trips'),
            pl.len().alias('trip_count'),
        )
        .sort('start_time')
        .collect()
    )

    print(days)


def main():
    plt.rcParams.update(tueplots.bundles.beamer_moml())

    stop_count = get_number_of_stops(date(2024, 1, 1), date.today())
    print(stop_count)

    cancellations = TrainStopStolli.load_data(
        min_date=date(2024, 1, 1), max_date=date.today()
    ).filter(DB_REGIO_FILTER)

    cancellations = cancellations.with_columns(
        (
            (pl.col('time_schedule') - pl.col('update_timestamp')).dt.total_seconds()
            // 60
        ).alias('minutes_to_planned_time'),
    )

    cancellations = cancellations.select(
        pl.len().alias('count'),
        ((pl.col('minutes_to_planned_time') <= 60) & pl.col('is_cancelled'))
        .sum()
        .alias('sponti_60_count'),
        ((pl.col('minutes_to_planned_time') <= 120) & pl.col('is_cancelled'))
        .sum()
        .alias('sponti_120_count'),
        ((pl.col('minutes_to_planned_time') <= 180) & pl.col('is_cancelled'))
        .sum()
        .alias('sponti_180_count'),
        pl.col('is_cancelled').sum().alias('canceled_count'),
        pl.lit(stop_count).alias('stop_count'),
    ).collect(streaming=True)

    print(cancellations)


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    analyze_15_11_to_15_12_24()

    main()
