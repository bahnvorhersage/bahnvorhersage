import math
from datetime import UTC, datetime

import polars as pl
import requests

from api import db_rest, fptf
from database.ris_transfer_time import (
    add_minimal_transfer_times_to_journeys,
)
from helpers.StreckennetzSteffi import StreckennetzSteffi
from helpers.tripel_iterator import iter_triples
from ml_models.data_preperation import bearing
from public_config import LONG_DISTANCE_CATEGORIES, PREDICTOR_URL


def minute_of_day(time: datetime) -> int:
    return time.hour * 60 + time.minute


def journeys_to_df(
    journeys: list[fptf.Journey],
    trips: dict[str, list[fptf.Stopover]],
    streckennetz: StreckennetzSteffi,
) -> pl.DataFrame:
    data = {
        'number': [],
        'lat': [],
        'lon': [],
        'stop_sequence': [],
        'distance_traveled': [],
        'dwell_time_schedule': [],
        'dwell_time_prognosed': [],
        # 'bearing': [],
        'lat_start': [],
        'lon_start': [],
        'lat_end': [],
        'lon_end': [],
        'delay_prognosed': [],
        'minute_of_day': [],
        'minutes_to_prognosed_time': [],
        'weekday': [],
        'is_regional': [],
        'is_arrival': [],
        'operator': [],
        'category': [],
        'line': [],
        'prognosed_transfer_time': [],
        'minimal_transfer_time': [],
    }
    for journey in journeys:
        for previous_leg, current_leg, next_leg in iter_triples(journey.legs):
            if previous_leg is None and next_leg is None:
                # If there is only one leg in the journey
                data['prognosed_transfer_time'].extend([None, None])
                data['minimal_transfer_time'].extend([None, None])
            elif previous_leg is None or next_leg is None:
                # If this is the first or last leg of a multi-leg journey
                data['prognosed_transfer_time'].append(None)
                data['minimal_transfer_time'].append(None)

            if current_leg.type == 'transfer':
                if previous_leg is not None and next_leg is not None:
                    transfer_time = int(
                        (next_leg.departure - previous_leg.arrival).total_seconds()
                        // 60
                    )
                    minimum_transfer_time = int(
                        math.ceil(current_leg.duration.seconds / 60)
                    )
                    data['prognosed_transfer_time'].extend(2 * [transfer_time])
                    data['minimal_transfer_time'].extend(2 * [minimum_transfer_time])

                continue

            current_trip = trips[current_leg.tripId]
            current_trip_path = [stopover.stop.name for stopover in current_trip]

            # departure of leg
            stop_sequence = current_trip_path.index(current_leg.origin.name)
            data['number'].append(int(current_leg.line.fahrtNr))
            data['lat'].append(current_leg.origin.location.latitude)
            data['lon'].append(current_leg.origin.location.longitude)
            data['stop_sequence'].append(stop_sequence)
            data['distance_traveled'].append(
                int(
                    streckennetz.route_length(
                        current_trip_path[:stop_sequence],
                        is_bus=current_leg.line.productName.lower() == 'bus',
                    )
                )
            )
            data['dwell_time_schedule'].append(
                current_trip[stop_sequence].planned_dwell_time_minutes()
            )
            data['dwell_time_prognosed'].append(
                current_trip[stop_sequence].dwell_time_minutes()
            )
            data['lat_start'].append(current_trip[0].stop.location.latitude)
            data['lon_start'].append(current_trip[0].stop.location.longitude)
            data['lat_end'].append(current_trip[-1].stop.location.latitude)
            data['lon_end'].append(current_trip[-1].stop.location.longitude)
            data['delay_prognosed'].append(current_leg.departure_delay_minutes)
            data['minute_of_day'].append(minute_of_day(current_leg.plannedDeparture))
            data['minutes_to_prognosed_time'].append(
                (current_leg.departure - datetime.now(UTC)).seconds // 60
            )
            data['weekday'].append(current_leg.plannedDeparture.weekday())
            data['is_regional'].append(
                current_leg.line.productName not in LONG_DISTANCE_CATEGORIES
            )
            data['is_arrival'].append(False)
            data['operator'].append(current_leg.line.adminCode.replace('_', ''))
            data['category'].append(current_leg.line.productName)
            data['line'].append(
                current_leg.line.name
            )  # TODO: Check if these match with the IRIS line names

            # arrival of leg
            stop_sequence = current_trip_path.index(current_leg.destination.name)
            data['number'].append(int(current_leg.line.fahrtNr))
            data['lat'].append(current_leg.destination.location.latitude)
            data['lon'].append(current_leg.destination.location.longitude)
            data['stop_sequence'].append(stop_sequence)
            data['distance_traveled'].append(
                int(
                    streckennetz.route_length(
                        current_trip_path[:stop_sequence],
                        is_bus=current_leg.line.productName.lower() == 'bus',
                    )
                )
            )
            data['dwell_time_schedule'].append(
                current_trip[stop_sequence].planned_dwell_time_minutes()
            )
            data['dwell_time_prognosed'].append(
                current_trip[stop_sequence].dwell_time_minutes()
            )
            data['lat_start'].append(current_trip[0].stop.location.latitude)
            data['lon_start'].append(current_trip[0].stop.location.longitude)
            data['lat_end'].append(current_trip[-1].stop.location.latitude)
            data['lon_end'].append(current_trip[-1].stop.location.longitude)
            data['delay_prognosed'].append(current_leg.arrival_delay_minutes)
            data['minute_of_day'].append(minute_of_day(current_leg.plannedArrival))
            data['minutes_to_prognosed_time'].append(
                (current_leg.arrival - datetime.now(UTC)).seconds // 60
            )
            data['weekday'].append(current_leg.plannedArrival.weekday())
            data['is_regional'].append(
                current_leg.line.productName not in LONG_DISTANCE_CATEGORIES
            )
            data['is_arrival'].append(True)
            data['operator'].append(current_leg.line.adminCode.replace('_', ''))
            data['category'].append(current_leg.line.productName)
            data['line'].append(
                current_leg.line.name
            )  # TODO: Check if these match with the IRIS line names - they don't

    df = pl.DataFrame(data)
    return bearing(df)


def add_predictions_to_journeys(
    journeys: list[fptf.Journey],
    probas: list[list[float]],
    transfer_scores: list[float | None],
    offset: int,
) -> list[fptf.Journey]:
    index = 0

    for journey in journeys:
        for previous, current, next in iter_triples(journey.legs):
            if current.type == 'transfer':
                if previous is not None and next is not None:
                    current.transferScore = transfer_scores[index]
            else:
                # Departure
                prediction = fptf.DelayPrediction(
                    predictions=probas[index], offset=offset, decimals=None
                )
                current.departureDelayPrediction = prediction
                index += 1

                # Arrival
                prediction = fptf.DelayPrediction(
                    predictions=probas[index], offset=offset, decimals=None
                )
                current.arrivalDelayPrediction = prediction
                index += 1
    return journeys


def rate_journeys(
    journeys: list[fptf.Journey],
    train_trips: dict[str, list[fptf.Stopover]],
    streckennetz: StreckennetzSteffi,
):
    journeys = add_minimal_transfer_times_to_journeys(journeys)

    prediction_data = journeys_to_df(journeys, train_trips, streckennetz)

    predictions = requests.post(
        PREDICTOR_URL + 'rate-journeys/',
        json=prediction_data.to_dict(as_series=False),
    )

    if predictions.ok:
        predictions = predictions.json()
        probas = predictions['predictions']
        transfer_scores = predictions['transfer_scores']
        offset = predictions['offset']
    else:
        raise ValueError('Could not get prediction results')

    journeys = add_predictions_to_journeys(journeys, probas, transfer_scores, offset)
    return journeys


def get_and_rate_journeys(
    streckennetz: StreckennetzSteffi,
    start: int,
    destination: int,
    date: datetime,
    search_for_arrival: bool = False,
    only_regional: bool = False,
    bike: bool = False,
) -> list[dict]:
    params = {
        'from_': start,
        'to': destination,
        'results': 6,
        'only_regional': only_regional,
        'bike': bike,
        'tickets': True,
    }
    if search_for_arrival:
        params['arrival'] = date
    else:
        params['departure'] = date
    journeys = db_rest.get_journey(
        **params,
    )

    # filter out journeys with cancelled legs
    journeys = [
        journey
        for journey in journeys
        if not any(leg.type == 'leg' and leg.cancelled for leg in journey.legs)
    ]

    return rate_journeys(
        journeys=journeys,
        train_trips=get_trips_from_journeys(journeys),
        streckennetz=streckennetz,
    )


def get_trips_from_journeys(
    journeys: list[fptf.Journey],
) -> dict[str, list[fptf.Stopover]]:
    trip_ids = set()
    for journey in journeys:
        trip_ids.update(journey.get_trip_ids())

    return {
        trip_id: trip
        for trip_id, trip in zip(trip_ids, map(db_rest.get_trip, trip_ids))
    }
