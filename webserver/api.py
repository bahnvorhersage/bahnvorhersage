import json
from datetime import datetime

from flask import (
    Blueprint,
    Response,
    current_app,
    jsonify,
    make_response,
    request,
)

from api import db_rest
from api.exceptions import RateLimitExceeded
from data_analysis import data_stats
from helpers.json_serializer import json_serial
from router.exceptions import NoRouteFound, NoTimetableFound
from webserver.db_logger import db, log_activity
from webserver.journeys import (
    get_and_rate_journeys,
    get_trips_from_journeys,
    rate_journeys,
)

bp = Blueprint('api', __name__, url_prefix='/api')
bp_limited = Blueprint('api_rate_limited', __name__, url_prefix='/api')


@bp.route('/station_list.json', methods=['GET'])
@log_activity
def get_station_list():
    """
    Gets called when the website is loaded
    And gets some data from and about the user
    It returns the trainstations for the autofill forms

    Returns
    -------
    flask generated json
        list: a list of strings with all the known train stations
    """
    resp = jsonify({'stations': current_app.streckennetz.get_ris_active_names()})
    # Cache for 1 week
    resp.cache_control.max_age = 60 * 60 * 24 * 7
    return resp


@bp_limited.route('/stations.json')
@log_activity
def station_dump():
    """Return all station data as json

    Returns
    -------
    json
        All station data
    """
    current_app.logger.info('Returning stations.json')
    # We don't use jsonify here, because we have to do the
    # conversion to str within pandas. Converting the stations
    # to a dict first results in a Datetime out of bounds error.
    r = make_response(
        json.dumps(
            {'stations': current_app.streckennetz.station_df.to_dicts()},
            default=json_serial,
            ensure_ascii=False,
            separators=(',', ':'),
            sort_keys=False,
        )
    )
    r.mimetype = 'application/json'
    # Cache for 1 week
    r.cache_control.max_age = 60 * 60 * 24 * 7
    return r


@bp.route('/journeys', methods=['POST'])
@log_activity
def journeys():
    start = request.json['start']
    destination = request.json['destination']
    date = datetime.strptime(request.json['date'], '%d.%m.%Y %H:%M')

    # optional:
    search_for_arrival = (
        request.json['search_for_arrival']
        if 'search_for_arrival' in request.json
        else False
    )
    only_regional = (
        request.json['only_regional'] if 'only_regional' in request.json else False
    )
    bike = request.json['bike'] if 'bike' in request.json else False

    current_app.logger.info(
        'Getting connections from ' + start + ' to ' + destination + ', ' + str(date)
    )

    start = current_app.streckennetz.get_eva(name=start)
    destination = current_app.streckennetz.get_eva(name=destination)

    try:
        journeys = get_and_rate_journeys(
            streckennetz=current_app.streckennetz,
            start=start,
            destination=destination,
            date=date,
            search_for_arrival=search_for_arrival,
            only_regional=only_regional,
            bike=bike,
        )
    except RateLimitExceeded:
        current_app.logger.error('DB-Rate limit exceeded')
        return jsonify(
            {
                'error': 'DB-Rate-Limit: Die Server der Deutschen Bahn haben eine benötigte Anfrage blockiert, weil Bahn-Vorhersage aktuell zu viele Anfrage stellt.'
            }
        ), 500

    resp = make_response(
        json.dumps(
            journeys, default=json_serial, ensure_ascii=False, separators=(',', ':')
        )
    )
    resp.mimetype = 'application/json'
    resp.headers.add('Access-Control-Allow-Origin', '*')

    return resp


@bp.route('/refresh-journey', methods=['POST'])
@log_activity
def refresh_journey():
    refresh_token = request.json['refresh_token']

    current_app.logger.info(f'Refreshing journey with token {refresh_token}')

    try:
        journeys = db_rest.refresh_journey(refresh_token)
        journeys = rate_journeys(
            journeys=journeys,
            train_trips=get_trips_from_journeys(journeys),
            streckennetz=current_app.streckennetz,
        )

    except RateLimitExceeded:
        current_app.logger.error('DB-Rate limit exceeded')
        return jsonify(
            {
                'error': 'DB-Rate-Limit: Die Server der Deutschen Bahn haben eine benötigte Anfrage blockiert, weil Bahn-Vorhersage aktuell zu viele Anfrage stellt.'
            }
        ), 500

    assert len(journeys) == 1, f'Expected exactly one journey, got {len(journeys)}'

    resp = make_response(
        json.dumps(
            journeys[0], default=json_serial, ensure_ascii=False, separators=(',', ':')
        )
    )
    resp.mimetype = 'application/json'
    resp.headers.add('Access-Control-Allow-Origin', '*')

    return resp


@bp_limited.route('/journeys-and-alternatives', methods=['POST'])
@log_activity
def journeys_and_alternatives():
    origin = request.json['origin']
    destination = request.json['destination']
    departure = datetime.fromisoformat(request.json['departure'])

    current_app.logger.info(f'Routing from {origin} to {destination} at {departure}')

    try:
        journeys = current_app.router.do_routing(
            origin, destination, departure, db.session
        )
    except NoTimetableFound as e:
        current_app.logger.error(f'No timetable found: {e}')
        return jsonify(
            {
                'error': 'Für den gewählten Zeitpunkt steht uns kein Fahrplan zur Verfügung'
            }
        ), 500
    except NoRouteFound as e:
        current_app.logger.error(f'No route found: {e}')
        return jsonify({'error': 'Es konnte keine Verbindung gefunden werden'}), 500

    resp = jsonify(journeys)
    # resp.headers.add("Access-Control-Allow-Origin", "*")
    return resp


@bp.route('/stats')
@log_activity
def stats():
    resp = make_response(
        json.dumps(
            data_stats.load_stats(),
            default=json_serial,
            ensure_ascii=False,
            separators=(',', ':'),
        )
    )
    resp.mimetype = 'application/json'
    resp.headers.add('Access-Control-Allow-Origin', '*')

    return resp


@bp.route('/stationplot/<string:date_range>.webp')
@log_activity
def station_plot(date_range):
    """
    Generates a plot that visualizes all the delays
    between the two dates specified in the url.

    Parameters
    ----------
    date_range : string
        The date range to generate the plot of in format
        `%d.%m.%Y, %H:%M-%d.%m.%Y, %H:%M`

    Returns
    -------
    image/webp
        The generated plot
    """

    current_app.logger.info(f'Generating plot: {date_range}')

    try:
        date_range = date_range.split('-')
        start_date = datetime.strptime(date_range[0], '%d.%m.%Y')
        end_date = datetime.strptime(date_range[1], '%d.%m.%Y')
        return Response(
            current_app.per_station_time.plot(start_date, end_date).getvalue(),
            mimetype='image/webp',
        )
    except ValueError:
        return Response(
            current_app.per_station_time.default().getvalue(), mimetype='image/webp'
        )


@bp.route('/stationplot/limits')
@log_activity
def limits():
    """
    Returns the current datetime limits between which we can generate plots.

    Returns
    -------
    {
        "min": <min_date>,
        "max": <max_date>,
        "freq": <frequency in hours>
    }
    """
    limits = {
        'max': current_app.per_station_time.max_time.date().isoformat(),
        'min': current_app.per_station_time.min_time.date().isoformat(),
        'freq': 24 * 7,
    }
    return jsonify(limits)
