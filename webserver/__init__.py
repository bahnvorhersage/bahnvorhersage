import os

# Do not use GUI for matplotlib
import matplotlib
from flask import Flask, request
from flask_compress import Compress
from flask_limiter import Limiter

from data_analysis.per_station import PerStationOverTimePolars
from helpers.logger import logging
from helpers.StreckennetzSteffi import StreckennetzSteffi
from router.router_csa import RouterCSA
from webserver.db_logger import db

matplotlib.use('Agg')


def own_get_remote_address():
    return (
        request.remote_addr
        if 'X-Original-Forwarded-For' not in request.headers
        else request.headers['X-Original-Forwarded-For']
    )


def create_app():
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    # Create app with changed paths https://stackoverflow.com/a/42791810
    app = Flask(
        __name__,
        instance_relative_config=True,
        template_folder='website/dist',
        static_folder='website/dist',
        static_url_path='',
    )

    compress = Compress()
    compress.init_app(app)

    from webserverconfig import DevelopmentConfig, ProductionConfig

    if app.config['DEBUG']:
        app.config.from_object(DevelopmentConfig)
    else:
        app.config.from_object(ProductionConfig)

    app.logger.info('DB init...')
    db.init_app(app)
    with app.app_context():
        db.create_all()
    app.logger.info('Done')

    logging.info('Initialising streckennetz')
    app.streckennetz = StreckennetzSteffi(prefer_cache=False)
    logging.info('Done!')

    logging.info('Initialising per_station_time')
    app.per_station_time = PerStationOverTimePolars()
    logging.info('Done!')

    logging.info('Initialising router')
    app.router = RouterCSA()
    logging.info('Done!')

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.logger.info('Initializing the api...')
    from webserver import api, mobile_v1

    app.register_blueprint(api.bp)
    app.register_blueprint(api.bp_limited)
    app.register_blueprint(mobile_v1.mobile_v1)
    app.logger.info('Done')

    limiter = Limiter(
        own_get_remote_address,
        app=app,
    )
    limiter.limit('2 per minute;60 per day')(api.bp_limited)

    app.logger.info(
        '\nSetup done, webserver is up and running!\
        \n^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n\n'
    )

    return app
