import json

from flask import (
    Blueprint,
    current_app,
    make_response,
    request,
)

from api import fptf
from helpers.json_serializer import json_serial
from webserver.db_logger import log_activity
from webserver.journeys import rate_journeys

mobile_v1 = Blueprint('mobile_v1', __name__, url_prefix='/api/mobile/v1')


@mobile_v1.route('/journeys', methods=['POST'])
@log_activity
def journeys():
    journeys = [fptf.Journey.from_dict(journey) for journey in request.json['journeys']]
    trips = {
        trip['id']: fptf.Stopover.trip_from_list(trip['stopovers'])
        for trip in request.json['trips'].values()
    }

    current_app.logger.info(f'Rating {len(journeys)} journeys with {len(trips)} trips')

    journeys = rate_journeys(
        journeys=journeys,
        train_trips=trips,
        streckennetz=current_app.streckennetz,
    )

    resp = make_response(
        json.dumps(
            journeys, default=json_serial, ensure_ascii=False, separators=(',', ':')
        )
    )
    resp.mimetype = 'application/json'
    resp.headers.add('Access-Control-Allow-Origin', '*')

    return resp
