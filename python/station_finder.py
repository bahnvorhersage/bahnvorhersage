from concurrent.futures import ProcessPoolExecutor
from datetime import date

from redis import Redis
from tqdm import tqdm

from api.iris import TimetableStop
from config import redis_url
from database.chunk_limits import get_date_limits
from database.engine import sessionfactory
from database.plan_by_id import PlanByIdV4
from helpers.date_range import date_range, date_range_len


def extract_station_names(service_date: date) -> set[str]:
    """
    For each historic delay datapoint, there is a path of stations that
    the train passed through. If a new station is build, it probably
    lies on one of these paths. This function extracts all station names
    from the paths of a chunk of historic delay datapoints.

    Parameters
    ----------
    chunk_limits : Tuple[int, int]
        Upper and lower limit of the hash_id of the chunk to process

    Returns
    -------
    Set[str]
        Set of station names
    """

    station_names = set()

    engine, Session = sessionfactory()
    with Session() as session:
        plans = PlanByIdV4.get_stops_of_service_date(session, service_date)

    for plan in plans:
        plan = TimetableStop(plan.plan)
        if plan.arrival is not None and plan.arrival.planned_path is not None:
            station_names.update(plan.arrival.planned_path)

        if plan.departure is not None and plan.departure.planned_path is not None:
            station_names.update(plan.departure.planned_path)

    return station_names


def find_stations() -> set[str]:
    """
    Find all station names that exist in the historic delay data.

    Returns
    -------
    Set[str]
        Set of all station names
    """
    engine, Session = sessionfactory()

    station_names = set()

    with Session() as session:
        date_limits = get_date_limits(session, PlanByIdV4.service_date)

    with ProcessPoolExecutor(max_workers=16) as executor:
        for names in tqdm(
            executor.map(extract_station_names, date_range(*date_limits)),
            total=date_range_len(*date_limits),
            desc='Finding stations',
        ):
            station_names.update(names)
            print(f'Found {len(station_names)} stations')

    return station_names


def main():
    redis_client = Redis.from_url(redis_url)

    stations = find_stations()
    print(stations)
    redis_client.sadd('station_names', *stations)


if __name__ == '__main__':
    main()
