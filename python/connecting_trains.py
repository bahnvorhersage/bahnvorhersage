from datetime import date, timedelta

import matplotlib.pyplot as plt
import polars as pl
import tueplots.bundles

from helpers import TrainStopStolli
from helpers.plt_legend_top import LEG_TOP_KWARGS
from ml_models.data_preperation import filter_useful
from ml_models.xgboost_single import load_ml_data
from public_config import OFFSET

CONNECTIONS_PATH = 'cache/train_stops_connections/{service_date}.parquet'


def generate_connecting_trains(service_date: date):
    arrivals = filter_useful(
        TrainStopStolli.load_data(service_date=service_date).filter(
            TrainStopStolli.final_filter & pl.col('is_arrival')
        )
    ).rename(lambda s: 'ar_' + s)
    departures = filter_useful(
        TrainStopStolli.load_data(service_date=service_date).filter(
            TrainStopStolli.final_filter & ~pl.col('is_arrival')
        )
    ).rename(lambda s: 'dp_' + s)

    # arrivals = filter_useful(arrivals)
    # departures = filter_useful(departures)

    # Join arrivals with departures that use the same stop and depart within 10 minutes
    # of the arrival
    connecting_trains = arrivals.join_where(
        departures,
        pl.col('ar_stop_id') == pl.col('dp_stop_id'),
        pl.col('ar_trip_id') != pl.col('dp_trip_id'),
        pl.col('ar_trip_headsign').abs() != pl.col('dp_trip_headsign').abs(),
        pl.col('dp_time_schedule').is_between(
            pl.col('ar_time_schedule'),
            pl.col('ar_time_schedule') + timedelta(minutes=10),
        ),
        suffix='',
    )

    # Only select ids necessary to join with real data
    connecting_trains = connecting_trains.select(
        [
            'ar_trip_id',
            'ar_stop_sequence',
            'ar_time_schedule',
            'dp_trip_id',
            'dp_stop_sequence',
            'dp_time_schedule',
        ]
    )
    connecting_trains.collect().write_parquet(
        CONNECTIONS_PATH.format(service_date=service_date.isoformat())
    )


def create_prediction_dataset(service_date: date):
    connecting_trains = pl.read_parquet(
        CONNECTIONS_PATH.format(service_date=service_date.isoformat())
    ).lazy()
    train_stops = load_ml_data(service_date, include_join_ids=True).lazy()

    connecting_trains = connecting_trains.join(
        train_stops.rename(lambda s: 'ar_' + s),
        on=['ar_trip_id', 'ar_stop_sequence'],
        # right_on=['trip_id', 'stop_sequence'],
        how='inner',
    )
    connecting_trains = connecting_trains.join(
        train_stops.rename(lambda s: 'dp_' + s),
        on=['dp_trip_id', 'dp_stop_sequence'],
        # right_on=['trip_id', 'stop_sequence'],
        how='inner',
    )

    connecting_trains = connecting_trains.with_columns(
        (
            (pl.col('dp_time_schedule') - pl.col('ar_time_schedule')).dt.total_seconds()
            // 60
        ).alias('schedule_transfer_time'),
        pl.lit(2).alias('minimal_transfer_time'),
    ).with_columns(
        (
            pl.col('schedule_transfer_time')
            - pl.col('ar_delay_prognosed')
            + pl.col('dp_delay_prognosed')
        ).alias('prognosed_transfer_time'),
        (
            pl.col('schedule_transfer_time')
            - pl.col('ar_delay_prognosed')
            - (pl.col('ar_delay_diff') - OFFSET)
            + pl.col('dp_delay_prognosed')
            + (pl.col('dp_delay_diff') - OFFSET)
        ).alias('final_transfer_time'),
    )

    arrivals = connecting_trains.select(pl.selectors.starts_with('ar_')).rename(
        lambda s: s.removeprefix('ar_')
    )
    departures = connecting_trains.select(pl.selectors.starts_with('dp_')).rename(
        lambda s: s.removeprefix('dp_')
    )
    transfer_times = connecting_trains.select(
        [
            'schedule_transfer_time',
            'prognosed_transfer_time',
            'minimal_transfer_time',
            'final_transfer_time',
        ]
    )

    return arrivals.collect(), departures.collect(), transfer_times.collect()


def plot_transfer_score_goodnes(
    transfer_times: pl.DataFrame, title: str, save_as: str | None = None
):
    fig, ax = plt.subplots()
    ax: plt.Axes

    transfer_times = transfer_times.sort('breakpoint')

    ax.step(
        transfer_times['breakpoint'],
        transfer_times['transfer_worked'],
        where='pre',
        label='Vorhersage',
    )
    ax.plot([0, 1], [0, 1], label='Perfekte Vorhersage', linestyle=':')
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.set_aspect('equal')
    ax.grid(True)

    ax.set_xlabel('Vorhergesagter Anteil funktionierender Umstiege')
    ax.set_ylabel('Anteil funktionierender Umstiege')

    fig.suptitle(title)

    fig.legend(ncols=3, **LEG_TOP_KWARGS)

    if save_as is not None:
        fig.savefig(save_as, dpi=700)
    else:
        plt.show()


def transfer_score_evaluation(service_date: date):
    # from ml_models.single_predictor import SinglePredictor

    # predictor = SinglePredictor()
    # arrivals, departures, transfer_times = create_prediction_dataset(service_date)
    # transfer_scores = predictor.predict_ar_dp(arrivals, departures, transfer_times)

    # transfer_times = transfer_times.with_columns(
    #     pl.Series('transfer_score', transfer_scores)
    # )

    # transfer_times.write_parquet('transfer_times.parquet')
    transfer_times = pl.read_parquet('transfer_times.parquet')

    transfer_times = transfer_times.with_columns(
        pl.col('transfer_score').qcut(30, include_breaks=True).alias('cut')
    ).unnest('cut')

    transfer_times = transfer_times.group_by(pl.col('category')).agg(
        pl.len().alias('count'),
        ((pl.col('final_transfer_time') - pl.col('minimal_transfer_time')) >= 0)
        .mean()
        .alias('transfer_worked'),
        pl.col('breakpoint').first(),
    )

    plot_transfer_score_goodnes(
        transfer_times,
        'Transfer-Score Qualität',
        save_as='cache/plots/transfer_score_goodness.png',
    )


def main():
    plt.rcParams.update(tueplots.bundles.beamer_moml())

    generate_connecting_trains(date(2024, 10, 2))
    transfer_score_evaluation(date(2024, 10, 2))


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
