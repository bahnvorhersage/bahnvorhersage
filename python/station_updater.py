import json
from concurrent.futures import ThreadPoolExecutor
from dataclasses import asdict, dataclass, replace

import geopy.distance
import pandas as pd
import polars as pl
from redis import Redis
from tqdm import tqdm

import api.db_rest as db_rest
import api.iris as iris
import api.ris as ris
from api.ris import (
    all_matches_by_name,
)
from config import redis_url
from database.engine import DB_URI
from helpers.retry import retry
from helpers.StationPhillip import PL_DB_SCHEMA_STATION_V3, StationPhillip, StationV3


@dataclass
class DerfStation:
    ds100: str
    eva: int
    lat: float
    lon: float
    name: str


def load_derf(path: str) -> dict[str, DerfStation]:
    stations = json.load(open(path))
    stations = {
        s['name']: DerfStation(
            ds100=s['ds100'],
            eva=s['eva'],
            name=s['name'],
            lat=s['latlong'][0],
            lon=s['latlong'][1],
        )
        for s in stations
    }
    return stations


def get_found_station_names() -> set[str]:
    redis_client = Redis.from_url(redis_url)

    station_names = redis_client.smembers('station_names')
    station_names = set(map(lambda x: x.decode('utf-8'), station_names))

    station_names.update(set(StationPhillip().stations['name']))

    return station_names


def sluggify(name: str) -> str:
    return (
        name.lower()
        .replace(' ', '-')
        .replace('(', '')
        .replace(')', '')
        .replace('ß', 'ss')
        .replace('ä', 'a')
        .replace('ö', 'o')
        .replace('ü', 'u')
    )


def find_match_manual(name: str) -> str:
    matches = all_matches_by_name(name)
    matches = set(map(lambda m: m.name, matches))
    more_matches = db_rest.get_matching_stops(name)
    matches.update(map(lambda m: m.name, more_matches))
    matches = list(matches)

    if len(matches):
        print(f'\nWhat is the best match for {name}?\n')
        for i, match in enumerate(matches):
            print(f'{i}: {match}')

        choice = input('\nEnter the number of the best match or new search string: ')
        if not choice.isdigit():
            if choice == 'n':
                return None
            return find_match_manual(choice)

        choice = int(choice)
        return matches[choice]
    else:
        print(f'No matches found for {name}')


def add_info_to_station(
    station: StationV3, other_station: StationV3
) -> tuple[StationV3, StationV3 | None]:
    if station.eva is None and other_station.eva is not None:
        station.eva = other_station.eva
    if station.ds100 is None and other_station.ds100 is not None:
        station.ds100 = other_station.ds100
    if station.lat is None and other_station.lat is not None:
        station.lat = other_station.lat
    if station.lon is None and other_station.lon is not None:
        station.lon = other_station.lon
    if station.is_active_ris is None and other_station.is_active_ris is not None:
        station.is_active_ris = other_station.is_active_ris
    if station.is_active_iris is None and other_station.is_active_iris is not None:
        station.is_active_iris = other_station.is_active_iris
    if station.meta_evas is None and other_station.meta_evas is not None:
        station.meta_evas = other_station.meta_evas
    if (
        station.available_transports is None
        and other_station.available_transports is not None
    ):
        station.available_transports = other_station.available_transports

    if station == other_station:
        # station is the correct currently active station, just add info
        return station, None
    else:
        # station is outdated, add info and add new station
        if other_station.is_active_ris:
            station.is_active_ris = False
        if other_station.is_active_iris:
            station.is_active_iris = False

        return station, other_station


@retry(3)
def complete_station(
    to_complete: StationV3,
    stations: StationPhillip,
    trainline_stations: pl.DataFrame,
    is_alternative_station=False,
) -> list[StationV3]:
    new_ris_station = None
    new_iris_station = None

    if to_complete.eva is None:
        if to_complete.lat is not None and to_complete.lon is not None:
            # try to find an eva by looking for all close stations
            ris_stations = ris.get_stations(lat=to_complete.lat, lon=to_complete.lon)
            if len(ris_stations):
                distances = list(
                    map(
                        lambda s: geopy.distance.distance(
                            (to_complete.lat, to_complete.lon), (s.lat, s.lon)
                        ).meters,
                        ris_stations,
                    )
                )

            print(f'Need to find EVA for: {to_complete}')
            print('Closest stations:')

            for i, (ris_station, distance) in enumerate(zip(ris_stations, distances)):
                if i > 10:
                    break
                print(f'{i}: \t{ris_station.name}: \t{distance:.0f}m')

            choice = input('Enter the number of the best match:')
            if not choice.isdigit():
                raise ValueError('No EVA and no location given')
            choice = int(choice)
            new_ris_station = ris_stations[choice]

            to_complete.eva = new_ris_station.eva
            to_complete.ds100 = new_ris_station.ds100

            to_complete, new_ris_station = add_info_to_station(
                to_complete, new_ris_station
            )

            # Add all info of this station to the current station
        else:
            raise ValueError('No EVA and no location given')

    if to_complete.ds100 is None:
        # try to find ds100 by looking up eva in iris and db-rest
        station = iris.get_station(search_term=to_complete.eva) or db_rest.get_station(
            eva=to_complete.eva
        )
        if station is not None:
            to_complete, new_iris_station = add_info_to_station(to_complete, station)

    if to_complete.lat is None or to_complete.lon is None:
        # try to find lat and lon by looking up eva in ris and db-rest,
        # search in current stations or ask user for location
        ris_station = ris.get_station(eva=to_complete.eva) or db_rest.get_station(
            eva=to_complete.eva
        )
        if ris_station is not None:
            to_complete, _ = add_info_to_station(to_complete, ris_station)
        else:
            # Try to find location in current stations
            try:
                location = stations.get_location(eva=to_complete.eva)
            except KeyError:
                location = (None, None)

            # Try to find location in trainline
            if None in location:
                try:
                    trainline_station = trainline_stations.row(
                        by_predicate=(pl.col('name') == to_complete.name), named=True
                    )
                    location = (
                        trainline_station['latitude'],
                        trainline_station['longitude'],
                    )
                except pl.exceptions.NoRowsReturnedError:
                    pass

            if None in location:
                print(f'No location found for {to_complete}')
                lat = input('Enter latitude: ')
                lon = input('Enter longitude: ')
                to_complete.lat = float(lat)
                to_complete.lon = float(lon)
            else:
                to_complete.lat, to_complete.lon = location

    if to_complete.is_active_ris is None:
        # check if eva and name are in ris
        ris_station = ris.get_station(eva=to_complete.eva)
        if ris_station is not None:
            to_complete, new_ris_station = add_info_to_station(to_complete, ris_station)

    if to_complete.is_active_iris is None or to_complete.meta_evas is None:
        # both must come from iris
        station = iris.get_station(search_term=to_complete.eva)
        if station is not None:
            to_complete, new_iris_station = add_info_to_station(to_complete, station)

    if to_complete.available_transports is None:
        # find available transports by eva in ris
        station = ris.get_station(eva=to_complete.eva) or db_rest.get_station(
            eva=to_complete.eva
        )
        if station is not None:
            to_complete, _ = add_info_to_station(to_complete, station)

    if not is_alternative_station and new_ris_station is not None:
        new_ris_stations = complete_station(
            new_ris_station, stations, trainline_stations, is_alternative_station=True
        )
        if len(new_ris_stations) > 1 and any(
            s != to_complete for s in new_ris_stations[1:]
        ):
            print(
                f'WARNING: More than one station found for new ris station: {new_ris_stations}'
            )

        new_ris_station = new_ris_stations[0]

    if not is_alternative_station and new_iris_station is not None:
        new_iris_stations = complete_station(
            new_iris_station, stations, trainline_stations, is_alternative_station=True
        )
        if len(new_iris_stations) > 1 and any(
            s != to_complete for s in new_iris_stations[1:]
        ):
            print(
                f'WARNING: More than one station found for new iris station: {new_iris_stations}'
            )
        new_iris_station = new_iris_stations[0]

    return [
        s for s in [to_complete, new_ris_station, new_iris_station] if s is not None
    ]


def check_is_active(station: StationV3):
    ris_station = ris.get_station(eva=station.eva)
    if ris_station is not None and ris_station == station:
        station.is_active_ris = True
    else:
        station.is_active_ris = False

    iris_station = iris.get_station(search_term=station.eva)
    if iris_station is not None and iris_station == station:
        station.is_active_iris = True
    else:
        station.is_active_iris = False

    return station


def find_stations() -> list[StationV3]:
    station_names = get_found_station_names()
    station_names = set(iris.filter_unknown_betriebstelle(station_names))

    stations = StationPhillip()
    trainline_stations = pl.read_csv(
        'cache/trainline_stations.csv', separator=';', infer_schema_length=10000
    )

    found = list()

    with ThreadPoolExecutor() as executor:
        # Search ris by name
        for name, station in tqdm(
            executor.map(lambda name: (name, ris.get_station(name)), station_names),
            desc='Searching RIS',
            total=len(station_names),
        ):
            if station is not None:
                found.append(station)

        missing = station_names - set(f.name for f in found)
        print(f'Found {len(found)} stations in RIS, missing {len(missing)} stations')

        # db-rest
        for name, station in tqdm(
            zip(missing, executor.map(db_rest.get_station, missing)),
            total=len(missing),
            desc='Searching db-rest',
        ):
            if station is not None:
                found.append(station)
        missing -= set(f.name for f in found)

        # Derf current stations
        derf_current = load_derf('cache/derf_stations.json')
        for name in missing:
            if name in derf_current:
                found.append(
                    StationV3(
                        eva=int(derf_current[name].eva),
                        ds100=derf_current[name].ds100,
                        name=name,
                        lat=derf_current[name].lat,
                        lon=derf_current[name].lon,
                    )
                )

        missing -= set(f.name for f in found)

        # Derf old stations
        derf_old = load_derf('cache/derf_old_stations.json')
        for name in missing:
            if name in derf_old:
                found.append(
                    StationV3(
                        eva=int(derf_old[name].eva),
                        ds100=derf_old[name].ds100,
                        name=name,
                        lat=derf_old[name].lat,
                        lon=derf_old[name].lon,
                    )
                )

        missing -= set(f.name for f in found)

        # Derf renamed
        derf_renamed = json.load(open('cache/derf_renamed.json'))
        for name in missing:
            if name in derf_renamed:
                try:
                    found.append(
                        StationV3(
                            eva=int(derf_current[derf_renamed[name]].eva),
                            ds100=derf_current[derf_renamed[name]].ds100,
                            name=name,
                            lat=derf_current[derf_renamed[name]].lat,
                            lon=derf_current[derf_renamed[name]].lon,
                        )
                    )
                except KeyError:
                    pass
        missing -= set(f.name for f in found)

        # Current stations
        for name in missing:
            if name in stations.station_df['name']:
                location = stations.get_location(eva=stations.get_eva(name=name))
                found.append(
                    StationV3(
                        eva=stations.get_eva(name=name),
                        name=name,
                        ds100=stations.get_ds100(eva=stations.get_eva(name=name)),
                        lat=location[0],
                        lon=location[1],
                    )
                )

        missing -= set(f.name for f in found)

        # IRIS
        for name, station in tqdm(
            zip(missing, executor.map(iris.get_station, missing)),
            total=len(missing),
            desc='Searching iris',
        ):
            if station is not None:
                found.append(station)
        missing -= set(f.name for f in found)

        # Trainline
        trainline = trainline_stations.filter(pl.col('name').is_in(missing))
        for row in trainline.iter_rows(named=True):
            found.append(
                StationV3(
                    eva=None,
                    name=row['name'],
                    ds100=None,
                    lat=row['latitude'],
                    lon=row['longitude'],
                )
            )
        missing -= set(f.name for f in found)

        # ZHV
        zhv = pl.read_csv(
            'cache/zHV_aktuell_csv.2024-10-14.csv',
            separator=';',
            infer_schema_length=10000,
            decimal_comma=True,
        ).filter(pl.col('Name').is_in(missing))
        for row in zhv.iter_rows(named=True):
            found.append(
                StationV3(
                    eva=None,
                    name=row['Name'],
                    ds100=None,
                    lat=row['Latitude'],
                    lon=row['Longitude'],
                )
            )
        missing -= set(f.name for f in found)

        # Trainline slugs
        missing_slugs = set(map(sluggify, missing))
        trainline = trainline_stations.filter(pl.col('slug').is_in(missing_slugs))
        for row in trainline.iter_rows(named=True):
            missing_name = next(
                filter(lambda name: sluggify(name) == row['slug'], missing)
            )

            found.append(
                StationV3(
                    eva=None,
                    name=missing_name,
                    ds100=None,
                    lat=row['latitude'],
                    lon=row['longitude'],
                )
            )
        missing -= set(f.name for f in found)

        # Manual Matching
        manual_renames = json.load(open('cache/manual_renames.json'))

        for name in tqdm(missing, desc='Manual matching'):
            if name in manual_renames:
                re_name = manual_renames[name]
            else:
                re_name = find_match_manual(name)
                manual_renames[name] = re_name
                json.dump(manual_renames, open('cache/manual_renames.json', 'w'))

            new_station = ris.get_station(name=re_name) or db_rest.get_station(re_name)
            if new_station is None:
                raise ValueError(f'No station found for manual match {re_name}')
            else:
                before_rename = replace(
                    new_station, name=name, is_active_ris=False, is_active_iris=False
                )
                found.append(before_rename)
                found.append(new_station)
    missing -= set(f.name for f in found)

    found_names = set(f.name for f in found)
    if len(station_names - found_names):
        raise ValueError(f'Could not find stations for {station_names - found_names}')
    if len(missing):
        raise ValueError(f'Could not find stations for {missing}')

    # We might know some stations that are not in the list, add them
    for known_station in stations.station_df.iter_rows(named=True):
        known_station = StationV3(
            eva=known_station['eva'],
            name=known_station['name'],
            ds100=known_station['ds100'],
            lat=known_station['lat'],
            lon=known_station['lon'],
        )
        if known_station not in found:
            found.append(known_station)

    # At this point, many stations have missing values, so we complete them
    completed_stations = []
    for station in tqdm(found, desc='Completing stations'):
        new_completed_stations = complete_station(station, stations, trainline_stations)
        for s in new_completed_stations:
            if s not in completed_stations:
                completed_stations.append(s)
            else:
                print(f'Duplicate station: {s}')

    # In theory, the stations should now have the correct is_active values, but
    # the don't so we do it again instead of fixing the bug
    with ThreadPoolExecutor() as executor:
        activity_checked_stations = list()
        for station in tqdm(
            executor.map(check_is_active, completed_stations),
            desc='Checking activity',
            total=len(completed_stations),
        ):
            activity_checked_stations.append(station)

    return activity_checked_stations


def push_stations_to_db(stations: list[StationV3]):
    # Convert list of enums to list of strings as replace missing values with empty
    # lists
    for station in stations:
        station: StationV3
        if station.available_transports is not None:
            station.available_transports = [
                t.name for t in station.available_transports
            ]
        else:
            station.available_transports = []
        if station.meta_evas is None:
            station.meta_evas = []
        if pd.isnull(station.ds100):
            station.ds100 = None
    unique_final_stations = list(map(asdict, stations))

    # Transform to polars dataframe and write to database
    station_df = pl.from_dicts(unique_final_stations, schema=PL_DB_SCHEMA_STATION_V3)
    station_df = station_df.with_columns(
        pl.col('meta_evas').cast(pl.List(pl.String)).list.join('|'),
        pl.col('available_transports').cast(pl.List(pl.String)).list.join('|'),
    )
    station_df.write_database(
        'stations_v3', connection=DB_URI, if_table_exists='replace'
    )


def main():
    found = find_stations()
    push_stations_to_db(found)


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
