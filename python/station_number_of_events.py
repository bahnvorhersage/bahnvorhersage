from datetime import date, timedelta

import polars as pl

from data_analysis.utils import data_aggregator
from database.engine import DB_URI
from helpers.StationPhillip import StationPhillip


def main():
    number_of_events = data_aggregator(
        filter_expr=pl.col('is_final'),
        group_by=pl.col('stop_id'),
        min_date=date.today() - timedelta(days=365),
    )

    number_of_events = number_of_events.select(['x', 'count']).rename(
        {'x': 'eva', 'count': 'number_of_events'}
    )

    station_df = StationPhillip().station_df.drop('number_of_events')

    station_df = station_df.join(number_of_events, on='eva', how='left')

    station_df = station_df.with_columns(
        pl.col('meta_evas').cast(pl.List(pl.String)).list.join('|'),
        pl.col('available_transports').cast(pl.List(pl.String)).list.join('|'),
    )
    station_df.write_database(
        'stations_v3', connection=DB_URI, if_table_exists='replace'
    )


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
