import enum

import sqlalchemy
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.types import BigInteger

from database.base import Base
from database.engine import sessionfactory
from public_config import LONG_DISTANCE_CATEGORIES


class RouteType(enum.Enum):
    TRAM = 0
    UNDERGROUND = 1
    RAIL = 2
    BUS = 3
    FERRY = 4
    CABLE_CAR = 5
    GONDOLA = 6
    FUNICULAR = 7
    TROLLEYBUS = 11
    MONORAIL = 12


class Routes(Base):
    __tablename__ = 'gtfs_routes'

    route_id: Mapped[int] = mapped_column(
        BigInteger, primary_key=True, autoincrement=False
    )
    agency_id: Mapped[str]
    route_short_name: Mapped[str]
    route_long_name: Mapped[str]
    route_type: Mapped[RouteType]

    def __repr__(self):
        return f'<Routes {self.route_id} {self.route_short_name}>'

    def as_dict(self):
        return {
            'route_id': self.route_id,
            'agency_id': self.agency_id,
            'route_short_name': self.route_short_name,
            'route_long_name': self.route_long_name,
            'route_type': self.route_type,
        }

    def as_ml_dict(self):
        return {
            'route_id': self.route_id,
            'operator': self.agency_id,
            'category': self.route_short_name.split(' ')[0],
            'number': self.route_long_name.rsplit(' ', maxsplit=1)[-1],
            'line': self.route_short_name.split(' ', maxsplit=1)[1],
            'is_regional': self.is_regional(),
        }

    def as_tuple(self):
        return (
            self.route_id,
            self.agency_id,
            self.route_short_name,
            self.route_long_name,
            self.route_type,
        )

    def is_regional(self):
        train_cat = self.route_short_name.split(' ')[0]
        return train_cat not in LONG_DISTANCE_CATEGORIES


class RouteRachel:
    routes: dict[int, Routes]

    def __init__(self):
        self.routes = {route.route_id: route for route in self._get_routes()}

    def _get_routes(self) -> list[Routes]:
        engine, Session = sessionfactory()
        with Session() as session:
            routes = session.scalars(sqlalchemy.select(Routes)).all()
        engine.dispose()
        return routes

    def to_polars(self):
        import polars as pl

        return pl.DataFrame([route.as_ml_dict() for route in self.routes.values()])

    def get_route(self, route_id: int) -> Routes:
        return self.routes[route_id]

    def get_category(self, route_id: int) -> str:
        return self.routes[route_id].route_short_name.split(' ')[0]

    def get_number(self, route_id: int) -> str:
        return self.routes[route_id].route_long_name.rsplit(' ', maxsplit=1)[-1]

    def get_line(self, route_id: int) -> str:
        return self.routes[route_id].route_short_name.split(' ', maxsplit=1)[1]

    def is_regional(self, route_id: int) -> bool:
        return self.get_category(route_id) not in LONG_DISTANCE_CATEGORIES
