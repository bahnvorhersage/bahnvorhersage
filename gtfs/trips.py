from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.types import BigInteger

from database.base import Base


class TripsAbstract(Base):
    __abstract__ = True

    trip_id: Mapped[int] = mapped_column(
        BigInteger, primary_key=True, autoincrement=False
    )
    route_id: Mapped[int] = mapped_column(BigInteger)
    service_id: Mapped[int] = mapped_column(BigInteger)
    trip_headsign: Mapped[int] = mapped_column(BigInteger)
    # shape_id: Mapped[int] = mapped_column(BigInteger)

    def as_tuple(self):
        return (
            self.trip_id,
            self.route_id,
            self.service_id,
            self.trip_headsign,
        )


class Trips(TripsAbstract):
    __tablename__ = 'gtfs_trips'


class TripTemp(TripsAbstract):
    __tablename__ = 'gtfs_trips_temp'
