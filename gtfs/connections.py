from datetime import datetime

import bahnvorhersage_routing as bvr
import sqlalchemy
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.orm import Session as SessionType
from sqlalchemy.types import BigInteger, DateTime

from database.base import Base
from database.engine import sessionfactory
from helpers.hash64 import xxhash64


class Connections(Base):
    __tablename__ = 'csa_connections'

    id: Mapped[int] = mapped_column(BigInteger, primary_key=True, autoincrement=False)
    dp_ts: Mapped[datetime] = mapped_column(DateTime(timezone=True), index=True)
    ar_ts: Mapped[datetime] = mapped_column(DateTime(timezone=True))
    planned_dp_ts: Mapped[datetime] = mapped_column(DateTime(timezone=True))
    planned_ar_ts: Mapped[datetime] = mapped_column(DateTime(timezone=True))
    dp_stop_id: Mapped[int] = mapped_column(BigInteger)
    ar_stop_id: Mapped[int] = mapped_column(BigInteger)
    trip_id: Mapped[int] = mapped_column(BigInteger)
    is_regio: Mapped[bool]
    dist_traveled: Mapped[int]
    dp_platform_id: Mapped[int] = mapped_column(BigInteger)
    ar_platform_id: Mapped[int] = mapped_column(BigInteger)

    @staticmethod
    def create_id(
        planned_dp_ts: datetime,
        planned_ar_ts: datetime,
        dp_stop_id: int,
        ar_stop_id: int,
        trip_id: int,
    ) -> int:
        return xxhash64(
            planned_dp_ts.isoformat()
            + planned_ar_ts.isoformat()
            + str(dp_stop_id)
            + str(ar_stop_id)
            + str(trip_id)
        )

    @staticmethod
    def create_tuple(
        dp_ts: datetime,
        ar_ts: datetime,
        planned_dp_ts: datetime,
        planned_ar_ts: datetime,
        dp_stop_id: int,
        ar_stop_id: int,
        trip_id: int,
        is_regio: bool,
        dist_traveled: int,
        dp_platform_id: int,
        ar_platform_id: int,
    ) -> tuple:
        return (
            Connections.create_id(
                planned_dp_ts, planned_ar_ts, dp_stop_id, ar_stop_id, trip_id
            ),
            dp_ts,
            ar_ts,
            planned_dp_ts,
            planned_ar_ts,
            dp_stop_id,
            ar_stop_id,
            trip_id,
            is_regio,
            dist_traveled,
            dp_platform_id,
            ar_platform_id,
        )

    def __repr__(self) -> str:
        return f'<Connection {self.dp_stop_id} {self.ar_stop_id} at {self.dp_ts} dp_ts and {self.ar_ts} ar_ts>'

    def as_tuple(self) -> tuple:
        return (
            self.id,
            self.dp_ts,
            self.ar_ts,
            self.planned_dp_ts,
            self.planned_ar_ts,
            self.dp_stop_id,
            self.ar_stop_id,
            self.trip_id,
            self.is_regio,
            self.dist_traveled,
            self.dp_platform_id,
            self.ar_platform_id,
        )

    @staticmethod
    def get_routing_connections(
        session: SessionType, from_ts: datetime, to_ts: datetime
    ) -> bvr.Connections:
        cursor = session.connection().connection.cursor()
        sql = 'SELECT * FROM csa_connections WHERE dp_ts >= %(from)s AND dp_ts < %(to)s ORDER BY dp_ts;'
        cursor.execute(sql, {'from': from_ts, 'to': to_ts})

        connections = bvr.Connections()
        for row in cursor:
            connections.append(
                dp_ts=int(row[1].timestamp()),
                ar_ts=int(row[2].timestamp()),
                dp_stop_id=row[5],
                ar_stop_id=row[6],
                trip_id=row[7],
                is_regio=row[8],
                dist_traveled=max(row[9], 0),
                dp_platform_id=row[10],
                ar_platform_id=row[11],
            )
        cursor.close()
        return connections


class ConnectionsTemp(Base):
    __tablename__ = 'csa_connections_temp'

    id: Mapped[int] = mapped_column(BigInteger, primary_key=True)
    dp_ts: Mapped[datetime] = mapped_column(DateTime(timezone=True), index=True)
    ar_ts: Mapped[datetime] = mapped_column(DateTime(timezone=True))
    planned_dp_ts: Mapped[datetime] = mapped_column(DateTime(timezone=True))
    planned_ar_ts: Mapped[datetime] = mapped_column(DateTime(timezone=True))
    dp_stop_id: Mapped[int] = mapped_column(BigInteger)
    ar_stop_id: Mapped[int] = mapped_column(BigInteger)
    trip_id: Mapped[int] = mapped_column(BigInteger)
    is_regio: Mapped[bool]
    dist_traveled: Mapped[int]
    dp_platform_id: Mapped[int] = mapped_column(BigInteger)
    ar_platform_id: Mapped[int] = mapped_column(BigInteger)

    @staticmethod
    def clear(session: SessionType):
        session.execute(
            sqlalchemy.text(f'TRUNCATE {ConnectionsTemp.__table__.fullname}')
        )
        session.commit()


if __name__ == '__main__':
    engine, Session = sessionfactory()
    with Session() as session:
        from_ts = datetime(2024, 1, 1, 0, 0, 0)
        to_ts = datetime(2024, 1, 2, 0, 0, 0)
        connections = Connections.get(session, from_ts, to_ts)
