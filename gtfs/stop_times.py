import enum
from datetime import date, datetime

from sqlalchemy.dialects.postgresql import ENUM as PgEnum
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.types import BigInteger, DateTime

from api.iris import Event
from database.base import Base, create_all
from database.engine import get_engine

# Partman partitioning
# SELECT partman.create_parent(
#     p_parent_table := 'public.gtfs_stop_times'
#     , p_control := 'service_date'
#     , p_interval := '1 day'
#     , p_start_partition := '2021-09-01'
#     , p_premake := '60'
# );


class PickUpDropOffType(enum.Enum):
    REGULAR = 0
    NOT_AVAILABLE = 1
    PHONE_AGENCY = 2
    COORDINATE_WITH_DRIVER = 3

    def __str__(self) -> str:
        return self.name

    @staticmethod
    def from_event(event: Event) -> 'PickUpDropOffType':
        if event.hidden:
            return PickUpDropOffType.NOT_AVAILABLE
        else:
            return PickUpDropOffType.REGULAR


class StopTimesAbstract(Base):
    __abstract__ = True

    service_date: Mapped[date] = mapped_column(primary_key=True)
    trip_id: Mapped[int] = mapped_column(
        BigInteger, primary_key=True, autoincrement=False
    )
    stop_sequence: Mapped[int] = mapped_column(primary_key=True, autoincrement=False)
    stop_id: Mapped[int] = mapped_column(BigInteger)
    arrival_time: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=True
    )
    departure_time: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=True
    )
    shape_dist_traveled: Mapped[float]
    route_id: Mapped[int] = mapped_column(BigInteger)
    trip_headsign: Mapped[int] = mapped_column(BigInteger)
    is_betriebshalt: Mapped[bool]
    pickup_type: Mapped[PickUpDropOffType] = mapped_column(
        PgEnum(PickUpDropOffType, create_type=False), nullable=True
    )
    drop_off_type: Mapped[PickUpDropOffType] = mapped_column(
        PgEnum(PickUpDropOffType, create_type=False), nullable=True
    )

    def as_tuple(self):
        return (
            self.service_date,
            self.trip_id,
            self.stop_sequence,
            self.stop_id,
            self.arrival_time,
            self.departure_time,
            self.shape_dist_traveled,
            self.route_id,
            self.trip_headsign,
            self.is_betriebshalt,
            self.pickup_type,
            self.drop_off_type,
        )


class StopTimes(StopTimesAbstract):
    __tablename__ = 'gtfs_stop_times'
    __table_args__ = {
        'postgresql_partition_by': 'RANGE (service_date)',
    }


if __name__ == '__main__':
    create_all(get_engine())
