from datetime import date, datetime

from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.types import BOOLEAN, BigInteger, DateTime

from database.base import Base

# Partman partitioning
# SELECT partman.create_parent(
#     p_parent_table := 'public.gtfs_trip_update'
#     , p_control := 'service_date'
#     , p_interval := '1 day'
#     , p_start_partition := '2021-09-01'
#     , p_premake := '60'
# );


class TripUpdateAbstract(Base):
    __abstract__ = True

    service_date: Mapped[date] = mapped_column(primary_key=True)
    trip_id: Mapped[int] = mapped_column(
        BigInteger, primary_key=True, autoincrement=False
    )
    stop_sequence: Mapped[int] = mapped_column(primary_key=True, autoincrement=False)
    timestamp: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), primary_key=True
    )
    stop_id: Mapped[int] = mapped_column(BigInteger)
    changed_stop_id: Mapped[int] = mapped_column(BigInteger, nullable=True)
    arrival_time: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=True
    )
    departure_time: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), nullable=True
    )
    arrival_cancelled: Mapped[bool] = mapped_column(BOOLEAN, nullable=True)
    departure_cancelled: Mapped[bool] = mapped_column(BOOLEAN, nullable=True)

    def as_tuple(self):
        return (
            self.service_date,
            self.trip_id,
            self.stop_sequence,
            self.timestamp,
            self.stop_id,
            self.changed_stop_id,
            self.arrival_time,
            self.departure_time,
            self.arrival_cancelled,
            self.departure_cancelled,
        )


class TripUpdate(TripUpdateAbstract):
    __tablename__ = 'gtfs_trip_update'
    __table_args__ = {
        'postgresql_partition_by': 'RANGE (service_date)',
    }


if __name__ == '__main__':
    from database.base import create_all
    from database.engine import get_engine

    create_all(get_engine())
