import warnings
from dataclasses import dataclass
from enum import Enum

import geopy.distance
import pandas as pd
import polars as pl

from database.cached_table_fetch import cached_sql_fetch_pl, cached_table_fetch
from helpers.cache import ttl_lru_cache
from public_config import CACHE_TIMEOUT_SECONDS


class AvailableTransports(Enum):
    HIGH_SPEED_TRAIN = 1
    INTERCITY_TRAIN = 2
    INTER_REGIONAL_TRAIN = 3
    REGIONAL_TRAIN = 4
    CITY_TRAIN = 5
    SUBWAY = 6
    TRAM = 7
    BUS = 8
    FERRY = 9
    TAXI = 10
    SHUTTLE = 11
    FLIGHT = 12

    @staticmethod
    def from_string(transport: str) -> 'AvailableTransports':
        match transport:
            case 'HIGH_SPEED_TRAIN':
                return AvailableTransports.HIGH_SPEED_TRAIN
            case 'INTERCITY_TRAIN':
                return AvailableTransports.INTERCITY_TRAIN
            case 'INTER_REGIONAL_TRAIN':
                return AvailableTransports.INTER_REGIONAL_TRAIN
            case 'REGIONAL_TRAIN':
                return AvailableTransports.REGIONAL_TRAIN
            case 'CITY_TRAIN':
                return AvailableTransports.CITY_TRAIN
            case 'SUBWAY':
                return AvailableTransports.SUBWAY
            case 'TRAM':
                return AvailableTransports.TRAM
            case 'BUS':
                return AvailableTransports.BUS
            case 'FERRY':
                return AvailableTransports.FERRY
            case 'TAXI':
                return AvailableTransports.TAXI
            case 'SHUTTLE':
                return AvailableTransports.SHUTTLE
            case 'FLIGHT':
                return AvailableTransports.FLIGHT
            case _:
                raise ValueError(f'Unknown transport type {transport}')


@dataclass(eq=False)
class StationV3:
    eva: int | None
    name: str | None
    ds100: str | None
    lat: float | None
    lon: float | None
    is_active_ris: bool | None = None
    is_active_iris: bool | None = None
    meta_evas: list[int] | None = None
    number_of_events: int | None = None
    available_transports: list[AvailableTransports] | None = None

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, StationV3):
            return NotImplemented

        if not isinstance(self.eva, int) or not isinstance(other.eva, int):
            return NotImplemented

        return (self.eva == other.eva) and (self.name == other.name)

    @staticmethod
    def from_dict(data: dict):
        data['available_transports'] = [
            AvailableTransports.from_string(transport)
            for transport in data['available_transports']
        ]
        return StationV3(**data)


PL_DB_SCHEMA_STATION_V3 = {
    'eva': pl.Int32,
    'ds100': pl.String,
    'lat': pl.Float64,
    'lon': pl.Float64,
    'name': pl.String,
    'is_active_ris': pl.Boolean,
    'is_active_iris': pl.Boolean,
    'meta_evas': pl.List(pl.Int32),
    'number_of_events': pl.Int32,
    'available_transports': pl.List(pl.Enum(AvailableTransports.__members__.keys())),
}


class StationPhillip:
    def __init__(self, init=True, **kwargs):
        if 'generate' in kwargs:
            kwargs['generate'] = False
            print('StationPhillip does not support generate')
        self.kwargs = kwargs

        if init:
            self.stations
            self.stations_by_eva
            self.eva_by_name

    @property
    @ttl_lru_cache(CACHE_TIMEOUT_SECONDS, 1)
    def station_df(self) -> pl.DataFrame:
        station_df = cached_sql_fetch_pl(
            'SELECT * FROM stations_v3', **self.kwargs
        ).with_columns(
            pl.col('meta_evas')
            .str.split('|')
            .cast(PL_DB_SCHEMA_STATION_V3['meta_evas'])
            .list.drop_nulls(),
            pl.col('available_transports')
            .str.split('|')
            .cast(PL_DB_SCHEMA_STATION_V3['available_transports'])
            .list.drop_nulls(),
        )

        return station_df

    @ttl_lru_cache(CACHE_TIMEOUT_SECONDS, 1)
    def get_ris_active_names(self) -> list[str]:
        return (
            self.station_df.filter(pl.col('is_active_ris'))
            .sort('number_of_events', descending=True, nulls_last=True)['name']
            .unique(maintain_order=True)
            .to_list()
        )

    @ttl_lru_cache(CACHE_TIMEOUT_SECONDS, 1)
    def get_iris_active_evas(self) -> list[int]:
        return (
            self.station_df.filter(pl.col('is_active_iris'))['eva'].unique().to_list()
        )

    @ttl_lru_cache(CACHE_TIMEOUT_SECONDS, 1)
    def get_all_evas(self) -> list[int]:
        return self.station_df['eva'].unique().to_list()

    def _eva_score_df(self) -> pl.DataFrame:
        return self.station_df.with_columns(
            (pl.col('eva') - 8050000).abs().alias('eva_score')
        ).sort('eva_score')

    @property
    @ttl_lru_cache(CACHE_TIMEOUT_SECONDS, 1)
    def eva_by_name(self) -> dict[str, int]:
        # Some station names are mapped to multiple evas. We define the best match
        # as the eva closest to 8050000.
        eva_matches = (
            self._eva_score_df()
            .group_by('name')
            .agg(pl.col('eva').first().alias('eva'))
        )

        mapping = dict(zip(eva_matches['name'].to_list(), eva_matches['eva'].to_list()))
        return mapping

    @property
    @ttl_lru_cache(CACHE_TIMEOUT_SECONDS, 1)
    def stations_by_eva(self) -> dict[int, StationV3]:
        station_df_dicts = (
            self.station_df.sort(['is_active_ris', 'is_active_iris'])
            .unique(['eva'])
            .to_dicts()
        )
        return {
            station['eva']: StationV3.from_dict(station) for station in station_df_dicts
        }

    @property
    @ttl_lru_cache(CACHE_TIMEOUT_SECONDS, 1)
    def stations(self) -> pd.DataFrame:
        warnings.warn(
            'stations is deprecated, use station_df, eva_by_name, stations_by_eva instead',
            DeprecationWarning,
        )

        stations = cached_table_fetch('stations', **self.kwargs)
        if 'valid_from' not in stations.columns:
            stations['valid_from'] = pd.NaT
        if 'valid_to' not in stations.columns:
            stations['valid_to'] = pd.NaT
        stations['valid_from'] = pd.to_datetime(stations['valid_from'])
        stations['valid_from'] = stations['valid_from'].fillna(pd.Timestamp.min)
        stations['valid_to'] = pd.to_datetime(stations['valid_to'])
        stations['valid_to'] = stations['valid_to'].fillna(pd.Timestamp.max)
        stations['eva'] = stations['eva'].astype(int)
        stations['name'] = stations['name'].astype(pd.StringDtype())
        stations['ds100'] = stations['ds100'].astype(pd.StringDtype())

        stations.set_index(
            ['name', 'eva', 'ds100'],
            drop=False,
            inplace=True,
        )
        stations.index.set_names(['name', 'eva', 'ds100'], inplace=True)
        stations = stations.sort_index()
        return stations

    def to_gdf(self):
        """
        Convert stations to geopandas GeoDataFrame.

        Returns
        -------
        geopandas.GeoDataFrame
            Stations with coordinates as geometry for geopandas.GeoDataFrame.
        """
        import geopandas as gpd

        pd_df = self.station_df.to_pandas()

        return gpd.GeoDataFrame(
            pd_df,
            geometry=gpd.points_from_xy(pd_df['lon'], pd_df['lat']),
        ).set_crs('EPSG:4326')

    def get_eva(self, name: str) -> int:
        return self.eva_by_name[name]

    def get_name(self, eva: int) -> str:
        return self.stations_by_eva[eva].name

    def get_ds100(self, eva: int) -> str:
        return self.stations_by_eva[eva].ds100

    def get_location(self, eva):
        station = self.stations_by_eva[eva]
        return station.lat, station.lon

    def geographic_distance(
        self,
        name1: str,
        name2: str,
    ) -> float:
        coords_1 = self.get_location(eva=self.get_eva(name=name1))
        coords_2 = self.get_location(eva=self.get_eva(name=name2))

        return geopy.distance.great_circle(coords_1, coords_2).meters

    def geographic_distance_by_eva(
        self,
        eva_1: int,
        eva_2: int,
    ) -> float:
        coords_1 = self.get_location(eva=eva_1)
        coords_2 = self.get_location(eva=eva_2)

        return geopy.distance.distance(coords_1, coords_2).meters


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    stations = StationPhillip(prefer_cache=False, init=False)

    print(stations.station_df)
    print(stations.get_ris_active_names())
    print(stations.eva_by_name)
    print(stations.stations_by_eva)

    print(stations.get_eva(name='Tübingen Hbf'))
    print(stations.get_eva(ds100='TT'))

    print(stations.stations_by_eva[8000141])
    print(stations.get_location(eva=8000141))

    print(stations.sta_list[:10])
