from datetime import date, timedelta


def date_range_len(start_date: date, end_date: date):
    return (end_date - start_date).days


def date_range(start_date: date, end_date: date):
    for n in range((end_date - start_date).days):
        yield start_date + timedelta(days=n)
