def group_list_of_dict(lst: list[dict], key: str) -> dict:
    """
    Group a list of dictionaries by a key.

    Args:
        lst (list[dict]): List of dictionaries.
        key (str): Key to group by.

    Returns:
        dict: Dictionary with key as key and list of dictionaries as value.
    """
    grouped = {}
    for item in lst:
        grouped.setdefault(item[key], []).append(item)
    return grouped
