def iter_triples(iterable):
    previous = None
    current = None
    for next in iterable:
        if current is not None:
            yield (previous, current, next)
        previous = current
        current = next

    yield (previous, current, None)
