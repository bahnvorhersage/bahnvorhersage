import glob
from datetime import date

import polars as pl

from public_config import PROGRESSION_GLOB_PATH


def _file_date(file: str):
    return date.fromisoformat(file.split('/')[-1].split('.')[0])


def _filter_parquet_files(
    min_date: date = None, max_date: date = None, service_date: date = None
):
    parquet_files = glob.glob(PROGRESSION_GLOB_PATH)
    if service_date is not None:
        parquet_files = [
            file for file in parquet_files if _file_date(file) == service_date
        ]
    if min_date:
        parquet_files = [file for file in parquet_files if _file_date(file) >= min_date]
    if max_date:
        parquet_files = [file for file in parquet_files if _file_date(file) < max_date]

    if len(parquet_files) == 0:
        raise ValueError('No files found for the given date range')

    return parquet_files


def days_of_data_available(**kwargs):
    return len(_filter_parquet_files(**kwargs))


def load_data(min_date: date = None, max_date: date = None, service_date: date = None):
    parquet_files = _filter_parquet_files(min_date, max_date, service_date)

    df = pl.scan_parquet(parquet_files).filter(~pl.col('is_betriebshalt'))
    return df


final_filter = pl.col('is_final')


if __name__ == '__main__':
    mini = load_data()['time_schedule'].min()
