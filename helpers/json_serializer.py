from dataclasses import asdict, is_dataclass
from datetime import date, datetime, timedelta

import isodate


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if is_dataclass(obj):
        return asdict(obj)
    if isinstance(obj, datetime | date):
        return obj.isoformat()
    elif isinstance(obj, timedelta):
        return isodate.duration_isoformat(obj)
    raise TypeError(f'Type {type(obj)} not serializable')


COMPACT_ENCODING_KWARGS = {
    'default': json_serial,
    'separators': (',', ':'),
    'ensure_ascii': False,
}
