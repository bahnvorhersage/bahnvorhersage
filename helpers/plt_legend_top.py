LEG_TOP_KWARGS = {
    'bbox_to_anchor': (0, 1.02, 1, 0.2),
    'loc': 'lower left',
    'mode': 'expand',
    'borderaxespad': 0,
}
