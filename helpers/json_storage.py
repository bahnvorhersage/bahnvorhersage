import json

import pyzstd

from helpers.hash64 import xxhash64
from helpers.json_serializer import json_serial


def compact_json(data: dict) -> str:
    """Format a dict as a compact and sorted JSON string."""
    return json.dumps(data, sort_keys=True, separators=(',', ':'), default=json_serial)


def hash_json(data: dict) -> int:
    """Create a hash from a dict."""
    return xxhash64(compact_json(data))


def compress_json(data: dict) -> bytes:
    """Compress a dict to a zstd compressed bytes object."""
    return pyzstd.compress(compact_json(data).encode(), level_or_option=3)


def decompress_json(data: bytes) -> dict:
    """Decompress a zstd compressed bytes object to a dict."""
    return json.loads(pyzstd.decompress(data).decode())


if __name__ == '__main__':
    my_str = '{"ar": [{"l": "A1", "pp": "", "ppth": "Hamburg-Eidelstedt|Hamburg-Eidelstedt Zentrum|Hamburg-H\\u00f6rgensweg|Hamburg-Schnelsen|Hamburg Burgwedel|B\\u00f6nningstedt|Hasloh|Quickborn S\\u00fcd", "pt": "2203211218"}], "dp": [{"l": "A1", "pp": "", "ppth": "Ellerau|Tanneneck|Ulzburg S\\u00fcd|Henstedt-Ulzburg|Kaltenkirchen S\\u00fcd|Kaltenkirchen(Holst)|Holstentherme|Dodenhof|N\\u00fctzen|Lentf\\u00f6hrden|Bad Bramstedt Kurhaus|Bad Bramstedt|Wiemersdorf|Gro\\u00dfenaspe|Boostedt|Neum\\u00fcnster S\\u00fcd AKN|Neum\\u00fcnster", "pt": "2203211221"}], "id": "7453419633865755652-2203211156-9", "station": "Quickborn", "tl": [{"c": "AKN", "f": "D", "n": "82455", "o": "A0", "t": "p"}]}'

    # Memory usage of my_str:
    import sys

    print('Memory usage of my_str:', sys.getsizeof(my_str))

    my_compressed_str = pyzstd.compress(my_str.encode(), level_or_option=3)
    print('Memory usage of my_compressed_str:', sys.getsizeof(my_compressed_str))

    another_compressed_str = compress_json(json.loads(my_str))
    print(
        'Memory usage of another_compressed_str:', sys.getsizeof(another_compressed_str)
    )
