import asyncio
import functools
import traceback
from collections.abc import Callable, Coroutine
from dataclasses import dataclass
from datetime import date, datetime, timedelta

import discord
import requests
from discord.ext import tasks

from config import discord_bot_token
from database.engine import sessionfactory
from database.plan_by_id import PlanByIdV4
from database.unique_change import UniqueChangeV4


@dataclass
class Counter:
    current_date: date
    today: int = 0
    tomorrow: int = 0

    def update(self, new_counts: dict[date, int]) -> int:
        # Check if the new counts are for the current or next day
        # and roll over if necessary
        if all(self.current_date < service_date for service_date in new_counts.keys()):
            self.today = self.tomorrow
            self.tomorrow = 0
            self.current_date = date.today()

        additional_count = 0

        for service_date, value in new_counts.items():
            if service_date == self.current_date:
                additional_count += value - self.today
                self.today = value
            elif service_date == self.current_date + timedelta(days=1):
                additional_count += value - self.tomorrow
                self.tomorrow = value
            else:
                print(f'Unexpected date: {service_date}')

        return additional_count


change_count = Counter(current_date=date.today())
plan_count = Counter(current_date=date.today())


def to_thread(func: Callable) -> Coroutine:
    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        loop = asyncio.get_event_loop()
        wrapped = functools.partial(func, *args, **kwargs)
        return await loop.run_in_executor(None, wrapped)

    return wrapper


@to_thread
def monitor_plan() -> str | None:
    message = None
    try:
        with Session() as session:
            new_counts = {
                date.today(): PlanByIdV4.count_at_date(session, date.today()),
                date.today() + timedelta(days=1): PlanByIdV4.count_at_date(
                    session, date.today() + timedelta(days=1)
                ),
            }

        plan_count_delta = plan_count.update(new_counts)

        if plan_count_delta < 500:
            message = f'@everyone The plan gatherer is not working, as {str(plan_count_delta)} new entries where added to database at {str(date.today())}'
        print(f'checked plan {date.today()}: {plan_count_delta} rows were added')

    except Exception:
        message = f'@everyone Error reading Database:\n{traceback.format_exc()}'
        print('checked ' + str(date.today()) + ': ???? rows were added')
        print(message)

    return message


@to_thread
def monitor_change() -> str | None:
    message = None
    try:
        with Session() as session:
            new_counts = {
                date.today(): UniqueChangeV4.count_at_date(session, date.today()),
                date.today() + timedelta(days=1): UniqueChangeV4.count_at_date(
                    session, date.today() + timedelta(days=1)
                ),
            }

        change_count_delta = change_count.update(new_counts)

        if change_count_delta < 500:
            message = f'@everyone The change gatherer is not working, as {str(change_count_delta)} new entries where added to database at {str(date.today())}'
        print(f'checked change {date.today()}: {change_count_delta} rows were added')

    except Exception:
        message = f'@everyone Error reading Database:\n{traceback.format_exc()}'
        print('checked ' + str(date.today()) + ': ???? rows were added')
        print(message)

    return message


async def monitor_website():
    channel = client.get_channel(720671295129518232)
    try:
        print('testing https://bahnvorhersage.de...')
        page = requests.get(
            'https://bahnvorhersage.de', headers={'User-Agent': 'Discord Monitor Bot'}
        )
        if page.ok:
            print('ok')
        else:
            message = f'{datetime.now()}: @everyone Error on the website:\n{str(page)}'
            print(message)
            await channel.send(message)
    except Exception:
        message = f'{datetime.now()}: @everyone Error on the website:\n{traceback.format_exc()}'
        print(message)
        await channel.send(message)

    try:
        print(
            'testing https://bahnvorhersage.de/api/journeys from Tübingen Hbf to Köln Hbf...'
        )
        search = {
            'start': 'Tübingen Hbf',
            'destination': 'Köln Hbf',
            'date': (datetime.now() + timedelta(hours=1)).strftime('%d.%m.%Y %H:%M'),
            'search_for_arrival': False,
        }
        trip = requests.post(
            'https://bahnvorhersage.de/api/journeys',
            json=search,
            headers={'User-Agent': 'Discord Monitor Bot'},
        )
        if trip.ok:
            print('ok')
        else:
            message = (
                f'{datetime.now()}: @everyone Error on the website api:\n{str(trip)}'
            )
            print(message)
            await channel.send(message)
    except Exception:
        message = f'{datetime.now()}: @everyone Error on the website api:\n{traceback.format_exc()}'
        await channel.send(message)
        print(message)


class Monitor(discord.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.old_change_count = 0

    async def setup_hook(self) -> None:
        self.monitor.start()

    async def on_ready(self):
        print(f'{client.user} has connected to Discord!')
        channel = client.get_channel(720671295129518232)
        await channel.send('Data gatherer v3 monitor now active')

    @tasks.loop(hours=1)
    async def monitor(self):
        await client.wait_until_ready()
        channel = client.get_channel(720671295129518232)

        message = await monitor_plan()
        if message is not None:
            await channel.send(message)

        message = await monitor_change()
        if message is not None:
            await channel.send(message)

        await monitor_website()


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    engine, Session = sessionfactory()

    intents = discord.Intents.default()
    intents.message_content = True

    client = Monitor(intents=intents)

    client.run(discord_bot_token)
