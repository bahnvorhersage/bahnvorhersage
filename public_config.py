import os

# Train stop data download
PROGRESSION_PATH = 'cache/train_stops_progression/{service_date}.parquet'
PROGRESSION_GLOB_PATH = 'cache/train_stops_progression/*.parquet'

# ML configuration
MODEL_PATH = os.getenv('MODEL_PATH', default='cache/xgboost_model.ubj')
DATA_SCHEMA_PATH = os.getenv('DATA_SCHEMA_PATH', default='cache/data_schema.pickle')
TRAINING_STATS_PATH = os.getenv(
    'TRAINING_STATS_PATH', default='cache/training_stats.json'
)
# Train on 5 * 7 days, test on 7 days
TRAIN_DAYS = 7
TRAIN_BATCHES = 5
TEST_DAYS = 7

# ML data preparation
OFFSET = 3
TRAIN_COLUMNS = [
    'number',  # 32
    'lat',  # 32
    'lon',  # 32
    'stop_sequence',  # 16
    'distance_traveled',  # 16
    'dwell_time_schedule',  # 16
    'dwell_time_prognosed',  # 16
    'bearing',  # 16
    'delay_prognosed',  # 16
    'minute_of_day',  # 16
    'minutes_to_prognosed_time',  # 32
    'weekday',  # 8
    'is_regional',  # 8
    'is_arrival',  # 8
    'operator',  # enum
    'category',  # enum
    'line',  # enum
]
LABEL_COLUMN = 'delay_diff'
CATEGORICAL_COLUMNS = ['operator', 'category', 'line']


# Crawling
STATIONS_TO_MONITOR_PER_THREAD = 300


# Predictor webserver
PREDICTOR_URL = 'http://10.200.200.2:31870/'


# GTFS
LONG_DISTANCE_CATEGORIES = {
    'IC',
    'EC',
    'ECE',
    'ICE',
    'EN',
    'RJ',
    'RJX',
    'TGV',
    'FLX',
    'NJ',
}

# General
CACHE_TIMEOUT_SECONDS = 60 * 60 * 24  # 24 hours
CACHE_PATH = 'cache'
