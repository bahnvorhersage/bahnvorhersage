from datetime import UTC, datetime
from itertools import pairwise

import bahnvorhersage_routing as bvr

from api import fptf
from gtfs.routes import Routes
from gtfs.stops import StopSteffen
from router.constants import (
    WALKING_TRIP_ID,
)


def utc_ts_to_iso(ts: int) -> str:
    return datetime.fromtimestamp(ts, UTC).isoformat()


def connections_equal(c1: bvr.Connection, c2: bvr.Connection):
    return (
        c1.dp_ts == c2.dp_ts
        and c1.ar_ts == c2.ar_ts
        and c1.dp_stop_id == c2.dp_stop_id
        and c1.ar_stop_id == c2.ar_stop_id
        # and c1.trip_id == c2.trip_id
        and c1.is_regio == c2.is_regio
        and c1.dist_traveled == c2.dist_traveled
    )


def clean_alternatives(
    journey: list[bvr.Connection], alternatives: list[list[bvr.Connection]]
):
    for i, alternative in enumerate(alternatives):
        for start_index in range(len(alternative)):
            if alternative[start_index] not in journey:
                break
        alternatives[i] = alternative[start_index:]

    return alternatives


def remove_duplicate_journeys(journeys: list[list[bvr.Connection]]):
    journeys = sorted(journeys, key=lambda j: j[0].dp_ts)
    unique_journeys: list[list[bvr.Connection]] = []
    for journey in journeys:
        for unique_journey in reversed(unique_journeys):
            if journey[0].dp_ts > unique_journey[0].dp_ts:
                unique_journeys.append(journey)
                break
            elif len(journey) == len(unique_journey):
                if all(
                    [
                        connections_equal(c1, c2)
                        for c1, c2 in zip(journey, unique_journey)
                    ]
                ):
                    break
            unique_journeys.append(journey)
            break
        else:
            unique_journeys.append(journey)

    return unique_journeys


def fptf_from_journey(
    journey: list[bvr.Connection],
    routes: dict[int, Routes],
    stop_steffen: StopSteffen,
) -> fptf.Journey:
    legs: list[fptf.Leg] = []
    stopovers: list[fptf.Stopover] = []

    dp_ts = journey[0].dp_ts
    dp_stop_id = journey[0].dp_platform_id
    dist_traveled = 0

    for c1, c2 in pairwise(journey):
        dist_traveled += c1.dist_traveled
        if c1.trip_id == c2.trip_id:
            current_stop = stop_steffen.get_stop(stop_id=c1.ar_platform_id)
            stopovers.append(
                fptf.Stopover(
                    stop=fptf.Stop(
                        id=current_stop.stop_name, name=current_stop.stop_name
                    ),
                    arrival=utc_ts_to_iso(c1.ar_ts),
                    arrivalPlatform=current_stop.platform_code,
                    departure=utc_ts_to_iso(c2.dp_ts),
                    departurePlatform=current_stop.platform_code,
                    distance=dist_traveled,
                )
            )
        elif c1.trip_id == WALKING_TRIP_ID:
            ar_stop = stop_steffen.get_stop(stop_id=c1.ar_stop_id)
            dp_stop = stop_steffen.get_stop(stop_id=c1.dp_stop_id)
            legs.append(
                fptf.Leg(
                    origin=fptf.Stop(id=dp_stop.stop_name, name=dp_stop.stop_name),
                    destination=fptf.Stop(id=ar_stop.stop_name, name=ar_stop.stop_name),
                    departure=utc_ts_to_iso(c1.dp_ts),
                    departurePlatform=None,
                    arrival=utc_ts_to_iso(c1.ar_ts),
                    arrivalPlatform=None,
                    stopovers=[],
                    distance=c1.dist_traveled,
                    walking=True,
                    mode='walking',
                    line=fptf.Line.walking(),
                )
            )

            dp_ts = c2.dp_ts
            dp_stop_id = c2.dp_platform_id
            dist_traveled = 0
            stopovers: list[fptf.Stopover] = []
        else:
            line = fptf.Line.from_route(routes[c1.trip_id])
            dp_stop = stop_steffen.get_stop(stop_id=dp_stop_id)
            ar_stop = stop_steffen.get_stop(stop_id=c1.ar_platform_id)
            legs.append(
                fptf.Leg(
                    origin=fptf.Stop(id=dp_stop.stop_name, name=dp_stop.stop_name),
                    destination=fptf.Stop(id=ar_stop.stop_name, name=ar_stop.stop_name),
                    departure=utc_ts_to_iso(dp_ts),
                    departurePlatform=dp_stop.platform_code,
                    arrival=utc_ts_to_iso(c1.ar_ts),
                    arrivalPlatform=ar_stop.platform_code,
                    stopovers=stopovers,
                    distance=dist_traveled,
                    line=line,
                )
            )

            dp_ts = c2.dp_ts
            dp_stop_id = c2.dp_platform_id
            dist_traveled = 0
            stopovers: list[fptf.Stopover] = []

    if journey[-1].trip_id == WALKING_TRIP_ID:
        ar_stop = stop_steffen.get_stop(stop_id=journey[-1].ar_stop_id)
        dp_stop = stop_steffen.get_stop(stop_id=journey[-1].dp_stop_id)
        legs.append(
            fptf.Leg(
                origin=fptf.Stop(id=dp_stop.stop_name, name=dp_stop.stop_name),
                destination=fptf.Stop(id=ar_stop.stop_name, name=ar_stop.stop_name),
                departure=utc_ts_to_iso(journey[-1].dp_ts),
                departurePlatform=dp_stop.platform_code,
                arrival=utc_ts_to_iso(journey[-1].ar_ts),
                arrivalPlatform=ar_stop.platform_code,
                stopovers=[],
                distance=journey[-1].dist_traveled,
                walking=True,
                mode='walking',
                line=fptf.Line.walking(),
            )
        )
    else:
        line = fptf.Line.from_route(routes[journey[-1].trip_id])
        dp_stop = stop_steffen.get_stop(stop_id=dp_stop_id)
        ar_stop = stop_steffen.get_stop(
            stop_id=journey[-1].ar_platform_id,
        )
        legs.append(
            fptf.Leg(
                origin=fptf.Stop(id=dp_stop.stop_name, name=dp_stop.stop_name),
                destination=fptf.Stop(id=ar_stop.stop_name, name=ar_stop.stop_name),
                departure=utc_ts_to_iso(dp_ts),
                departurePlatform=dp_stop.platform_code,
                arrival=utc_ts_to_iso(journey[-1].ar_ts),
                arrivalPlatform=ar_stop.platform_code,
                stopovers=stopovers,
                distance=dist_traveled + journey[-1].dist_traveled,
                line=line,
            )
        )

    return fptf.Journey(legs=legs)
