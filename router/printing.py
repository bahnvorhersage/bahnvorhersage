from datetime import datetime
from itertools import pairwise

import bahnvorhersage_routing as bvr

from gtfs.routes import Routes
from gtfs.stops import StopSteffen
from router.constants import WALKING_TRIP_ID


def connection_to_str(
    connection: bvr.Connection,
    last_dp_ts: int,
    stop_steffen: StopSteffen,
    routes: dict[int, Routes],
) -> str:
    duration = (connection.ar_ts - last_dp_ts) // 60
    journey_str = f'{datetime.fromtimestamp(last_dp_ts).strftime("%H:%M")} -> '
    if connection.trip_id == WALKING_TRIP_ID:
        journey_str += f'🚶{duration:n}min -> '
    elif connection.is_regio:
        journey_str += f'🭃{duration:n}min {routes[connection.trip_id].route_short_name} {connection.trip_id:x}🭎 -> '
    else:
        journey_str += f'🭊🭂{duration:n}min {routes[connection.trip_id].route_short_name} {connection.trip_id:x}🭍🬿 -> '
    journey_str += f'{datetime.fromtimestamp(connection.ar_ts).strftime("%H:%M")} '
    journey_str += f'{stop_steffen.get_name(connection.ar_stop_id)} '
    return journey_str


def journey_to_str(
    journey: list[bvr.Connection], stop_steffen: StopSteffen, routes: dict[int, Routes]
):
    dp_ts = datetime.fromtimestamp(
        journey[0].dp_ts
    )  # TODO: probably not handling timezones correctly
    ar_ts = datetime.fromtimestamp(journey[-1].ar_ts)
    duration = journey[-1].ar_ts - journey[0].dp_ts
    dist_traveled = sum([c.dist_traveled for c in journey])
    is_regio = all([c.is_regio for c in journey])

    journey_str = f'{dp_ts.strftime("%H:%M")} - '
    journey_str += f'{ar_ts.strftime("%H:%M")} '
    journey_str += f'({duration//60:n}min, {dist_traveled//1000:n}km, r:{is_regio}): '
    journey_str += f'{stop_steffen.get_name(journey[0].dp_stop_id)} '

    last_dp_ts = journey[0].dp_ts

    for c1, c2 in pairwise(journey):
        if c1.trip_id != c2.trip_id:
            journey_str += connection_to_str(c1, last_dp_ts, stop_steffen, routes)
            last_dp_ts = c2.dp_ts

    journey_str += connection_to_str(journey[-1], last_dp_ts, stop_steffen, routes)

    return journey_str


def print_journeys(
    journeys: list[list[bvr.Connection]],
    stop_steffen: StopSteffen,
    routes: dict[int, Routes],
):
    for journey in journeys:
        print(journey_to_str(journey, stop_steffen, routes=routes))
