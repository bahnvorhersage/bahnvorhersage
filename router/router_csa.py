from dataclasses import dataclass
from datetime import datetime, timedelta
from itertools import pairwise

import bahnvorhersage_routing as bvr
import sqlalchemy
from sqlalchemy.orm import Session as SessionType

from api import fptf
from database.engine import sessionfactory
from gtfs.connections import Connections as DBConnections
from gtfs.routes import Routes
from gtfs.stops import StopSteffen
from gtfs.transfers import get_transfers
from gtfs.trips import Trips
from router.constants import (
    ADDITIONAL_SEARCH_WINDOW_HOURS,
    MAX_EXPECTED_DELAY_SECONDS,
    MAX_SEARCH_WINDOW_HOURS,
    NO_DELAYED_TRIP_ID,
    NO_TRIP_ID,
    STANDART_SEARCH_WINDOW_HOURS,
    WALKING_TRIP_ID,
)
from router.datatypes import Changeover
from router.exceptions import NoTimetableFound
from router.journey_reconstruction import (
    clean_alternatives,
    fptf_from_journey,
    remove_duplicate_journeys,
)
from router.printing import print_journeys

# TODO:
# - sort out splitting and merging trains


def _get_routes_from_db(
    trip_ids: list[int], session: SessionType
) -> list[tuple[Routes, int]]:
    stmt = (
        sqlalchemy.select(Routes, Trips.trip_id)
        .join(Trips, Routes.route_id == Trips.route_id)
        .where(Trips.trip_id.in_(trip_ids))
    )

    return session.execute(stmt).all()


def get_routes(trip_ids: list[int], session: SessionType) -> dict[int, Routes]:
    routes = _get_routes_from_db(trip_ids, session)

    routes = {trip_id: route for route, trip_id in routes}

    return routes


@dataclass
class RoutingParams:
    origin: str
    destination: str
    origin_stop_id: int
    destination_stop_id: int
    dp_ts: datetime
    session: SessionType
    n_hours_to_future: int
    heuristics: dict[int, int]


class RouterCSA:
    def __init__(self):
        self.stop_steffen = StopSteffen()
        self.transfers = get_transfers()
        self.params: RoutingParams = None
        self.csa_runner = None

    def run_csa(
        self,
        stop_reachabilities: dict[int, bvr.Reachability],
        profile: bvr.RoutingProfile,
        start_ts: int,
        delayed_trip_id: int = NO_DELAYED_TRIP_ID,
        min_delay: int = 0,
    ):
        self.csa_runner.configure(
            stop_reachabilities=stop_reachabilities,
            profile=profile,
            delayed_trip_id=delayed_trip_id,
            min_delay=min_delay,
        )

        self.csa_runner.run(start_ts)

        while (
            not self.csa_runner.finished
            and self.params.n_hours_to_future < MAX_SEARCH_WINDOW_HOURS
        ):
            new_connections = DBConnections.get_routing_connections(
                session=self.params.session,
                from_ts=self.params.dp_ts
                + timedelta(hours=self.params.n_hours_to_future),
                to_ts=self.params.dp_ts
                + timedelta(
                    hours=self.params.n_hours_to_future + ADDITIONAL_SEARCH_WINDOW_HOURS
                ),
            )
            if len(new_connections) == 0:
                # TODO: Avoid failing, try to return partial results
                raise NoTimetableFound(
                    'No additional timetable found for given date and time'
                )
            self.params.n_hours_to_future += ADDITIONAL_SEARCH_WINDOW_HOURS

            start_ts = new_connections.get_min_dp_ts()
            self.csa_runner.extend_connections(new_connections)

            self.csa_runner.run(start_ts)

        return self.csa_runner.extract_journeys()

    def do_routing(
        self,
        origin: str,
        destination: str,
        dp_ts: datetime,
        session: SessionType,
    ) -> list[fptf.JourneyAndAlternatives]:
        destination_stop_id = self.stop_steffen.names_to_ids[destination][0]

        heuristics = bvr.Heuristics()
        for stop in self.stop_steffen.stations():
            heuristics.insert(
                stop.stop_id,
                int(self.stop_steffen.get_distance(stop.stop_id, destination_stop_id)),
            )

        self.params = RoutingParams(
            origin=origin,
            destination=destination,
            origin_stop_id=self.stop_steffen.names_to_ids[origin][0],
            destination_stop_id=destination_stop_id,
            dp_ts=dp_ts,
            session=session,
            n_hours_to_future=STANDART_SEARCH_WINDOW_HOURS,
            heuristics=heuristics,
        )

        connections = DBConnections.get_routing_connections(
            session=session,
            from_ts=dp_ts,
            to_ts=dp_ts + timedelta(hours=STANDART_SEARCH_WINDOW_HOURS),
        )

        if len(connections) == 0:
            raise NoTimetableFound('No timetable found for given date and time')

        self.csa_runner = bvr.Csa(
            transfers=self.transfers,
            heuristics=self.params.heuristics,
            destination_stop_id=self.params.destination_stop_id,
        )
        self.csa_runner.extend_connections(connections)

        origin_reachability = bvr.Reachability(
            dp_ts=int(dp_ts.timestamp()),
            ar_ts=int(dp_ts.timestamp()),
            changeovers=0,
            dist_traveled=0,
            is_regio=True,
            transfer_time_from_delayed_trip=0,
            from_failed_transfer_stop_id=False,
            current_trip_id=NO_TRIP_ID,
            min_heuristic=self.params.heuristics.get(self.params.origin_stop_id),
            last_id=0,
            last_dp_ts=int(dp_ts.timestamp()),
            last_changeover_duration=0,
        )

        journeys = self.run_csa(
            stop_reachabilities={self.params.origin_stop_id: origin_reachability},
            start_ts=int(dp_ts.timestamp()),
            profile=bvr.RoutingProfile.Normal,
        )

        journeys = remove_duplicate_journeys(journeys)

        alternatives = [
            self.find_alternative_connections(journey=journey) for journey in journeys
        ]

        alternatives = [
            clean_alternatives(journey=journey, alternatives=alternatives_for_journey)
            for journey, alternatives_for_journey in zip(journeys, alternatives)
        ]

        alternatives = [
            remove_duplicate_journeys(alternatives_for_journey)
            for alternatives_for_journey in alternatives
        ]

        journeys_and_alternatives = self.to_fptf(journeys, alternatives)

        self.params = None
        return journeys_and_alternatives

    def find_alternative_connections(
        self,
        journey: list[bvr.Connection],
    ):
        changeovers: list[Changeover] = []

        is_regio = all(c.is_regio for c in journey)
        last_transfer_station = journey[0].dp_stop_id
        n_changeovers = 0
        dist_traveled = 0

        start_index = 1 if journey[0].trip_id == WALKING_TRIP_ID else 0
        end_index = (
            len(journey) - 1 if journey[-1].trip_id == WALKING_TRIP_ID else len(journey)
        )
        for c1, c2 in pairwise(journey[start_index:end_index]):
            dist_traveled += c1.dist_traveled
            if c1.trip_id != c2.trip_id:
                is_regio = min(is_regio, c1.is_regio)
                changeovers.append(
                    Changeover(
                        ar_ts=c1.ar_ts,
                        dp_ts=c2.dp_ts,
                        stop_id=c2.dp_stop_id,
                        is_regio=is_regio,
                        ar_trip_id=c1.trip_id,
                        previous_transfer_stop_id=last_transfer_station,
                        changeovers=n_changeovers,
                        dist_traveled=dist_traveled,
                    )
                )
                last_transfer_station = c2.dp_stop_id
                n_changeovers += 1

        alternatives = []

        for i, transfer in enumerate(changeovers):
            transfer_time_missed = transfer.dp_ts - transfer.ar_ts
            if transfer_time_missed > MAX_EXPECTED_DELAY_SECONDS:
                continue

            ar_ts = changeovers[i - 1].ar_ts if i > 0 else journey[0].dp_ts - 1

            stop_before_transfer = bvr.Reachability(
                dp_ts=ar_ts,
                ar_ts=ar_ts,
                changeovers=changeovers[i - 1].changeovers if i > 0 else 0,
                dist_traveled=changeovers[i - 1].dist_traveled if i > 0 else 0,
                is_regio=transfer.is_regio,
                current_trip_id=(
                    changeovers[i - 1].ar_trip_id if i > 0 else NO_TRIP_ID
                ),
                min_heuristic=self.params.heuristics.get(
                    transfer.previous_transfer_stop_id
                ),
                last_id=0,
                last_dp_ts=0,
                last_changeover_duration=0,
                transfer_time_from_delayed_trip=0,
                from_failed_transfer_stop_id=False,
            )

            stop_at_transfer = bvr.Reachability(
                dp_ts=transfer.ar_ts,
                ar_ts=transfer.ar_ts,
                changeovers=transfer.changeovers,
                dist_traveled=transfer.dist_traveled,
                is_regio=transfer.is_regio,
                current_trip_id=transfer.ar_trip_id,
                min_heuristic=self.params.heuristics.get(transfer.stop_id),
                last_id=0,
                last_dp_ts=0,
                last_changeover_duration=0,
                transfer_time_from_delayed_trip=0,
                from_failed_transfer_stop_id=True,
            )

            stop_reachabilities = {
                changeovers[i - 1].stop_id
                if i > 0
                else journey[0].dp_stop_id: stop_before_transfer,
                transfer.stop_id: stop_at_transfer,
            }

            journeys = self.run_csa(
                stop_reachabilities=stop_reachabilities,
                profile=bvr.RoutingProfile.Alternatives,
                start_ts=ar_ts,
                delayed_trip_id=transfer.ar_trip_id,
                min_delay=transfer_time_missed,
            )

            alternatives.extend(journeys)

        return alternatives

    def to_fptf(
        self,
        journeys: list[list[bvr.Connection]],
        alternatives: list[list[list[bvr.Connection]]],
    ) -> list[fptf.JourneyAndAlternatives]:
        trip_ids = set()
        for journey in journeys:
            for connection in journey:
                trip_ids.add(connection.trip_id)
        for alternatives_for_journey in alternatives:
            for alternative in alternatives_for_journey:
                for connection in alternative:
                    trip_ids.add(connection.trip_id)

        routes = get_routes(trip_ids, self.params.session)

        for journey, alternatives_for_journey in zip(journeys, alternatives):
            print('Journey:')
            print_journeys([journey], self.stop_steffen, routes=routes)

            print('Alternatives:')
            print_journeys(alternatives_for_journey, self.stop_steffen, routes=routes)

        journey_and_alternatives: list[fptf.JourneyAndAlternatives] = []

        for journey, alternatives_for_journey in zip(journeys, alternatives):
            journey_and_alternatives.append(
                fptf.JourneyAndAlternatives(
                    journey=fptf_from_journey(
                        journey, routes=routes, stop_steffen=self.stop_steffen
                    ),
                    alternatives=[
                        fptf_from_journey(
                            alternative, routes=routes, stop_steffen=self.stop_steffen
                        )
                        for alternative in alternatives_for_journey
                    ],
                )
            )

        return journey_and_alternatives


def main():
    engine, Session = sessionfactory()

    with Session() as session:
        router = RouterCSA()
        router.do_routing(
            origin='Tübingen Hbf',
            destination='Köln Hbf',
            dp_ts=datetime(2024, 2, 27, 13, 0, 0),
            session=session,
        )


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
