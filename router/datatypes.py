from collections import namedtuple

Changeover = namedtuple(
    'Changeover',
    [
        'ar_ts',
        'dp_ts',
        'stop_id',
        'is_regio',
        'ar_trip_id',
        'previous_transfer_stop_id',
        'changeovers',
        'dist_traveled',
    ],
)
