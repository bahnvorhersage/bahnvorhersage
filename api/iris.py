import enum
import urllib.parse
from collections.abc import Iterator
from dataclasses import dataclass, field
from datetime import date, datetime
from typing import Literal

import lxml.etree as etree
import pandas as pd
import pytz
import requests

from helpers.hash64 import xxhash64
from helpers.retry import retry
from helpers.StationPhillip import StationV3
from helpers.xml_parser import xml_to_json

PLAN_URL = 'https://iris.noncd.db.de/iris-tts/timetable/plan/'
CHANGES_URL = 'https://iris.noncd.db.de/iris-tts/timetable/fchg/'
RECENT_CHANGES_URL = 'https://iris.noncd.db.de/iris-tts/timetable/rchg/'

IRIS_TIMEOUT = 90


def db_to_utc(dt: str | None) -> datetime | None:
    """
    Convert bahn time in format: '%y%m%d%H%M' to datetime.
    As it is fastest to directly construct a datetime object from this, no strptime is
    used.

    Args:
        dt (str): bahn time format

    Returns:
        datetime.datetime: converted bahn time
    """
    if dt is None:
        return None
    local_datetime = datetime(
        year=int('20' + dt[0:2]),
        month=int(dt[2:4]),
        day=int(dt[4:6]),
        hour=int(dt[6:8]),
        minute=int(dt[8:10]),
        tzinfo=pytz.timezone('Europe/Berlin'),
    )
    utc_datetime = local_datetime.astimezone(pytz.utc)
    return utc_datetime


def filter_unknown_betriebstelle(station_names):
    return filter(
        lambda name: 'Betriebsstelle nicht bekannt' not in name, station_names
    )


def parse_path(path: str | None) -> list[str] | None:
    if path is None or not path:
        return None
    path = path.split('|')
    path = list(filter_unknown_betriebstelle(path))
    return path if len(path) > 0 else None


def parse_id(id: str) -> tuple[int, datetime, int]:
    """
    Parse a stop_id into its components

    Parameters
    ----------
    id : str
        A stop_id

    Returns
    -------
    tuple[int, datetime, int]
        trip_id, date_id, stop_id
    """
    trip_id, date_id, stop_id = id.rsplit('-', 2)
    return int(trip_id), db_to_utc(date_id), int(stop_id)


def parse_short_id(id: str) -> tuple[int, datetime]:
    """
    Parse a short_id into its components

    Parameters
    ----------
    id : str
        A short_id

    Returns
    -------
    tuple[int, datetime]
        trip_id, date_id
    """
    trip_id, date_id = id.rsplit('-', 1)
    return int(trip_id), db_to_utc(date_id)


class EventStatus(enum.StrEnum):
    PLANNED = 'p'
    ADDED = 'a'
    CANCELLED = 'c'


class MessagePriority(enum.IntEnum):
    HIGH = 1
    MEDIUM = 2
    LOW = 3
    DONE = 4


class MessageType(enum.StrEnum):
    HAFAS_INFORMATION_MANAGER = 'h'
    QUALITY_CHANGE = 'q'
    FREE = 'f'
    CAUSE_OF_DELAY = 'd'
    IBIS = 'i'
    UNASSIGNED_IBIS_MESSAGE = 'u'
    DISRUPTION = 'r'
    CONNECTION = 'c'


class DistributorType(enum.StrEnum):
    CITY = 's'
    REGION = 'r'
    LONG_DISTANCE = 'f'
    OTHER = 'x'


class Filter(enum.StrEnum):
    NON_DB = 'D'
    LONG_DISTANCE = 'F'
    REGIONAL = 'N'
    SBAHN = 'S'


class ConnectionStatus(enum.StrEnum):
    WAITING = 'w'
    TRANSITION = 'n'
    ALTERNATIVE = 'a'


class DelaySource(enum.StrEnum):
    LEIBIT = 'L'
    AUTOMATIC_IRIS_NE = 'NA'
    MANUAL_IRIS_NE = 'NM'
    VDV = 'V'
    AUTOMATIC_ISTP = 'IA'
    MANUAL_ISTP = 'IM'
    AUTOMATIC_PROGNOSIS = 'A'


class ReferenceTripRelationToStop(enum.StrEnum):
    BEFORE = 'b'
    END = 'e'
    BETWEEN = 'c'
    START = 's'
    AFTER = 'a'


@dataclass
class TripLabel:
    """Compound data type that contains common data items that characterize a Trip"""

    category: str
    filter: Filter
    number: str
    owner: str
    type: Literal['p', 'e', 'z', 's', 'h', 'n']
    # I did not find any documentation on what these letters mean

    def __init__(self, trip_label: dict) -> None:
        self.category = trip_label['c']
        self.filter = Filter(trip_label['f']) if 'f' in trip_label else None
        self.number = trip_label['n']
        self.owner = trip_label['o']
        self.type = trip_label['t']


@dataclass
class DistributorMessage:
    """
    An additional message to a given station-based disruption by a specific distributor
    """

    internal_text: str
    distributor_name: str
    distributor_type: DistributorType
    timestamp: datetime

    def __init__(self, distributor_message: dict) -> None:
        self.internal_text = distributor_message['int']
        self.distributor_name = distributor_message['n']
        self.distributor_type = DistributorType(distributor_message['t'])
        self.timestamp = db_to_utc(distributor_message['ts'])


@dataclass
class Message:
    """A message that is associated with an event, a stop or a trip."""

    code: int | None
    category: str | None
    deleted: bool | None
    distributor_message: DistributorMessage | None
    external_category: str | None
    external_link: str | None
    external_text: str | None
    valid_from: datetime | None
    message_id: str | None
    internal_text: str | None
    owner: str | None
    priority: MessagePriority | None
    message_type: MessageType | None
    trip_label: TripLabel | None
    valid_to: datetime | None
    timestamp: datetime | None

    def __init__(self, message: dict) -> None:
        self.code = message['c'] if 'c' in message else None
        self.category = message['cat'] if 'cat' in message else None
        self.deleted = bool(message['del'] == 'true') if 'del' in message else None
        self.distributor_message = (
            DistributorMessage(message['dm']) if 'dm' in message else None
        )
        self.external_category = message['ec'] if 'ec' in message else None
        self.external_link = message['elnk'] if 'elnk' in message else None
        self.external_text = message['ext'] if 'ext' in message else None
        self.valid_from = db_to_utc(message['from']) if 'from' in message else None
        self.message_id = message['id'] if 'id' in message else None
        self.internal_text = message['int'] if 'int' in message else None
        self.owner = message['o'] if 'o' in message else None
        self.priority = MessagePriority(int(message['pr'])) if 'pr' in message else None
        self.message_type = MessageType(message['t']) if 't' in message else None
        self.trip_label = TripLabel(message['tl']) if 'tl' in message else None
        self.valid_to = db_to_utc(message['to']) if 'to' in message else None
        self.timestamp = db_to_utc(message['ts']) if 'ts' in message else None


@dataclass
class Event:
    """An event (arrival or departure) that is part of a stop."""

    changed_distant_endpoint: str | None
    cancellation_time: datetime | None
    changed_platform: str | None
    changed_path: list[str] | None
    changed_status: EventStatus | None
    changed_time: datetime | None
    distant_change: int | None
    hidden: bool | None
    line: str | None
    messages: list[Message] | None
    planned_distant_endpoint: str | None
    planned_platform: str | None
    planned_path: list[str] | None
    planned_status: EventStatus | None
    planned_time: datetime | None
    transition: str | None
    wings: list[str] | None

    def __init__(self, event: dict) -> None:
        self.changed_distant_endpoint = event['cde'] if 'cde' in event else None
        self.cancellation_time = db_to_utc(event['clt']) if 'clt' in event else None
        self.changed_platform = event['cp'] if 'cp' in event else None
        self.changed_path = parse_path(event['cpth']) if 'cpth' in event else None
        self.changed_status = EventStatus(event['cs']) if 'cs' in event else None
        self.changed_time = db_to_utc(event['ct']) if 'ct' in event else None
        self.distant_change = int(event['dc']) if 'dc' in event else None
        self.hidden = bool(int(event['hi'])) if 'hi' in event else None
        self.line = event['l'] if 'l' in event else None

        if 'm' in event:
            self.messages = [Message(message) for message in event['m']]
        else:
            self.messages = None
        self.planned_distant_endpoint = event['pde'] if 'pde' in event else None
        self.planned_platform = event['pp'] if 'pp' in event else None
        self.planned_path = parse_path(event['ppth']) if 'ppth' in event else None
        self.planned_status = EventStatus(event['ps']) if 'ps' in event else None
        self.planned_time = db_to_utc(event['pt']) if 'pt' in event else None
        self.transition = event['tra'] if 'tra' in event else None
        self.wings = event['wings'].split('|') if 'wings' in event else None


@dataclass
class Connection:
    """
    It's information about a connected train at a
    particular stop.
    """

    connection_status: ConnectionStatus
    eva: int
    connection_id: str
    ref: ...  # not in documentation
    s: ...  # not in documentation
    timestamp: datetime


@dataclass
class HistoricDelay:
    """
    It's the history of all delay-messages for a stop.
    This element extends HistoricChange.
    """

    arrival: datetime
    delay_cause: str
    departure: datetime
    delay_source: DelaySource
    timestamp: datetime


@dataclass
class HistoricPlatformChange:
    """
    It's the history of all platform-changes for a stop.
    This element extends HistoricChange.
    """

    arrival: datetime
    cause_of_track_change: str
    departure: datetime
    timestamp: datetime


@dataclass
class ReferenceTripLabel:
    """
    It's a compound data type that contains common data
    items that characterize a reference trip.
    """

    category: str
    number: str

    def __init__(self, reference_trip_label: dict) -> None:
        self.category = reference_trip_label['c']
        self.number = reference_trip_label['n']


@dataclass
class TripReference:
    """
    It's a reference to another trip, which holds its
    label and reference trips, if available.
    """

    refered_trips: list[TripLabel]
    trip_label: TripLabel

    def __init__(self, trip_reference: dict) -> None:
        if 'rt' in trip_reference:
            self.refered_trips = [
                ReferenceTrip(trip_label) for trip_label in trip_reference['rt']
            ]
        else:
            self.refered_trips = None
        self.trip_label = TripLabel(trip_reference['tl'][0])


@dataclass
class ReferenceTripStopLabel:
    """
    It's a compound data type that contains common data
    items that characterize a reference trip stop. The
    contents is represented as a compact 4-tuple in XML.
    """

    eva: int
    index: int
    name: str
    planned_time: datetime

    def __init__(self, reference_trip_stop_label: dict) -> None:
        self.eva = int(reference_trip_stop_label['eva'])
        self.index = int(reference_trip_stop_label['i'])
        self.name = reference_trip_stop_label['n']
        self.planned_time = db_to_utc(reference_trip_stop_label['pt'])


@dataclass
class ReferenceTrip:
    """
    A reference trip is another real trip, but it
    doesn't have its own stops and events. It refers
    only to its referenced regular trip. The reference
    trip collects mainly all different attributes of the
    referenced regular trip.
    """

    cancelled: bool
    ea: ReferenceTripStopLabel  # ? what ea means
    trip_id: int
    date_id: datetime
    reference_trip_label: ReferenceTripLabel
    sd: ReferenceTripStopLabel  # ? what sd means

    def __init__(self, reference_trip: dict) -> None:
        self.cancelled = bool(reference_trip['c'] == 'true')
        self.ea = ReferenceTripStopLabel(reference_trip['ea'][0])
        self.trip_id, self.date_id = parse_short_id(reference_trip['id'])
        self.reference_trip_label = ReferenceTripLabel(reference_trip['rtl'][0])
        self.sd = ReferenceTripStopLabel(reference_trip['sd'][0])


@dataclass
class ReferenceTripRelation:
    """
    A reference trip relation holds how a reference trip is
    related to a stop, for instance the reference trip starts
    after the stop. Stop contains a collection of that type,
    only if reference trips are available.
    """

    reference_trip: ReferenceTrip
    reference_trip_relation_to_stop: ReferenceTripRelationToStop | None

    def __init__(self, reference_trip_relation: dict) -> None:
        self.reference_trip = ReferenceTrip(reference_trip_relation['rt'][0])
        self.reference_trip_relation_to_stop = (
            ReferenceTripRelationToStop(reference_trip_relation['rts'][0])
            if reference_trip_relation['rts'][0]
            else None
        )


@dataclass
class TimetableStop:
    """A stop is a part of a Timetable."""

    arrival: Event | None
    departure: Event | None
    connection: Connection
    historic_delay: HistoricDelay
    historic_platform_change: HistoricPlatformChange
    raw_id: str
    hash_id: int
    trip_id: int
    date_id: datetime
    stop_sequence_id: int
    message: list[Message] | None
    reference: TripReference | None
    rtr: ReferenceTripRelation | None
    trip_label: TripLabel | None
    eva: int | None

    def __init__(self, stop: dict) -> None:
        if 'conn' in stop or 'hd' in stop or 'hpc' in stop:
            raise NotImplementedError(
                f'Found weird stop. Please report this to the developers: {stop}'
            )

        self.arrival = Event(stop['ar'][0]) if 'ar' in stop else None
        self.departure = Event(stop['dp'][0]) if 'dp' in stop else None
        self.connection = Connection(stop['conn']) if 'conn' in stop else None
        self.historic_delay = HistoricDelay(stop['hd']) if 'hd' in stop else None
        self.historic_platform_change = (
            HistoricPlatformChange(stop['hpc']) if 'hpc' in stop else None
        )
        self.raw_id = stop['id']
        self.trip_id, self.date_id, self.stop_sequence_id = parse_id(self.raw_id)
        self.hash_id = xxhash64(self.raw_id)
        if 'm' in stop:
            self.message = [Message(message) for message in stop['m']]
        else:
            self.message = None
        self.reference = TripReference(stop['ref'][0]) if 'ref' in stop else None
        self.rtr = ReferenceTripRelation(stop['rtr'][0]) if 'rtr' in stop else None
        self.trip_label = TripLabel(stop['tl'][0]) if 'tl' in stop else None
        self.eva = int(stop['eva']) if 'eva' in stop else None

    def is_bus(self) -> bool:
        return (
            self.trip_label.category == 'Bus' or self.arrival.line == 'SEV'
            if self.arrival is not None
            else False or self.departure.line == 'SEV'
            if self.departure is not None
            else False
        )

    @property
    def is_betriebshalt(self) -> bool:
        # At a Betriebshalt, the train stops but no passengers are allowed to enter or
        # exit according to some unqualified lookup in the data, Betriebshalte have both
        # arrival and departure hidden and no planned platform

        arrival_hidden = bool(self.arrival.hidden) if self.arrival is not None else True
        departure_hidden = (
            bool(self.departure.hidden) if self.departure is not None else True
        )
        arrival_has_platform = (
            bool(self.arrival.planned_platform) if self.arrival is not None else False
        )
        departure_has_platform = (
            bool(self.departure.planned_platform)
            if self.departure is not None
            else False
        )

        return (
            arrival_hidden
            and departure_hidden
            and not arrival_has_platform
            and not departure_has_platform
        )


@dataclass
class IrisStation:
    name: str
    eva: int
    ds100: str
    db: bool
    valid_from: datetime
    valid_to: datetime
    creationts: datetime
    meta: list[int] = field(default_factory=list)

    def __init__(self, iris_station: dict) -> None:
        self.name = iris_station['name']
        self.eva = int(iris_station['eva'])
        self.ds100 = iris_station['ds100']
        self.db = iris_station['db'] == 'true'
        self.creationts = datetime.strptime(
            iris_station['creationts'], '%d-%m-%y %H:%M:%S.%f'
        )
        self.valid_from = datetime.strptime(
            iris_station['creationts'], '%d-%m-%y %H:%M:%S.%f'
        )
        self.valid_to = pd.Timestamp.max.to_pydatetime(warn=False)

        self.meta = parse_meta(iris_station.get('meta', ''))


def xml_str_to_json(xml_response: str) -> list[dict]:
    xml_response = etree.fromstring(xml_response.encode())
    return list(xml_to_json(single) for single in xml_response)


def stations_equal(iris_station: IrisStation, station: pd.Series) -> bool:
    """
    Check weather two stations are equal (same name, same eva, same ds100)
    Args:
        iris_station: A station from the IRIS API
        station: A station from StationPhillip's database

    Returns: True if the stations are equal, False otherwise
    """
    return (
        iris_station.name == station['name']
        and iris_station.eva == station['eva']
        and iris_station.ds100 == station['ds100']
    )


def parse_meta(meta: str) -> list[int]:
    """Parse meta string from IRIS

    Parameters
    ----------
    meta : str
        Metas separated by | (e.g. 80001|80002|80004)

    Returns
    -------
    List[int]
        List of metas. Empty list if meta is empty
    """
    if not meta:
        return []
    meta = meta.split('|')
    return list(map(int, meta))


def get_iris_station(search_term: str | int) -> IrisStation | None:
    """
    Search IRIS for a station either by name or eva number

    Parameters
    ----------
    search_term : Union[str, int]
        Name or eva number of the station

    Returns
    -------
    IrisStation | None
        found station or None if no station was found

    Raises
    ------
    ValueError
        Invalid response from IRIS
    ValueError
        More than 1 match found for search_term
    """
    if isinstance(search_term, int):
        search_term = str(search_term)
    search_term = urllib.parse.quote(search_term)

    matches = requests.get(
        f'http://iris.noncd.db.de/iris-tts/timetable/station/{search_term}'
    )
    if matches.ok:
        matches = matches.text
    else:
        raise ValueError(
            'Did not get a valid response. Http Code: ' + str(matches.status_code)
        )

    matches = etree.fromstring(matches.encode())
    matches = list(xml_to_json(match) for match in matches)

    if len(matches) == 0:
        return None
    elif len(matches) != 1:
        raise ValueError(
            f'More than 1 match found for {search_term} which is unexpected'
        )
    match = matches[0]
    return IrisStation(match)


def get_station(search_term: str | int) -> StationV3 | None:
    station = get_iris_station(search_term)
    if station is None:
        return None
    else:
        return StationV3(
            eva=int(station.eva),
            name=station.name,
            ds100=station.ds100,
            lat=None,
            lon=None,
            is_active_iris=True,
            meta_evas=station.meta,
        )


def search_iris_multiple(
    search_terms: set[str | int],
) -> Iterator[IrisStation | None]:
    """Search IRIS for multiple stations

    Parameters
    ----------
    search_terms : Set[Union[str, int]]
        Iterable of search terms (name or eva number)

    Yields
    ------
    Iterator[Union[IrisStation, None]]
        An Iterator over the results of the search. If a station
        was not found, None is yielded instead of an IrisStation
    """
    for search_term in search_terms:
        yield get_iris_station(search_term)


@retry(max_retries=3)
def _make_iris_request(url: str, session: requests.Session = None) -> list[dict]:
    if session is None:
        r = requests.get(url, timeout=IRIS_TIMEOUT)
    else:
        r = session.get(url, timeout=IRIS_TIMEOUT)
    r.raise_for_status()
    return xml_str_to_json(r.text)


def get_plan(
    eva: int, date: date, hour: int, session: requests.Session = None
) -> list[dict]:
    return _make_iris_request(
        PLAN_URL + f"{eva}/{date.strftime('%y%m%d')}/{hour:02d}", session=session
    )


def get_all_changes(eva: int, session: requests.Session = None) -> list[dict]:
    return _make_iris_request(CHANGES_URL + f'{eva}', session=session)


def get_recent_changes(eva: int, session: requests.Session = None) -> list[dict]:
    return _make_iris_request(RECENT_CHANGES_URL + f'{eva}', session=session)


if __name__ == '__main__':
    # get_stations_from_iris(8000001)
    get_iris_station('Tübingen')
