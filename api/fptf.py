from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Literal

from api.ris import TransferInfo
from gtfs.routes import Routes, RouteType
from public_config import LONG_DISTANCE_CATEGORIES


@dataclass
class Location:
    id: str
    latitude: float
    longitude: float
    type = 'location'

    @staticmethod
    def from_dict(location: dict):
        return Location(
            id=location['id'],
            latitude=location['latitude'],
            longitude=location['longitude'],
        )


@dataclass
class Products:
    nationalExpress: bool
    national: bool
    regionalExpress: bool
    regional: bool
    suburban: bool
    bus: bool
    ferry: bool
    subway: bool
    tram: bool
    taxi: bool

    @staticmethod
    def from_dict(products):
        return Products(**products)


@dataclass
class Stop:
    id: str
    name: str
    location: Location | None = None
    products: Products | None = None
    type: str = 'stop'

    @staticmethod
    def from_dict(stop: dict, parse_products: bool = True) -> 'Stop':
        return Stop(
            id=stop['id'],
            name=stop['name'],
            location=Location.from_dict(stop['location']),
            products=Products.from_dict(stop['products']) if parse_products else None,
            type=stop['type'] if 'type' in stop else 'stop',
        )


@dataclass
class Line:
    id: int
    name: str
    operator: str
    isRegio: bool
    productName: str
    mode: str
    fahrtNr: str | None = None
    adminCode: str | None = None
    type: str = 'line'

    @staticmethod
    def from_dict(line: dict) -> 'Line':
        return Line(
            id=line['id'],
            name=line['name'],
            operator=line.get('operator'),
            isRegio=line['productName'] not in LONG_DISTANCE_CATEGORIES,
            productName=line['productName'],
            mode=line['mode'],
            fahrtNr=line.get('fahrtNr'),
            adminCode=line.get('adminCode'),
        )

    @staticmethod
    def from_route(route: Routes) -> 'Line':
        return Line(
            id=route.route_id,
            name=route.route_short_name,
            operator=route.agency_id,
            isRegio=route.is_regional(),
            productName=route.route_short_name.split(' ')[0],
            mode='bus' if route.route_type == RouteType.BUS else 'train',
        )

    @staticmethod
    def walking() -> 'Line':
        return Line(
            id=0,
            name='walking',
            operator='walking',
            isRegio=True,
            productName='walking',
            mode='walking',
        )


@dataclass
class DelayPrediction:
    predictions: list[float]
    offset: int
    type: str = 'delayPrediction'

    def __init__(
        self, predictions: list[float], offset: int, decimals: int | None = None
    ):
        if decimals is not None:
            self.predictions = [
                round(prediction, decimals) for prediction in predictions
            ]
        else:
            self.predictions = predictions
        self.offset = offset


@dataclass
class Stopover:
    stop: Stop
    arrival: datetime | None
    plannedArrival: datetime | None
    arrivalPlatform: str
    departure: datetime | None
    plannedDeparture: datetime | None
    departurePlatform: str
    cancelled: bool
    type: str = 'stopover'

    @staticmethod
    def from_dict(stopover: dict) -> 'Stopover':
        return Stopover(
            stop=Stop.from_dict(stopover['stop'], parse_products=False),
            arrival=datetime.fromisoformat(stopover['arrival'])
            if stopover.get('arrival')
            else None,
            plannedArrival=datetime.fromisoformat(stopover['plannedArrival'])
            if stopover.get('plannedArrival')
            else None,
            arrivalPlatform=stopover['arrivalPlatform'],
            departure=datetime.fromisoformat(stopover['departure'])
            if stopover.get('departure')
            else None,
            plannedDeparture=datetime.fromisoformat(stopover['plannedDeparture'])
            if stopover.get('plannedDeparture')
            else None,
            departurePlatform=stopover['departurePlatform'],
            cancelled=stopover.get('cancelled', False),
        )

    @staticmethod
    def trip_from_list(stopovers: list[dict]) -> list['Stopover']:
        return [Stopover.from_dict(stopover) for stopover in stopovers]

    def dwell_time_minutes(self) -> int | None:
        if self.arrival is not None and self.departure is not None:
            return (self.departure - self.arrival).seconds // 60
        else:
            return None

    def planned_dwell_time_minutes(self) -> int | None:
        if self.plannedArrival is not None and self.plannedDeparture is not None:
            return (self.plannedDeparture - self.plannedArrival).seconds // 60
        else:
            return None


@dataclass
class Transfer:
    origin: Stop
    destination: Stop
    duration: timedelta
    distanceMeters: int | None = None
    source: Literal['RIL420', 'INDOOR_ROUTING', 'EFZ', 'FALLBACK', 'HAFAS'] = 'HAFAS'
    identicalPhysicalPlatform: bool | None = None
    mode: Literal['walk', 'bike'] | None = None
    transferScore: float | None = None
    type: str = 'transfer'

    @staticmethod
    def from_leg_dict(leg: dict) -> 'Transfer':
        duration = timedelta(0)
        if leg['departure'] is not None and leg['arrival'] is not None:
            duration = datetime.fromisoformat(
                leg['departure']
            ) - datetime.fromisoformat(leg['arrival'])
        return Transfer(
            origin=Stop.from_dict(leg['origin'], parse_products=False),
            destination=Stop.from_dict(leg['destination'], parse_products=False),
            duration=duration,
            distanceMeters=leg['distance'] if 'distance' in leg else None,
            mode='walk',
        )

    def add_transfer_info(self, transfer_info: TransferInfo):
        self.duration = transfer_info.occasional_traveller.duration
        self.distanceMeters = (
            transfer_info.occasional_traveller.distance
            if transfer_info.occasional_traveller.distance is not None
            else self.distanceMeters
        )
        self.source = transfer_info.source
        self.identicalPhysicalPlatform = transfer_info.identical_physical_platform


@dataclass
class Leg:
    origin: Stop
    destination: Stop
    departure: datetime | None
    departurePlatform: str
    arrival: datetime | None
    arrivalPlatform: str
    cancelled: str
    direction: str | None = None
    stopovers: list[Stopover] | None = None
    plannedDeparture: datetime | None = None
    plannedDeparturePlatform: str | None = None
    departureDelayPrediction: DelayPrediction | None = None
    plannedArrival: datetime | None = None
    plannedArrivalPlatform: str | None = None
    arrivalDelayPrediction: DelayPrediction | None = None
    line: Line | None = None
    tripId: str | None = None
    type: str = 'leg'

    @staticmethod
    def from_dict(leg: dict) -> 'Leg':
        return Leg(
            origin=Stop.from_dict(leg['origin'], parse_products=False),
            destination=Stop.from_dict(leg['destination'], parse_products=False),
            departure=datetime.fromisoformat(leg['departure'])
            if leg['departure'] is not None
            else None,
            plannedDeparture=datetime.fromisoformat(leg['plannedDeparture'])
            if 'plannedDeparture' in leg
            else None,
            departurePlatform=leg['departurePlatform'],
            plannedDeparturePlatform=leg.get('plannedDeparturePlatform'),
            arrival=datetime.fromisoformat(leg['arrival'])
            if leg['arrival'] is not None
            else None,
            plannedArrival=datetime.fromisoformat(leg['plannedArrival'])
            if 'plannedArrival' in leg
            else None,
            arrivalPlatform=leg['arrivalPlatform'],
            plannedArrivalPlatform=leg.get('plannedArrivalPlatform'),
            cancelled=leg.get('cancelled', False),
            direction=leg.get('direction'),
            stopovers=[Stopover.from_dict(stopover) for stopover in leg['stopovers']]
            if 'stopovers' in leg
            else None,
            line=Line.from_dict(leg['line']) if 'line' in leg else None,
            tripId=leg.get('tripId'),
        )

    @property
    def arrival_delay_minutes(self) -> int | None:
        if self.plannedArrival is not None and self.arrival is not None:
            return (self.arrival - self.plannedArrival).seconds // 60
        else:
            return None

    @property
    def departure_delay_minutes(self) -> timedelta | None:
        if self.plannedDeparture is not None and self.departure is not None:
            return (self.departure - self.plannedDeparture).seconds // 60
        else:
            return None


def leg_or_transfer(leg: dict) -> Leg | Transfer:
    if 'walking' in leg and leg['walking']:
        return Transfer.from_leg_dict(leg)
    else:
        return Leg.from_dict(leg)


@dataclass
class Price:
    amount: int
    currency: str
    type: str = 'price'

    @staticmethod
    def from_dict(price: dict) -> 'Price':
        if price['amount'] < 0:
            None
        else:
            return Price(amount=price['amount'], currency=price['currency'])


@dataclass
class Journey:
    legs: list[Leg | Transfer]
    price: Price | None = None
    refreshToken: str | None = None
    type: str = 'journey'

    @staticmethod
    def from_dict(journey: dict) -> 'Journey':
        return Journey(
            legs=[leg_or_transfer(leg) for leg in journey['legs']],
            price=Price.from_dict(journey['price']) if 'price' in journey else None,
            refreshToken=journey.get('refreshToken'),
        )

    def get_trip_ids(self) -> set[str]:
        return {
            leg.tripId
            for leg in self.legs
            if leg.type == 'leg' and leg.tripId is not None
        }


@dataclass
class JourneyAndAlternatives:
    journey: Journey
    alternatives: list[Journey]
