import urllib.parse
from datetime import datetime
from typing import Literal

import requests

from api import fptf
from api.exceptions import RateLimitExceeded
from helpers.cache import ttl_lru_cache
from helpers.StationPhillip import AvailableTransports, StationV3

# DB_REST_BASE_URL = 'https://db-rest.bahnvorhersage.de/'
DB_REST_BASE_URL = 'http://10.200.200.2:32411/'


def get_matching_stops(name: str):
    r = requests.get(
        DB_REST_BASE_URL + 'locations',
        params={'query': name, 'addresses': False, 'poi': False},
    )
    if r.ok:
        return [fptf.Stop.from_dict(stop) for stop in r.json()]
    elif r.status_code == 429:
        raise RateLimitExceeded('Too many requests, rate limit exceeded')
    else:
        raise requests.RequestException(r.text)


def get_exact_stop(name: str = None, eva: int = None):
    if eva is not None:
        r = requests.get(DB_REST_BASE_URL + f'stops/{eva}')
        if r.ok:
            return fptf.Stop.from_dict(r.json()) if r.json() else None
        elif r.status_code == 429:
            raise RateLimitExceeded('Too many requests, rate limit exceeded')
        elif r.status_code == 404:
            return None
        else:
            raise requests.RequestException(r.text)

    elif name is not None:
        matches = get_matching_stops(name)
        for match in matches:
            if match.name == name:
                return match
        else:
            return None


def get_station(name: str = None, eva: int = None):
    stop = get_exact_stop(name=name, eva=eva)
    if stop is None:
        return None

    available_transports = []
    if stop.products.nationalExpress:
        available_transports.append(AvailableTransports.HIGH_SPEED_TRAIN)
    if stop.products.national:
        available_transports.append(AvailableTransports.INTERCITY_TRAIN)
    if stop.products.regionalExpress or stop.products.regional:
        available_transports.append(AvailableTransports.REGIONAL_TRAIN)
    if stop.products.suburban:
        available_transports.append(AvailableTransports.CITY_TRAIN)
    if stop.products.bus:
        available_transports.append(AvailableTransports.BUS)
    if stop.products.ferry:
        available_transports.append(AvailableTransports.FERRY)
    if stop.products.subway:
        available_transports.append(AvailableTransports.SUBWAY)
    if stop.products.tram:
        available_transports.append(AvailableTransports.TRAM)
    if stop.products.taxi:
        available_transports.append(AvailableTransports.TAXI)

    return StationV3(
        eva=int(stop.id),
        name=stop.name,
        ds100=None,
        lat=stop.location.latitude,
        lon=stop.location.longitude,
        available_transports=available_transports,
    )


def get_journey(
    from_: int | str,
    to: int | str,
    departure: datetime | None = None,
    arrival: datetime | None = None,
    results: int | None = None,
    stopovers: bool = True,
    transfers: int | None = None,
    transfer_time: int | None = None,
    accessibility: Literal['partial', 'complete'] | None = None,
    bike: bool = False,
    start_with_walking: bool = True,
    walking_speed: Literal['slow', 'normal', 'fast'] = 'normal',
    tickets: bool = False,
    only_regional: bool = False,
):
    params = {
        'from': from_,
        'to': to,
    }

    if departure is not None and arrival is not None:
        raise ValueError('Either departure or arrival must be given, not both')
    elif departure is not None:
        params['departure'] = departure.isoformat()
    elif arrival is not None:
        params['arrival'] = arrival.isoformat()
    else:
        raise ValueError('Either departure or arrival must be given')
    if results is not None:
        params['results'] = results
    if stopovers:
        params['stopovers'] = True
    if transfers is not None:
        params['transfers'] = transfers
    if transfer_time is not None:
        params['transferTime'] = transfer_time
    if accessibility is not None:
        params['accessibility'] = accessibility
    if bike:
        params['bike'] = bike
    if not start_with_walking:
        params['startWithWalking'] = start_with_walking
    if walking_speed != 'normal':
        params['walkingSpeed'] = walking_speed
    if tickets:
        params['tickets'] = tickets
    if only_regional:
        params['national'] = False
        params['nationalExpress'] = False

    r = requests.get(
        DB_REST_BASE_URL + 'journeys',
        params=params,
    )
    if r.ok:
        return [fptf.Journey.from_dict(journey) for journey in r.json()['journeys']]
    elif r.status_code == 429:
        raise RateLimitExceeded('Too many requests, rate limit exceeded')
    elif r.status_code == 404:
        return None
    else:
        raise requests.RequestException(r.text)


def refresh_journey(refresh_token: str):
    r = requests.get(
        DB_REST_BASE_URL + f'journeys/{urllib.parse.quote(refresh_token, safe="")}',
        params={'tickets': True, 'stopovers': True},
    )
    if r.ok:
        return [fptf.Journey.from_dict(r.json()['journey'])]
    elif r.status_code == 429:
        raise RateLimitExceeded('Too many requests, rate limit exceeded')
    else:
        raise requests.RequestException(r.text)


@ttl_lru_cache(seconds_to_live=60 * 2, maxsize=1024)
def get_trip(trip_id: str):
    r = requests.get(
        f'{DB_REST_BASE_URL}trips/{urllib.parse.quote(trip_id)}',
    )
    if r.ok:
        return fptf.Stopover.trip_from_list(r.json()['trip']['stopovers'])
    elif r.status_code == 429 or 'Too Many Requests' in r.text:
        raise RateLimitExceeded('Too many requests, rate limit exceeded')
    else:
        raise requests.RequestException(r.text)


if __name__ == '__main__':
    print(
        get_journey(
            from_=8000141,
            to=8000207,
            departure=datetime.now(),
            stopovers=True,
            tickets=True,
        )
    )
