import argparse
from concurrent.futures import ThreadPoolExecutor
from datetime import date, timedelta
from itertools import repeat

import polars as pl
from sqlalchemy import and_, select
from sqlalchemy.dialects import postgresql
from tqdm import tqdm

from database.engine import PL_DB_URI
from gtfs.routes import RouteRachel
from gtfs.stop_times import PickUpDropOffType, StopTimes
from gtfs.stops import StopSteffen
from gtfs.trip_update import TripUpdate
from helpers.date_range import date_range
from helpers.retry import retry
from public_config import PROGRESSION_PATH

PL_PICKUP_DROP_OFF_TYPE = pl.Enum(list(PickUpDropOffType.__members__.keys()))

DB_TYPES_PL = {
    'stop_id': pl.Int64,
    'stop_sequence': pl.UInt16,
    'arrival_time': pl.Datetime(time_zone='UTC'),
    'departure_time': pl.Datetime(time_zone='UTC'),
    'distance_traveled': pl.Int32,
    'trip_id': pl.Int64,
    'route_id': pl.Int64,
    'trip_headsign': pl.Int64,
    'update_timestamp': pl.Datetime(time_zone='UTC'),
    'changed_stop_id': pl.Int64,
    'changed_arrival_time': pl.Datetime(time_zone='UTC'),
    'changed_departure_time': pl.Datetime(time_zone='UTC'),
    'arrival_cancelled': pl.Boolean,
    'departure_cancelled': pl.Boolean,
    'is_betriebshalt': pl.Boolean,
    'pickup_type': PL_PICKUP_DROP_OFF_TYPE,
    'drop_off_type': PL_PICKUP_DROP_OFF_TYPE,
}

DATAFRAME_TYPES_PL = {
    'trip_id': pl.Int64,
    'time_schedule': pl.Datetime(time_zone='UTC', time_unit='us'),
    'time_real': pl.Datetime(time_zone='UTC', time_unit='us'),
    'update_timestamp': pl.Datetime(
        time_zone='UTC', time_unit='us'
    ),  # Crawling timestamp of the update / realtime data
    'delay': pl.Int32,  # In seconds
    'dwell_time_schedule': pl.Int32,  # In seconds
    'dwell_time_real': pl.Int32,  # In seconds
    'is_final': pl.Boolean,
    'is_arrival': pl.Boolean,
    'is_cancelled': pl.Boolean,
    'is_regional': pl.Boolean,
    'category': pl.String,
    'number': pl.String,
    'line': pl.String,
    'operator': pl.String,
    'stop_id': pl.Int64,
    'platform_id_schedule': pl.Int64,
    'platform_id_real': pl.Int64,
    'lat': pl.Float64,
    'lon': pl.Float64,
    'distance_traveled': pl.Int32,
    'stop_sequence': pl.UInt16,
    'trip_headsign': pl.Int64,
    'is_betriebshalt': pl.Boolean,
    'pickup_drop_off_type': PL_PICKUP_DROP_OFF_TYPE,
}


def check_schema(df: pl.DataFrame, schema: dict):
    for col, dtype in schema.items():
        if col not in df.columns:
            raise ValueError(f'Column {col} not found in DataFrame')
        if df[col].dtype != dtype:
            raise ValueError(
                f'Column {col} has wrong type {df[col].dtype}, expected {dtype}'
            )

    for col in df.columns:
        if col not in schema:
            raise ValueError(f'Column {col} not expected in DataFrame')


def db_join_statement(service_date: date):
    stops = (
        select(
            StopTimes.stop_id.label('stop_id'),
            StopTimes.stop_sequence.label('stop_sequence'),
            StopTimes.arrival_time.label('arrival_time'),
            StopTimes.departure_time.label('departure_time'),
            StopTimes.shape_dist_traveled.label('distance_traveled'),
            StopTimes.trip_id.label('trip_id'),
            StopTimes.route_id.label('route_id'),
            StopTimes.trip_headsign.label('trip_headsign'),
            TripUpdate.timestamp.label('update_timestamp'),
            TripUpdate.changed_stop_id.label('changed_stop_id'),
            TripUpdate.arrival_time.label('changed_arrival_time'),
            TripUpdate.departure_time.label('changed_departure_time'),
            TripUpdate.arrival_cancelled.label('arrival_cancelled'),
            TripUpdate.departure_cancelled.label('departure_cancelled'),
            StopTimes.is_betriebshalt.label('is_betriebshalt'),
            StopTimes.pickup_type.label('pickup_type'),
            StopTimes.drop_off_type.label('drop_off_type'),
        )
        .join(
            TripUpdate,
            and_(
                StopTimes.service_date == TripUpdate.service_date,
                StopTimes.trip_id == TripUpdate.trip_id,
                StopTimes.stop_sequence == TripUpdate.stop_sequence,
            ),
            isouter=True,
        )
        .where(
            StopTimes.service_date == service_date,
        )
    )

    return str(
        stops.compile(
            dialect=postgresql.dialect(), compile_kwargs={'literal_binds': True}
        )
    )


def add_route_info(static: pl.DataFrame, stops: pl.DataFrame, routes: pl.DataFrame):
    # Add route and stop info
    # IMPORTANT: Updates use eva as stop_id, not a hash of stop and platform code,
    # thus, first, the hash in static must be replaced with the eva, and then it can
    # be joined
    static = (
        static.join(stops, on='stop_id', how='left')
        .join(routes, on='route_id', how='left')
        .drop(['route_id'])
        .rename({'stop_id': 'platform_id_schedule', 'parent_station': 'stop_id'})
    )

    return static


@retry(3)
def download_progression_for_date(
    service_date: date, stops: pl.DataFrame, routes: pl.DataFrame
):
    statement = db_join_statement(service_date)

    joined = pl.read_database_uri(
        query=statement, uri=PL_DB_URI, schema_overrides=DB_TYPES_PL
    )

    # Do not process days with missing data
    if joined.is_empty():
        return

    joined = add_route_info(joined, stops, routes)

    # Mark newest update as final
    joined = joined.sort(
        ['trip_id', 'stop_id', 'stop_sequence', 'update_timestamp']
    ).with_columns(
        pl.struct(['trip_id', 'stop_id', 'stop_sequence'])
        .is_last_distinct()
        .alias('is_final')
    )

    joined = joined.rename({'changed_stop_id': 'platform_id_real'})

    # Fill missing changed times with scheduled times and missing cancellations with
    # False. Some static data does not have updates, so we need to fill that too
    joined = joined.with_columns(
        pl.col('changed_arrival_time').fill_null(pl.col('arrival_time')),
        pl.col('changed_departure_time').fill_null(pl.col('departure_time')),
        pl.col('arrival_cancelled').fill_null(pl.lit(False)),
        pl.col('departure_cancelled').fill_null(pl.lit(False)),
        pl.col('is_final').fill_null(
            pl.lit(True)
        ),  # If there is no update, the plan is final
        pl.col('platform_id_real').fill_null(pl.col('platform_id_schedule')),
    )

    # Add dwell times
    joined = joined.with_columns(
        (pl.col('departure_time').sub(pl.col('arrival_time')))
        .dt.total_seconds()
        .alias('dwell_time_schedule')
        .cast(DATAFRAME_TYPES_PL['dwell_time_schedule']),
        # If arrival or departure is cancelled, real dwell time is not defined
        pl.when(pl.col('arrival_cancelled') | pl.col('departure_cancelled'))
        .then(pl.lit(None))
        .otherwise(
            (
                pl.col('changed_departure_time').sub(pl.col('changed_arrival_time'))
            ).dt.total_seconds()
        )
        .alias('dwell_time_real')
        .cast(DATAFRAME_TYPES_PL['dwell_time_real']),
    )

    # Extract "pure" arrival and pure departure data
    arrivals = (
        joined.filter(pl.col('arrival_time').is_not_null())
        .rename(
            {
                'arrival_time': 'time_schedule',
                'changed_arrival_time': 'time_real',
                'arrival_cancelled': 'is_cancelled',
                'pickup_type': 'pickup_drop_off_type',
            }
        )
        .with_columns(
            pl.lit(True).alias('is_arrival'),
        )
        .drop(
            [
                'departure_time',
                'changed_departure_time',
                'departure_cancelled',
                'drop_off_type',
            ]
        )
    )
    departures = (
        joined.filter(pl.col('departure_time').is_not_null())
        .rename(
            {
                'departure_time': 'time_schedule',
                'changed_departure_time': 'time_real',
                'departure_cancelled': 'is_cancelled',
                'drop_off_type': 'pickup_drop_off_type',
            }
        )
        .with_columns(
            pl.lit(False).alias('is_arrival'),
        )
        .drop(
            ['arrival_time', 'changed_arrival_time', 'arrival_cancelled', 'pickup_type']
        )
    )

    joined = pl.concat([arrivals, departures])

    # Add delays
    joined = joined.with_columns(
        (pl.col('time_real').sub(pl.col('time_schedule')))
        .dt.total_seconds()
        .alias('delay')
        .cast(DATAFRAME_TYPES_PL['delay']),
    )

    check_schema(joined, DATAFRAME_TYPES_PL)

    joined.write_parquet(PROGRESSION_PATH.format(service_date=service_date.isoformat()))


def download_date_range(start: date, end: date):
    stops = (
        StopSteffen()
        .to_polars()
        .rename({'stop_lat': 'lat', 'stop_lon': 'lon'})
        .drop(['location_type', 'stop_name', 'platform_code'])
    )

    routes = RouteRachel().to_polars()

    days_to_download = list(date_range(start, end))
    n_days = len(days_to_download)

    # # Non-concurrent download for debugging
    # for _ in tqdm(
    #     map(
    #         download_progression_for_date,
    #         days_to_download,
    #         repeat(stops),
    #         repeat(routes),
    #     ),
    #     desc='Downloading train stops',
    #     total=n_days,
    # ):
    #     pass

    with ThreadPoolExecutor(max_workers=8) as executor:
        for _ in tqdm(
            executor.map(
                download_progression_for_date,
                days_to_download,
                repeat(stops),
                repeat(routes),
            ),
            desc='Downloading train stops',
            total=n_days,
        ):
            pass


def download_last_two_weeks():
    download_date_range(date.today() - timedelta(days=14), date.today())


def download_all():
    download_date_range(date(2021, 9, 1), date.today())


def main(args):
    if args.last_two_weeks:
        download_last_two_weeks()
    elif args.all:
        download_all()


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '--last-two-weeks', action='store_true', help='Download last two weeks'
    )
    argparser.add_argument(
        '--all', action='store_true', help='Download all available data'
    )
    args = argparser.parse_args()
    main(args)
