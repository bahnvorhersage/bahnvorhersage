from database.engine import get_engine
from database.upsert import upsert_with_retry
from gtfs.agency import Agency
from gtfs.routes import Routes
from gtfs.stops import Stops
from helpers.logger import logging


class GTFSUpserter:
    def __init__(self):
        self.stops = {}
        self.agecies = {}
        self.routes = {}
        self.sa_engine = get_engine()

        self.stops_changed = False
        self.agencies_changed = False
        self.routes_changed = False

    def upsert(
        self,
        stops: dict[int, tuple],
        agencies: dict[int, tuple],
        routes: dict[int, tuple],
    ):
        self.stops_changed = stops.items() >= self.stops.items() or self.stops_changed
        self.agencies_changed = (
            agencies.items() >= self.agecies.items() or self.agencies_changed
        )
        self.routes_changed = (
            routes.items() >= self.routes.items() or self.routes_changed
        )

        self.stops.update(stops)
        self.agecies.update(agencies)
        self.routes.update(routes)

    def flush(self):
        if self.stops_changed:
            logging.info('upserting stops')
            upsert_with_retry(
                engine=self.sa_engine,
                table=Stops.__table__,
                rows=list(self.stops.values()),
            )
            self.stops_changed = False

        if self.agencies_changed:
            logging.info('upserting agencies')
            upsert_with_retry(
                engine=self.sa_engine,
                table=Agency.__table__,
                rows=list(self.agecies.values()),
            )
            self.agencies_changed = False

        if self.routes_changed:
            logging.info('upserting routes')
            upsert_with_retry(
                engine=self.sa_engine,
                table=Routes.__table__,
                rows=list(self.routes.values()),
            )
            self.routes_changed = False
