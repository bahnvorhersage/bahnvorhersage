import sys
import time
import traceback

from redis import Redis

from config import redis_url
from database import unparsed
from helpers.logger import logging
from parser.generate_trip_updates import (
    parse_trip_update_service_date,
)
from parser.gtfs_upserter import GTFSUpserter
from parser.to_gtfs_static import parse_service_date


def parse_unparsed_static(
    redis_client: Redis, last_stream_id: bytes, upserter: GTFSUpserter
) -> bytes:
    last_stream_id, unparsed_ids = unparsed.get_plan(redis_client, last_stream_id)
    if unparsed_ids:
        logging.info(f'parsing {len(unparsed_ids)} unparsed plans')
        parsing_result = parse_service_date(unparsed_ids=unparsed_ids)
        upserter.upsert(*parsing_result)
        upserter.flush()

        # Do not parse to csa connections for now
        # stop_steffen = StopSteffen()
        # engine, Session = sessionfactory()
        # calendar_dates = parsing_result[2]
        # dates = set(date for _, date, _ in calendar_dates.values())
        # for date in dates:
        #     print('parsing ', date, ' to csa connections')
        #     to_csa_connections(date, stop_steffen, engine, Session)

    return last_stream_id


def parse_unparsed_updates(
    redis_client: Redis,
    last_stream_id: bytes,
) -> bytes:
    last_stream_id, unparsed_ids = unparsed.get_change(redis_client, last_stream_id)
    if unparsed_ids:
        logging.info(f'parsing {len(unparsed_ids)} unparsed updates')
        parse_trip_update_service_date(unparsed_ids=unparsed_ids)

    return last_stream_id


def parse_unparsed_continues():
    gtfs_upserter = GTFSUpserter()
    redis_client = Redis.from_url(redis_url)
    last_static_stream_id = b'0-0'
    last_update_stream_id = b'0-0'
    while True:
        try:
            last_static_stream_id = parse_unparsed_static(
                redis_client, last_static_stream_id, gtfs_upserter
            )
        except Exception:
            logging.error('Error parsing static')
            traceback.print_exc(file=sys.stdout)

        try:
            last_update_stream_id = parse_unparsed_updates(
                redis_client, last_update_stream_id
            )
        except Exception:
            logging.error('Error parsing updates')
            traceback.print_exc(file=sys.stdout)
        time.sleep(60)


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    parse_unparsed_continues()
