import concurrent.futures
import multiprocessing as mp
import os
from datetime import date

import sqlalchemy
from tqdm import tqdm

from api.iris import TimetableStop
from database.chunk_limits import get_date_limits
from database.engine import sessionfactory
from database.plan_by_id import PlanByIdV4
from database.upsert import tuples_to_csv, upsert_copy_from
from gtfs.agency import Agency
from gtfs.routes import Routes, RouteType
from gtfs.stop_times import PickUpDropOffType, StopTimes
from gtfs.stops import LocationType, Stops
from helpers.date_range import date_range, date_range_len
from helpers.hash64 import xxhash64
from helpers.StreckennetzSteffi import StreckennetzSteffi
from parser.gtfs_upserter import GTFSUpserter

""" Clear dangeling connections to db
SELECT pg_terminate_backend(pid)
FROM pg_stat_activity
WHERE datname = 'tcp'
AND pid <> pg_backend_pid()
AND state in ('idle', 'idle in transaction', 'idle in transaction (aborted)', 'disabled');
"""  # noqa: W505

streckennetz = StreckennetzSteffi(prefer_cache=False)


def stop_to_gtfs(
    plan: PlanByIdV4,
) -> tuple[Stops, Stops, Stops, Agency, Routes, StopTimes]:
    stop = TimetableStop(plan.plan)
    line = stop.arrival.line if stop.arrival is not None else stop.departure.line
    if line is None:
        line = stop.trip_label.number
    route_short_name = f'{stop.trip_label.category} {line}'
    route_long_name = f'{stop.trip_label.category} {stop.trip_label.number}'

    trip_id = xxhash64(str(stop.trip_id) + '_' + stop.date_id.isoformat())
    route_id = xxhash64(route_long_name)

    station_name = streckennetz.get_name(eva=plan.stop_id)
    platform_code = (
        stop.arrival.planned_platform
        if stop.arrival is not None
        else stop.departure.planned_platform
    )
    stop_id = xxhash64(str(plan.stop_id) + '_' + platform_code)

    station_coords = streckennetz.get_location(eva=plan.stop_id)

    station = Stops(
        stop_id=plan.stop_id,
        stop_name=station_name,
        stop_lat=station_coords[0],
        stop_lon=station_coords[1],
        location_type=LocationType.STATION,
        parent_station=None,
        platform_code=None,
    )

    platform = Stops(
        stop_id=stop_id,
        stop_name=station_name,
        stop_lat=station_coords[0],
        stop_lon=station_coords[1],
        location_type=LocationType.STOP,
        parent_station=plan.stop_id,
        platform_code=platform_code,
    )

    if stop.departure is None or stop.departure.planned_path is None:
        heading_to_stop_id = plan.stop_id
    else:
        for i in range(1, len(stop.departure.planned_path) + 1):
            try:
                heading_to_stop_id = streckennetz.get_eva(
                    stop.departure.planned_path[-i]
                )
                break
            except KeyError:
                continue
        else:
            heading_to_stop_id = plan.stop_id
            print(f'Could not find heading stop for {stop.departure.planned_path}')

    heading_to_coords = streckennetz.get_location(eva=heading_to_stop_id)

    heading_to = Stops(
        stop_id=heading_to_stop_id,
        stop_name=streckennetz.get_name(eva=heading_to_stop_id),
        stop_lat=heading_to_coords[0],
        stop_lon=heading_to_coords[1],
        location_type=LocationType.STATION,
        parent_station=None,
        platform_code=None,
    )

    agency = Agency(
        agency_id=stop.trip_label.owner,
        agency_name=stop.trip_label.owner,
        agency_url='https://bahnvorhersage.de',
        agency_timezone='Europe/Berlin',
    )

    routes = Routes(
        route_id=route_id,
        agency_id=stop.trip_label.owner,
        route_short_name=route_short_name,
        route_long_name=route_long_name,
        route_type=RouteType.BUS if stop.is_bus() else RouteType.RAIL,
    )

    stop_times = StopTimes(
        service_date=stop.date_id.date(),
        trip_id=trip_id,
        stop_id=stop_id,
        stop_sequence=stop.stop_sequence_id,
        arrival_time=stop.arrival.planned_time if stop.arrival is not None else None,
        departure_time=stop.departure.planned_time
        if stop.departure is not None
        else None,
        shape_dist_traveled=streckennetz.route_length(
            stop.arrival.planned_path + [station_name], is_bus=stop.is_bus()
        )
        if stop.arrival is not None and stop.arrival.planned_path is not None
        else 0,
        route_id=route_id,
        trip_headsign=heading_to_stop_id,
        is_betriebshalt=stop.is_betriebshalt,
        pickup_type=PickUpDropOffType.from_event(stop.arrival)
        if stop.arrival is not None
        else None,
        drop_off_type=PickUpDropOffType.from_event(stop.departure)
        if stop.departure is not None
        else None,
    )

    return (
        station,
        platform,
        heading_to,
        agency,
        routes,
        stop_times,
    )


def parse_service_date(service_date: date = None, unparsed_ids: list[dict] = None):
    engine, Session = sessionfactory(
        poolclass=sqlalchemy.pool.NullPool,
    )

    with Session() as session:
        if service_date is not None:
            plans = PlanByIdV4.get_stops_of_service_date(session, service_date)
        elif unparsed_ids is not None:
            plans = PlanByIdV4.get_stops_from_unparsed_ids(session, unparsed_ids)
        else:
            raise ValueError('Either service_date or unparsed_ids must be given')

    stops = {}
    agencies = {}
    routes = {}
    stop_times = {}

    for plan in plans:
        station, platform, heading_to, agency, route, stop_time = stop_to_gtfs(plan)

        stops[station.stop_id] = station.as_tuple()
        stops[platform.stop_id] = platform.as_tuple()
        stops[heading_to.stop_id] = heading_to.as_tuple()
        agencies[agency.agency_id] = agency.as_tuple()
        routes[route.route_id] = route.as_tuple()
        stop_times[stop_time.trip_id, stop_time.stop_sequence] = stop_time.as_tuple()

    upsert_copy_from(
        table=StopTimes.__table__,
        csv=tuples_to_csv(list(stop_times.values())),
        on_conflict_strategy='nothing',
        engine=engine,
    )

    return (
        stops,
        agencies,
        routes,
    )


def parse_all():
    """Parse all raw data there is"""

    engine, Session = sessionfactory(
        poolclass=sqlalchemy.pool.NullPool,
    )

    with Session() as session:
        date_limits = get_date_limits(session, PlanByIdV4.service_date)
    engine.dispose()

    gtfs_upserter = GTFSUpserter()

    # # Non-concurrent code for debugging
    # for service_date in tqdm(date_range(*date_limits), total=date_range_len(*date_limits)):  # noqa: W505
    #     gtfs_upserter.upsert(*parse_service_date(service_date=service_date))

    n_processes = min(20, os.cpu_count())
    all_dates = list(date_range(*date_limits))

    with tqdm(total=date_range_len(*date_limits)) as pbar:
        with concurrent.futures.ProcessPoolExecutor(
            n_processes, mp_context=mp.get_context('spawn')
        ) as executor:
            parser_tasks = {
                executor.submit(parse_service_date, all_dates.pop(0))
                for _ in range(min(100, date_range_len(*date_limits)))
            }

            while parser_tasks:
                done, parser_tasks = concurrent.futures.wait(
                    parser_tasks, return_when='FIRST_COMPLETED'
                )

                for future in done:
                    gtfs_upserter.upsert(*future.result())
                    if all_dates:
                        parser_tasks.add(
                            executor.submit(parse_service_date, all_dates.pop(0))
                        )
                    pbar.update()

        gtfs_upserter.flush()


def main():
    parse_all()


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
