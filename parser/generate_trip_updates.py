import concurrent.futures
import multiprocessing as mp
import os
from datetime import date

import sqlalchemy
import sqlalchemy.orm
from tqdm import tqdm

from api.iris import EventStatus, TimetableStop
from database.chunk_limits import get_date_limits
from database.engine import sessionfactory
from database.unique_change import UniqueChangeV4
from database.upsert import tuples_to_csv, upsert_copy_from
from gtfs.trip_update import TripUpdate
from helpers.date_range import date_range, date_range_len
from helpers.hash64 import xxhash64

""" Clear dangeling connections to db
SELECT pg_terminate_backend(pid)
FROM pg_stat_activity
WHERE datname = 'tcp'
AND pid <> pg_backend_pid()
AND state in ('idle', 'idle in transaction', 'idle in transaction (aborted)', 'disabled');
"""  # noqa: W505


def change_to_trip_update(
    change: UniqueChangeV4,
) -> TripUpdate | None:
    # Some changes do not contain trip information, these usually contain a 'from' key.
    # Example: {"from": "2311120915", "id": "i1039370", "t": "i", "to": "2311121900", "ts": "2311120917", "ts-tts": "23-11-12 09:18:11.657"} # noqa: W505
    if 'from' in change.change:
        return None

    stop = TimetableStop(change.change)

    trip_id = xxhash64(str(stop.trip_id) + '_' + stop.date_id.isoformat())

    platform_code = (
        stop.arrival.planned_platform
        if stop.arrival is not None
        else (stop.departure.planned_platform if stop.departure is not None else None)
    )
    stop_id = (
        xxhash64(str(change.stop_id) + '_' + platform_code)
        if platform_code is not None
        else change.stop_id
    )

    changed_platform_code = (
        stop.arrival.changed_platform
        if stop.arrival is not None
        else (stop.departure.changed_platform if stop.departure is not None else None)
    )
    changed_stop_id = (
        xxhash64(str(stop.stop_sequence_id) + '_' + changed_platform_code)
        if changed_platform_code is not None
        else None
    )

    trip_update = TripUpdate(
        service_date=change.service_date,
        trip_id=trip_id,
        stop_id=stop_id,
        timestamp=change.time_crawled,
        changed_stop_id=changed_stop_id,
        stop_sequence=stop.stop_sequence_id,
        arrival_time=stop.arrival.changed_time if stop.arrival is not None else None,
        departure_time=stop.departure.changed_time
        if stop.departure is not None
        else None,
        arrival_cancelled=stop.arrival.changed_status == EventStatus.CANCELLED
        if stop.arrival is not None
        else None,
        departure_cancelled=stop.departure.changed_status == EventStatus.CANCELLED
        if stop.departure is not None
        else None,
    )

    change = None

    return trip_update


def parse_trip_update_service_date(
    service_date: date | None = None, unparsed_ids: list[dict] | None = None
):
    engine, Session = sessionfactory(
        poolclass=sqlalchemy.pool.NullPool,
    )

    with Session() as session:
        if service_date is not None:
            changes = UniqueChangeV4.get_stops_of_service_date(session, service_date)
        elif unparsed_ids is not None:
            changes = UniqueChangeV4.get_stops_from_unparsed_ids(session, unparsed_ids)
        else:
            raise ValueError('Either service_date or unparsed_ids must be given')

    trip_updates = {}

    for change in changes:
        trip_update = change_to_trip_update(change)

        if trip_update is not None:
            trip_updates[
                trip_update.trip_id, trip_update.stop_id, trip_update.timestamp
            ] = trip_update.as_tuple()

        if len(trip_updates) >= 500_000:
            upsert_copy_from(
                table=TripUpdate.__table__,
                csv=tuples_to_csv(list(trip_updates.values())),
                on_conflict_strategy='nothing',
                engine=engine,
            )
            trip_updates = {}

    if len(trip_updates) > 0:
        upsert_copy_from(
            table=TripUpdate.__table__,
            csv=tuples_to_csv(list(trip_updates.values())),
            on_conflict_strategy='nothing',
            engine=engine,
        )


def parse_all():
    """Parse all raw data there is"""

    engine, Session = sessionfactory(
        poolclass=sqlalchemy.pool.NullPool,
    )

    with Session() as session:
        date_limits = list(get_date_limits(session, UniqueChangeV4.service_date))

    # We have changes from before September 2021, but we don't want to
    # parse them since we don't have plans for them.
    date_limits[0] = max(date_limits[0], date(2021, 9, 1))

    # # Non-concurrent code for debugging
    # for service_date in tqdm(
    #     date_range(*date_limits), total=date_range_len(*date_limits)
    # ):
    #     parse_trip_update_service_date(service_date=service_date)

    n_processes = min(20, os.cpu_count())
    all_dates = list(date_range(*date_limits))

    with tqdm(total=date_range_len(*date_limits)) as pbar:
        with concurrent.futures.ProcessPoolExecutor(
            n_processes, mp_context=mp.get_context('spawn')
        ) as executor:
            parser_tasks = {
                executor.submit(parse_trip_update_service_date, all_dates.pop(0))
                for _ in range(min(100, date_range_len(*date_limits)))
            }

            while parser_tasks:
                done, parser_tasks = concurrent.futures.wait(
                    parser_tasks, return_when='FIRST_COMPLETED'
                )

                for future in done:
                    if all_dates:
                        parser_tasks.add(
                            executor.submit(
                                parse_trip_update_service_date, all_dates.pop(0)
                            )
                        )
                    pbar.update()


def main():
    parse_all()


if __name__ == '__main__':
    from helpers.bahn_vorhersage import COLORFUL_ART

    print(COLORFUL_ART)

    main()
